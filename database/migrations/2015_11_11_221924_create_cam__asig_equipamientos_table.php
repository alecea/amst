<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_AsigEquipamientosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__asig_equipamientos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('Emp_Id', 10);
			$table->datetime('Aeq_Fecha', 23);
			$table->string('Aeq_Hora', 5);
			$table->int('Aeq_Observaciones', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__asig_equipamientos');
	}

}
