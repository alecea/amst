<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDes_ValesGasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('des__vales_gas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->Int('Emp_IdSolicita');
			$table->String('Via_Observacion', 150);
			$table->float('Via_CantidadSolicitada');
			$table->Boolean('Via_Autorizado');
			$table->date('Via_FechaEntrega');
			$table->double('Via_CantidadEntregada');
			$table->int('Emp_IdAutoriza');
			$table->int('Veh_Id');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('des__vales_gas');
	}

}
