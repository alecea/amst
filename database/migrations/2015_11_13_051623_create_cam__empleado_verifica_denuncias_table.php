<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_EmpleadoVerificaDenunciasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__empleado_verifica_denuncias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('Den_Id', 10);
			$table->int('Emp_Id', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__empleado_verifica_denuncias');
	}

}
