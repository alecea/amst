<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDes_DiagnosticoTallersTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('des__diagnostico_tallers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('Emp_Id')->nullable();
			$table->datetime('Dta_FechaDiagnostico')->nullable();
			$table->string('Dta_Diagnostico', 10)->nullable();
			$table->int('Tal_Id')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('des__diagnostico_tallers');
	}

}
