<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_DetAsignacionEquipamientosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__det_asignacion_equipamientos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('Cantidad', 10);
			$table->int('Aeq_Id', 10);
			$table->int('Teq_Id', 10);
			$table->int('Equ_Id', 10);
			$table->int('Arm_Id', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__det_asignacion_equipamientos');
	}

}
