<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_DenunciasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__denuncias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('Per_Id', 10);
			$table->datetime('Den_Fecha', 23);
			$table->string('Den_Hora', 5);
			$table->Int('Aop_Id', 10);
			$table->int('Zon_Id', 10);
			$table->string('Den_Direccion', 50);
			$table->string('Den_ObservacionDenuncia', 2147483647);
			$table->bit('Den_Procede', 1);
			$table->string('Den_JustificacionProcedencia', 2147483647);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__denuncias');
	}

}
