<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_MultasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__multas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->varchar('Mul_Estado', 45);
			$table->int('Omu_Id', 10);
			$table->int('Inf_Id', 10);
			$table->datetime('Mul_Fecha', 23);
			$table->varchar('Mul_Hora', 5);
			$table->int('Tin_Id', 10);
			$table->int('Per_Id', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__multas');
	}

}
