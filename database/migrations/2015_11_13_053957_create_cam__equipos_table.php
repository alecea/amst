<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_EquiposTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__equipos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->varchar('Equ_NumeroSerie', 15);
			$table->varchar('Equ_Descripcion', 50);
			$table->int('Teq_Id', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__equipos');
	}

}
