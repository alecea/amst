<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_PatrullajesTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__patrullajes', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('Tur_Id', 10);
			$table->int('Emp_Id', 10);
			$table->varchar('Pat_Observaciones', 50);
			$table->datetime('Pat_Fecha', 23);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__patrullajes');
	}

}
