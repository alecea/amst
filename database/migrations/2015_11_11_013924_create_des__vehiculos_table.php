<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDes_VehiculosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('des__vehiculos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->String('Veh_Marca', 50);
			$table->Int('Veh_AnnioFabricacion');
			$table->String('Veh_NoMatricula', 20);
			$table->Int('Tve_Id');
			$table->int('Veh_Annio');
			$table->String('Veh_Modelo', 50);
			$table->Int('Veh_Kilometraje');
			$table->String('Veh_Color', 50);
			$table->float('Veh_Cilindraje');
			$table->string('Veh_NoMotor', 20);
			$table->String('Veh_NoChasis', 20);
			$table->datetime('Veh_FechaAdquisicion');
			$table->Bit('Veh_Actibo');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('des__vehiculos');
	}

}
