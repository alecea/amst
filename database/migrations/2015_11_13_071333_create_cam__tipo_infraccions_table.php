<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_TipoInfraccionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__tipo_infraccions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->varchar('Tin_Articulo', 3);
			$table->varchar('Tin_Descripcion', 2147483647);
			$table->float('Tin_Monto', 53);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__tipo_infraccions');
	}

}
