<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_MovInventariosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__mov_inventarios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->datetime('Min_Fecha', 23);
			$table->char('Min_TipoMov', 1);
			$table->int('Com_Id', 10);
			$table->int('Equ_Id', 10);
			$table->int('Arm_Id', , 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__mov_inventarios');
	}

}
