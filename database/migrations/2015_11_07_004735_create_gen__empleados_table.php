<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGen_EmpleadosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('gen__empleados', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('Emp_PrimerNombre', 20)->nullable();
			$table->string('Emp_SegundoNombre', 20)->nullable();
			$table->string('Emp_PrimerApellido', 20)->nullable();
			$table->string('Emp_SegundoApellido', 20)->nullable();
			$table->string('Emp_Telefono', 10)->nullable();
			$table->string('Emp_Celular', 10)->nullable();
			$table->string('Emp_Dui', 20)->nullable();
			$table->string('Emp_Direccion', 45)->nullable();
			$table->string('Emp_Cargo', 50)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('gen__empleados');
	}

}
