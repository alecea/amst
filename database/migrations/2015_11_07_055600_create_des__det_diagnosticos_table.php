<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDes_DetDiagnosticosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('des__det_diagnosticos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('Dta_Id', 10)->nullable();
			$table->int('Pve_Id', 10)->nullable();
			$table->string('Ded_Observacion', 100)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('des__det_diagnosticos');
	}

}
