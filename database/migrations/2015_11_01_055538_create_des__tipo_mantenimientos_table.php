<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDes_TipoMantenimientosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('des__tipo_mantenimientos', function(Blueprint $table)
		{
			$table->increments('Tma_Id');
			$table->string('Tma_Descripcion', 150)->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('des__tipo_mantenimientos');
	}

}
