<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_TurnosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__turnos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('Tur_HorarioInicial')->required();
			$table->string('Tur_HoraFinal')->required();
			$table->string('Tur_Descripcion');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__turnos');
	}

}
