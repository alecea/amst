<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_ArmasTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__armas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('Arm_Origen', 50);
			$table->int('Arm_Longitud', 10);
			$table->int 10('Arm_LongitudCanion');
			$table->string('Arm_Calibre', 5);
			$table->float('Arm_Peso');
			$table->int('Arm_CapacidadMunicion', 10);
			$table->float('Arm_TipoDisparo', 53);
			$table->float('Arm_VelocidadInicial', 53);
			$table->string('Arm_Mira', 5);
			$table->int('Teq_Id', 10);
			$table->int('Arm_Estado', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__armas');
	}

}
