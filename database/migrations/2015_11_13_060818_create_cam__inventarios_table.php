<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_InventariosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__inventarios', function(Blueprint $table)
		{
			$table->increments('id');
			$table->int('Teq_Id', 10);
			$table->int('Inv_Cantidad', 10);
			$table->int('Com_Id', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__inventarios');
	}

}
