<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDes_TipoVehiculosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('des__tipo_vehiculos', function(Blueprint $table)
		{
			$table->increments('Tve_Id');
			$table->string('Tve_Descripcion', 150);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('des__tipo_vehiculos');
	}

}
