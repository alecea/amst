<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_TipoEquipamientosTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__tipo_equipamientos', function(Blueprint $table)
		{
			$table->increments('id');
			$table->varchar('Teq_Descripcion', 50);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__tipo_equipamientos');
	}

}
