<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCam_InspeccionsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cam__inspeccions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->datetime('Ins_FechaInspeccion', 23);
			$table->varchar('Ins_HoraInspeccion', 5);
			$table->int('Den_Id', 10);
			$table->int('Pat_Id', 10);
			$table->varchar('Ins_PersonaAuxiliada', 150);
			$table->varchar('Ins_Motivo', 2147483647);
			$table->varchar('Ins_Auxilio', 2147483647);
			$table->varchar('Ins_ApoyoVehicular', 50);
			$table->varchar('Ins_AccionRealizada', 2147483647);
			$table->varchar('Ins_Evento', 2147483647);
			$table->varchar('Ins_AgentePnc', 2147483647);
			$table->varchar('Ins_ProductoDecomisado', 2147483647);
			$table->int('Aop_Id', 10);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cam__inspeccions');
	}

}
