@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_AccionOperativas</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camAccionOperativas.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camAccionOperativas->isEmpty())
                <div class="well text-center">No Cam_AccionOperativas found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Aop Tipo</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camAccionOperativas as $camAccionOperativa)
                        <tr>
                            <td>{!! $camAccionOperativa->Aop_Tipo !!}</td>
                            <td>
                                <a href="{!! route('camAccionOperativas.edit', [$camAccionOperativa->Aop_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camAccionOperativas.delete', [$camAccionOperativa->Aop_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_AccionOperativa?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
