@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camAccionOperativas.store']) !!}

        @include('camAccionOperativas.fields')

    {!! Form::close() !!}
</div>
@endsection
