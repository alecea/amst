@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camAccionOperativa, ['route' => ['camAccionOperativas.update', $camAccionOperativa->Aop_Id], 'method' => 'patch']) !!}

        @include('camAccionOperativas.fields')

    {!! Form::close() !!}
</div>
@endsection
