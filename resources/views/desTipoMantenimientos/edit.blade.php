@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($desTipoMantenimiento, ['route' => ['desTipoMantenimientos.update', $desTipoMantenimiento->Tma_Id], 'method' => 'patch']) !!}

        @include('desTipoMantenimientos.fields')

    {!! Form::close() !!}
</div>
@endsection
