@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Des_TipoMantenimientos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('desTipoMantenimientos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($desTipoMantenimientos->isEmpty())
                <div class="well text-center">No Des_TipoMantenimientos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Tma Descripcion</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($desTipoMantenimientos as $desTipoMantenimiento)
                        <tr>
                            <td>{!! $desTipoMantenimiento->Tma_Descripcion !!}</td>
                            <td>
                                <a href="{!! route('desTipoMantenimientos.edit', [$desTipoMantenimiento->Tma_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('desTipoMantenimientos.delete', [$desTipoMantenimiento->Tma_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Des_TipoMantenimiento?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
