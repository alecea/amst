@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'desTipoMantenimientos.store']) !!}

        @include('desTipoMantenimientos.fields')

    {!! Form::close() !!}
</div>
@endsection
