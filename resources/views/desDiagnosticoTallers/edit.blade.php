@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($desDiagnosticoTaller, ['route' => ['desDiagnosticoTallers.update', $desDiagnosticoTaller->Dta_Id], 'method' => 'patch']) !!}

        @include('desDiagnosticoTallers.fields')

    {!! Form::close() !!}
</div>
@endsection
