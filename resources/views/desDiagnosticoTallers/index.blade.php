@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Des_DiagnosticoTallers</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('desDiagnosticoTallers.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($desDiagnosticoTallers->isEmpty())
                <div class="well text-center">No Des_DiagnosticoTallers found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Emp Id</th>
			<th>Dta Fechadiagnostico</th>
			<th>Dta Diagnostico</th>
			<th>Tal Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($desDiagnosticoTallers as $desDiagnosticoTaller)
                        <tr>
                            <td>{!! $desDiagnosticoTaller->Emp_Id !!}</td>
					<td>{!! $desDiagnosticoTaller->Dta_FechaDiagnostico !!}</td>
					<td>{!! $desDiagnosticoTaller->Dta_Diagnostico !!}</td>
					<td>{!! $desDiagnosticoTaller->Tal_Id !!}</td>
                            <td>
                                <a href="{!! route('desDiagnosticoTallers.edit', [$desDiagnosticoTaller->Dta_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('desDiagnosticoTallers.delete', [$desDiagnosticoTaller->Dta_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Des_DiagnosticoTaller?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
