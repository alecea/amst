<!--- Emp Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Id', 'Emp Id:') !!}
    {!! Form::text('Emp_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Dta Fechadiagnostico Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Dta_FechaDiagnostico', 'Dta Fechadiagnostico:') !!}
    {!! Form::text('Dta_FechaDiagnostico', null, ['class' => 'form-control']) !!}
</div>

<!--- Dta Diagnostico Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Dta_Diagnostico', 'Dta Diagnostico:') !!}
    {!! Form::text('Dta_Diagnostico', null, ['class' => 'form-control']) !!}
</div>

<!--- Tal Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Tal_Id', 'Tal Id:') !!}
    {!! Form::text('Tal_Id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
