@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'desDiagnosticoTallers.store']) !!}

        @include('desDiagnosticoTallers.fields')

    {!! Form::close() !!}
</div>
@endsection
