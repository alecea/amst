@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camTurno, ['route' => ['camTurnos.update', $camTurno->Tur_Id], 'method' => 'patch']) !!}

        @include('camTurnos.fields')

    {!! Form::close() !!}
</div>
@endsection
