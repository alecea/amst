@extends('amst.templates.mainSystems')

@section('title')
.:: Sistema de CAM - Crear ::.
@stop

@section('logoGoTo')
{!! url('/cam') !!}?system=cam
@stop

@section('content')
<div class="span12">
    <!-- BEGIN SAMPLE FORMPORTLET-->
    @include('common.errors')
    <div class="widget green">
        <div class="widget-title">
            <h4><i class="icon-reorder"></i>Asignación de turnos</h4>
            <span class="tools">
            <a href="javascript:;" class="icon-chevron-down"></a>
            <a href="javascript:;" class="icon-remove"></a>
            </span>
        </div>
        <div class="widget-body">
            <!-- BEGIN FORM-->


    {!! Form::open(['route' => 'camTurnos.store']) !!}

        @include('amst.cam.camTurnos.fields')

    {!! Form::close() !!}
                <!-- END FORM-->
            </div>
        </div>
        <!-- END SAMPLE FORM PORTLET-->
    </div>

@endsection
