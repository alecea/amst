<!--- Tur Horarioinicial Field --->
<div class="control-group">
  {!! Form::label('Tur_HorarioInicial', 'Horario inicial:',['class' => 'control-label'] ) !!}
<div class="controls">
  {!! Form::input('time', 'Tur_HorarioInicial', null, ['class' => 'form-control', 'placeholder' => 'Time']) !!}
</div>
</div>

<!--- Tur Horafinal Field --->
<div class="control-group">
    {!! Form::label('Tur_HoraFinal', 'Horario final:',['class' => 'control-label'] ) !!}
<div class="controls">
    {!! Form::input('time', 'Tur_HoraFinal', null, ['class' => 'form-control', 'placeholder' => 'Time']) !!}
</div>
</div>

<!--- Tur Descripcion Field --->
<div class="control-group">
    {!! Form::label('Tur_Descripcion', 'Descripción:') !!}
<div class="controls">
    {!! Form::text('Tur_Descripcion', null, ['class' => 'input-xxlarge']) !!}
</div>
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary']) !!}
</div>
