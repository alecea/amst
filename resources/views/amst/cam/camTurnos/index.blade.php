@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_Turnos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camTurnos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camTurnos->isEmpty())
                <div class="well text-center">No Cam_Turnos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Tur Horarioinicial</th>
			<th>Tur Horafinal</th>
			<th>Tur Descripcion</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camTurnos as $camTurno)
                        <tr>
                            <td>{!! $camTurno->Tur_HorarioInicial !!}</td>
					<td>{!! $camTurno->Tur_HoraFinal !!}</td>
					<td>{!! $camTurno->Tur_Descripcion !!}</td>
                            <td>
                                <a href="{!! route('camTurnos.edit', [$camTurno->Tur_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camTurnos.delete', [$camTurno->Tur_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_Turno?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
