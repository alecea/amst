@extends('amst.templates.mainSystems')

@section('title')
.:: Sistema de CAM ::.
@stop

@section('logoGoTo')
{!! url('/cam') !!}?system=cam
@stop

@section('content')

<!-- BEGIN SIDEBAR MENU -->
@include('amst.templates.horizontalMenu.adminHrMenu')
<!-- END SIDEBAR MENU -->

<img src="{{ asset('/amst/img/logogrande.png') }}" width=1000  class="center">

@endsection
