@extends('amst.templates.mainHome')

@section('title')
.:: Inicio ::.
@stop

@section('login-wrap')
<div class="metro single-size gray">
</div>
<div class="metro single-size red" onclick="location.href='{!! url('/clinica') !!}?system=clinica'">
  <i class="icon-stethoscope"></i>
  <span>Clinica</span>
</div>
<div class="metro single-size blue" onclick="location.href='{!! url('/cam') !!}?system=cam'">
  <i class="icon-bell"></i>
  <span>CAM</span>
</div>
<div class="metro single-size green" onclick="location.href='{!! url('/transporte') !!}?system=transporte'">
  <i class="icon-truck"></i>
  <span>Transporte</span>
</div>
<div class="metro single-size purple" onclick="location.href='{!! url('/') !!}'">
  <i class="icon-lock"></i>
  <span>Cerrar Sesion</span>
</div>
@endsection
