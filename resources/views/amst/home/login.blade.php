@extends('amst.templates.mainHome')

@section('title')
.:: Login ::.
@stop

@section('login-wrap')
<div class="metro single-size red">
  <div class="locked">
    <i class="icon-lock"></i>
    <span>Iniciar sesión</span>
  </div>
</div>
<div class="metro double-size green">
  <form action="#">
    <div class="input-append lock-input">
      <input name="user" type="text" class="" id="user" placeholder="Usuario">
    </div>
  </form>
</div>
<div class="metro double-size yellow">
  <form action="#">
    <div class="input-append lock-input">
      <input type="password" class="" placeholder="Contraseña">
    </div>
  </form>
</div>
<div class="metro single-size terques login">
  <form action="{!! url('/home') !!}">
    <button class="btn login-btn">
      Ingresar&nbsp;<i class=" icon-long-arrow-right"></i>
    </button>
  </form>
</div>
<div class="login-footer">
  <div class="remember-hint pull-left">
    <input type="checkbox" id="">&nbsp;Recordar
  </div>
  <div class="forgot-hint pull-right">
    <a id="forget-password" class="" href="#">¿Olvidó su contraseña?</a>
  </div>
</div>
@endsection
