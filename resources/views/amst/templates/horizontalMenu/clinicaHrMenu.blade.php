<!--BEGIN METRO STATES-->
<div class="metro-nav">
    <div class="metro-nav-block nav-block-orange">
        <a data-original-title="" href="{!! url('/clinica/expediente/create') !!}">
            <i class="icon-archive"></i>
            <div class="info">Expediente</div>
            <div class="status">Nuevo Expediente</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-yellow">
        <a data-original-title="" href="#">
            <i class="icon-user-md"></i>
            <div class="info">Consulta</div>
            <div class="status">Nueva Consulta</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-grey">
        <a data-original-title="" href="#">
            <i class="icon-file-text-alt"></i>
            <div class="info">Examenes</div>
            <div class="status">Lista de Examenes</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-blue double">
        <a data-original-title="" href="#">
            <i class="icon-medkit"></i>
            <div class="info">Receta</div>
            <div class="status">Nueva Receta</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-red">
        <a data-original-title="" href="#">
            <i class="icon-list-alt"></i>
            <div class="info">Inventario</div>
            <div class="status">Nuevo Producto</div>
        </a>
    </div>
</div>

<div class="space10"></div>
<!--END METRO STATES-->
