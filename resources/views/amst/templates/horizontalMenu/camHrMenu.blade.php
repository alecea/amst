<!--BEGIN METRO STATES-->
<div class="metro-nav">
    <div class="metro-nav-block nav-block-yellow double">
        <a data-original-title="" href="#">
            <i class="icon-gears (alias)"></i>
            <div class="info">Equipamiento</div>
            <div class="status">Nuevo</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-grey">
        <a data-original-title="" href="#">
            <i class="icon-calendar"></i>
            <div class="info">Patrullaje</div>
            <div class="status">Nuevo</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-blue">
        <a data-original-title="" href="#">
            <i class="icon-pencil"></i>
            <div class="info">Denuncia</div>
            <div class="status">Nuevo</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-green">
        <a data-original-title="" href="cam_asignar_turno.html">
            <i class="icon-archive"></i>
            <div class="info">Reportes</div>
            <div class="status">Consultar</div>
        </a>
    </div>

</div>

<div class="space10"></div>
<!--END METRO STATES-->
