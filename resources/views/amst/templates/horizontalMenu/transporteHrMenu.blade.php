<!--BEGIN METRO STATES-->
<div class="metro-nav">
    <div class="metro-nav-block nav-block-orange">
        <a data-original-title="" href="#">
            <i class="icon-truck"></i>
            <div class="info">Vehiculos</div>
            <div class="status">Nuevo vehiculo</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-yellow">
        <a data-original-title="" href="#">
            <i class="icon-plane"></i>
            <div class="info">Tipo</div>
            <div class="status">Nuevo tipo</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-grey">
        <a data-original-title="" href="#">
            <i class="icon-file-text-alt"></i>
            <div class="info">Estado </div>
            <div class="status">Mantenimientos</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-blue double">
        <a data-original-title="" href="#">
            <i class="icon-pencil"></i>
            <div class="info">Rutas</div>
            <div class="status">Nueva Ruta</div>
        </a>
    </div>
    <div class="metro-nav-block nav-block-red">
        <a data-original-title="" href="#">
            <i class="icon-list-alt"></i>
            <div class="info">Viaticos</div>
            <div class="status">Nueva Solicitud</div>
        </a>
    </div>
</div>

<div class="space10"></div>
<!--END METRO STATES-->
