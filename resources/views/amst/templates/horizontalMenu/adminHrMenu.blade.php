{{--*/ $value = Session::get('system', 'default') /*--}}
@if ($value === 'cam')
    @include('amst.templates.horizontalMenu.camHrMenu')
@elseif ($value === 'clinica')
    @include('amst.templates.horizontalMenu.clinicaHrMenu')
@elseif ($value === 'transporte')
    @include('amst.templates.horizontalMenu.transporteHrMenu')
@else
<div class="metro-nav">
  <div class="space10"></div>
</div>
@endif
