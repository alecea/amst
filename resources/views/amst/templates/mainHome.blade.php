<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
  <meta charset="utf-8" />
  <meta content="" name="author" />
  <meta content="" name="description" />
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <link href="{{ asset('/amst/assets/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/amst/assets/bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" />
  <link href="{{ asset('/amst/assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
  <link href="{{ asset('/amst/css/style.css') }}" rel="stylesheet" />
  <link href="{{ asset('/amst/css/style-responsive.css') }}" rel="stylesheet" />
  <link href="{{ asset('/amst/css/style-default.css') }}" rel="stylesheet" id="style_color" />
  <title>@yield('title', '.:: Alcaldia Municipal de Santa Tecla System ::.')</title>
</head>
<!-- END HEAD -->
<!-- BEGIN BODY -->
<body class="lock">
  <div class="lock-header">
    <!-- BEGIN LOGO -->
    <a class="center" id="logo" href="#">
      <img class="center" alt="logo" src="{{ asset('/amst/img/logogrande.png') }}" width="950" >
    </a>
    <!-- END LOGO -->
  </div>
  <div class="login-wrap">
    @yield('login-wrap')
  </div>
</body>
<!-- END BODY -->
</html>
