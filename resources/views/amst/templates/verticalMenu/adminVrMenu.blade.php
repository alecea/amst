{{--*/ $value = Session::get('system', 'default') /*--}}
@if ($value === 'cam')
    @include('amst.templates.verticalMenu.camVrMenu')
@elseif ($value === 'clinica')
    @include('amst.templates.verticalMenu.clinicaVrMenu')
@elseif ($value === 'transporte')
    @include('amst.templates.verticalMenu.transporteVrMenu')
@else
@endif
