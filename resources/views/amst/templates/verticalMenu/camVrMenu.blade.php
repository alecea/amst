<!-- BEGIN SIDEBAR MENU -->
<ul class="sidebar-menu">
<li class="sub-menu active">
        <a class="" href="{!! url('/cam') !!}?system=cam">
            <i class="icon-home"></i>
            <span>Inicio</span>
        </a>
    </li>
<!-- OPCIÓN 1 MENÚ -->
<li class="sub-menu">
        <a href="javascript:;" class="">
            <i class="icon-gears"></i>
            <span>Equipamiento</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub">
            <li><a class="" href="{!! url('/camArmas/create') !!}">
            <span>Ingresar Arma</span></a></li>
            <li><a class="" href="#">
            <span>Asignar Equipamiento</span></a></li>
        </ul>
    </li>

<!-- OPCIÓN 2 MENÚ -->
    <li class="sub-menu">
        <a href="javascript:;" class="">
            <i class="icon-calendar"></i>
            <span>Patrullaje</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub">
            <li><a class="" href="#">
            <span>Detalle Patrullaje</span></a></li>
            <li><a class="" href="{!! url('/camTurnos/create') !!}">
            <span>Turnos</span></a></li>
            <li><a class="" href="#">
            <span>Zona</span></a></li>
        </ul>
    </li>

<!-- OPCIÓN 3 MENÚ -->

    <li class="sub-menu">
        <a href="javascript:;" class="">
            <i class="icon-pencil"></i>
            <span>Denuncia</span>
            <span class="arrow"></span>
        </a>
        <ul class="sub">
            <li><a class="" href="#">
            <span>Recepción Denuncia</span></a></li>
            <li><a class="" href="{!! url('/camInspeccions/create') !!}">
            <span>Formulario de Inspección</span></a></li>
            <li><a class="" href="#">
            <span>Registro de Multa</span></a></li>
            <li><a class="" href="#">
            <span>Tipo de Infracción</span></a></li>
        </ul>
    </li>

<!-- OPCIÓN 4 MENÚ -->

    <li class="sub-menu">
            <a href="javascript:;" class="">
                <i class="icon-archive"></i>
                <span>Reportes</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub">
                <li><a class="" href="#">
                <span>Registro de Multas</span></a></li>
                <li><a class="" href="#">
                <span>Inventario de Armas</span></a></li>
                <li><a class="" href="#">
                <span>Inventario de Equipo</span></a></li>
            </ul>
        </li>
</ul>
<!-- END SIDEBAR MENU -->
