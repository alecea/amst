<!-- BEGIN SIDEBAR MENU -->
<ul class="sidebar-menu">
  <li class="sub-menu active"> <a class="" href="{!! url('/transporte') !!}?system=transporte"> <i class="icon-home"></i> <span>Inicio</span> </a> </li>
  <li class="sub-menu"> <a href="javascript:;" class=""> <i class="icon-truck"></i> <span>Vehiculos</span> <span class="arrow"></span> </a>
    <ul class="sub">
      <li><a class="" href="#"> <i class="icon-plus"></i> <span>Crear Vehiculo</span></a></li>
      <li><a class="" href="#"> <i class="icon-search"></i> <span>Buscar Vehiculo</span></a></li>
      <li><a class="" href="#"> <i class="icon-refresh"></i> <span>Actualizar Vehiculo</span></a></li>
    </ul>
  </li>
  <li class="sub-menu"> <a href="javascript:;" class=""> <i class="icon-fighter-jet"></i> <span>Tipo de Vehiculos</span> <span class="arrow"></span> </a>
    <ul class="sub">
      <li><a class="" href="#"> <i class="icon-plus"></i> <span>Crear Tipo Vehiculo</span></a></li>
      <li><a class="" href="#"> <i class="icon-search"></i> <span>Buscar Tipo Vehiculo</span></a></li>
      <li><a class="" href="#"> <i class="icon-refresh"></i> <span>Actualizar Tipo Vehiculo</span></a></li>
    </ul>
  </li>
  <li class="sub-menu"> <a href="javascript:;" class=""> <i class="icon-plane"></i> <span>Estado de Vehiculos</span> <span class="arrow"></span> </a>
    <ul class="sub">
      <li><a class="" href="#"> <i class="icon-plus"></i> <span>Crear Estado Vehiculo</span></a></li>
      <li><a class="" href="#"> <i class="icon-search"></i> <span>Buscar Estado Vehiculo</span></a></li>
      <li><a class="" href="#"> <i class="icon-refresh"></i> <span>Actualizar Estado Vehiculo</span></a></li>
    </ul>
  </li>
  <li class="sub-menu"> <a href="javascript:;" class=""> <i class="icon-pencil"></i> <span>Rutas</span> <span class="arrow"></span> </a>
    <ul class="sub">
      <li><a class="" href="#"> <i class="icon-plus"></i> <span>Crear Rutas</span></a></li>
      <li><a class="" href="#"> <i class="icon-search"></i> <span>Buscar Rutas</span></a></li>
      <li><a class="" href="#"> <i class="icon-refresh"></i> <span>Actualizar Rutas</span></a></li>
    </ul>
  </li>
  <li class="sub-menu"> <a href="javascript:;" class=""> <i class="icon-hospital"></i> <span>Manto. Vehiculo</span> <span class="arrow"></span> </a>
    <ul class="sub">
      <li><a class="" href="#"> <i class="icon-plus"></i> <span>Ingresar Vehiculo a Mantenimiento</span></a></li>
      <li><a class="" href="#"> <i class="icon-search"></i> <span>Buscar Vehiculo en Mantenimiento</span></a></li>
      <li><a class="" href="#"> <i class="icon-refresh"></i> <span>Actualizar Vehiculo en Mantenimiento</span></a></li>
    </ul>
  </li>
  <li class="sub-menu"> <a href="javascript:;" class=""> <i class="icon-user"></i> <span>Tipo Mantenimiento</span> <span class="arrow"></span> </a>
    <ul class="sub">
      <li><a class="" href="#"> <i class="icon-plus"></i> <span>Ingresar Tipo de Mantenimiento</span></a></li>
      <li><a class="" href="#"> <i class="icon-search"></i> <span>Buscar Tipo de Mantenimiento</span></a></li>
      <li><a class="" href="#"> <i class="icon-refresh"></i> <span>Actualizar Tipo de Mantenimiento</span></a></li>
    </ul>

  </li>
  <li class="sub-menu"> <a href="javascript:;" class=""> <i class="icon-usd"></i> <span>Vale de gasolina</span> <span class="arrow"></span> </a>
    <ul class="sub">
      <li><a class="" href="#"> <i class="icon-plus"></i> <span>Ingresar Vale</span></a></li>
      <li><a class="" href="#"> <i class="icon-search"></i> <span>Buscar Vale</span></a></li>
      <li><a class="" href="#"> <i class="icon-refresh"></i> <span>Actualizar Vale</span></a></li>
    </ul>
  </li>
</ul>
<!-- END SIDEBAR MENU -->
