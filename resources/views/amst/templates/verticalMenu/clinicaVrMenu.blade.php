<!-- BEGIN SIDEBAR MENU -->
 <ul class="sidebar-menu">
     <li class="sub-menu active">
         <a class="" href="{!! url('/clinica') !!}?system=clinica">
             <i class="icon-home"></i>
             <span>Inicio</span>
         </a>
     </li>

     <li class="sub-menu">
         <a href="javascript:;" class="">
             <i class="icon-archive"></i>
             <span>Expediente</span>
             <span class="arrow"></span>
         </a>
         <ul class="sub">
             <li><a class="" href="{!! url('/clinica/expediente/create') !!}">Crear Expediente</a></li>
             <li><a class="" href="#">Buscar y Editar Expediente</a></li>
         </ul>
     </li>
     <li class="sub-menu">
         <a href="javascript:;" class="">
             <i class="icon-stethoscope"></i>
             <span>Consulta</span>
             <span class="arrow"></span>
         </a>
         <ul class="sub">
            <li><a class="" href="#">Nueva Consulta</a></li>
         </ul>
     </li>
     <li class="sub-menu">
         <a href="javascript:;" class="">
             <i class="icon-file-text-alt"></i>
             <span>Examenes</span>
             <span class="arrow"></span>
         </a>
         <ul class="sub">
             <li><a class="" href="#">Lista de Examenes</a></li>

         </ul>
     </li>


     <li class="sub-menu">
         <a href="javascript:;" class="">
             <i class="icon-medkit"></i>
             <span>Receta</span>
             <span class="arrow"></span>
         </a>
         <ul class="sub">
             <li><a class="" href="#">Crear Receta</a></li>
         </ul>
     </li>
     <li class="sub-menu">
         <a href="javascript:;" class="">
             <i class="icon-list-alt"></i>
             <span>Inventario</span>
             <span class="arrow"></span>
         </a>
         <ul class="sub">
             <li><a class="" href="#">Producto</a></li>
   <li><a class="" href="#">Buscar Medicamento</a></li>
         </ul>
     </li>

     <li>
         <a class="" href="{!! url('/') !!}">
           <i class="icon-user"></i>
           <span>Login Page</span>
         </a>
     </li>
 </ul>
<!-- END SIDEBAR MENU -->
