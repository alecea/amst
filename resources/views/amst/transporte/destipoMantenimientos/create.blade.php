@extends('amst.templates.mainSystems')

@section('title')
.:: Sistema de Clinica - Crear ::.
@stop

@section('logoGoTo')
{!! url('/clinica') !!}?system=clinica
@stop

@section('content')
<div class="container">

    @include('common.errors')
    <div class="widget green">
        <div class="widget-title">
            <h4><i class="icon-reorder"></i>Asignacion de turnos</h4>
            <span class="tools">
            <a href="javascript:;" class="icon-chevron-down"></a>
            <a href="javascript:;" class="icon-remove"></a>
            </span>
        </div>
        <div class="widget-body">
    {!! Form::open(['route' => 'desTipoMantenimientos.store']) !!}

        @include('desTipoMantenimientos.fields')

    {!! Form::close() !!}
  </div>
</div>
@endsection
