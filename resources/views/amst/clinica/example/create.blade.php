@extends('amst.templates.mainSystems')

@section('title')
.:: Sistema de Clinica - Crear ::.
@stop

@section('logoGoTo')
{!! url('/clinica') !!}?system=clinica
@stop

@section('content')

<div class="span12">
    <!-- BEGIN SAMPLE FORMPORTLET-->
    <div class="widget green">
        <div class="widget-title">
            <h4><i class="icon-reorder"></i>Creación de Expediente</h4>
            <span class="tools">
            <a href="javascript:;" class="icon-chevron-down"></a>
            <a href="javascript:;" class="icon-remove"></a>
            </span>
        </div>
        <div class="widget-body">
            <!-- BEGIN FORM-->
            <form action="#" class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">Numero de Expediente</label>
                    <div class="controls">
                        <input type="text" placeholder="Numero" class="input-small" />
                        <span class="help-inline"></span>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Fecha de Creación</label>

                    <div class="controls">
                        <div class="input-append date" id="dpYears" data-date="12-02-2015"
                             data-date-format="dd-mm-yyyy" data-date-viewmode="years">
                            <input class="m-ctrl-medium" size="16" type="text" value="01-01-2015" readonly>
                            <span class="add-on"><i class="icon-calendar"></i></span>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Enfermedad</label>
                    <div class="controls">
                        <textarea class="input-xxlarge" rows="7"></textarea>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label">Fecha de Diagnostico</label>

                    <div class="controls">
                        <input id="dp1" type="text" value="12-02-2012" size="16" class="m-ctrl-medium">
                    </div>
                </div>

<div class="control-group">
                    <label class="control-label">Diagnostico</label>
                    <div class="controls">
                        <textarea class="input-xxlarge" rows="3"></textarea>
                    </div>
                </div>

<div class="control-group">
                    <label class="control-label">Operaciones</label>
                    <div class="controls">
                        <textarea class="input-xxlarge" rows="3"></textarea>
                    </div>
                </div>

<div class="control-group">
                    <label class="control-label">Tratamiento</label>
                    <div class="controls">
                        <textarea class="input-xxlarge" rows="5"></textarea>
                    </div>
                </div>

<div class="form-actions">
                <button type="submit" class="btn btn-success">Submit</button>
<button type="button" class="btn btn-success">Limpiar</button>
                <button type="button" class="btn">Cancel</button>
            </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
</div>

@endsection
