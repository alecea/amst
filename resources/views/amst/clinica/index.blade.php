@extends('amst.templates.mainSystems')

@section('title')
.:: Sistema de Clinica ::.
@stop

@section('logoGoTo')
{!! url('/clinica') !!}?system=clinica
@stop

@section('content')

<!-- BEGIN SIDEBAR MENU -->
@include('amst.templates.horizontalMenu.adminHrMenu')
<!-- END SIDEBAR MENU -->

<img src="{{ asset('/amst/img/logogrande.png') }}" width=1000  class="center">

@endsection
