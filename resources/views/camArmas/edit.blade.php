@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camArmas, ['route' => ['camArmas.update', $camArmas->Arm_Id], 'method' => 'patch']) !!}

        @include('camArmas.fields')

    {!! Form::close() !!}
</div>
@endsection
