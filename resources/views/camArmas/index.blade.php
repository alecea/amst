@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_Armas</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camArmas.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camArmas->isEmpty())
                <div class="well text-center">No Cam_Armas found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Arm Origen</th>
			<th>Arm Longitud</th>
			<th>Arm Longitudcanion</th>
			<th>Arm Calibre</th>
			<th>Arm Peso</th>
			<th>Arm Capacidadmunicion</th>
			<th>Arm Tipodisparo</th>
			<th>Arm Velocidadinicial</th>
			<th>Arm Mira</th>
			<th>Teq Id</th>
			<th>Arm Estado</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camArmas as $camArmas)
                        <tr>
                            <td>{!! $camArmas->Arm_Origen !!}</td>
					<td>{!! $camArmas->Arm_Longitud !!}</td>
					<td>{!! $camArmas->Arm_LongitudCanion !!}</td>
					<td>{!! $camArmas->Arm_Calibre !!}</td>
					<td>{!! $camArmas->Arm_Peso !!}</td>
					<td>{!! $camArmas->Arm_CapacidadMunicion !!}</td>
					<td>{!! $camArmas->Arm_TipoDisparo !!}</td>
					<td>{!! $camArmas->Arm_VelocidadInicial !!}</td>
					<td>{!! $camArmas->Arm_Mira !!}</td>
					<td>{!! $camArmas->Teq_Id !!}</td>
					<td>{!! $camArmas->Arm_Estado !!}</td>
                            <td>
                                <a href="{!! route('camArmas.edit', [$camArmas->Arm_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camArmas.delete', [$camArmas->Arm_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_Armas?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
