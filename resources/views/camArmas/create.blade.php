@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camArmas.store']) !!}

        @include('camArmas.fields')

    {!! Form::close() !!}
</div>
@endsection
