<!--- Arm Origen Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_Origen', 'Arm Origen:') !!}
    {!! Form::text('Arm_Origen', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Longitud Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_Longitud', 'Arm Longitud:') !!}
    {!! Form::text('Arm_Longitud', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Longitudcanion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_LongitudCanion', 'Arm Longitudcanion:') !!}
    {!! Form::text('Arm_LongitudCanion', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Calibre Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_Calibre', 'Arm Calibre:') !!}
    {!! Form::text('Arm_Calibre', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Peso Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_Peso', 'Arm Peso:') !!}
    {!! Form::text('Arm_Peso', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Capacidadmunicion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_CapacidadMunicion', 'Arm Capacidadmunicion:') !!}
    {!! Form::text('Arm_CapacidadMunicion', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Tipodisparo Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_TipoDisparo', 'Arm Tipodisparo:') !!}
    {!! Form::text('Arm_TipoDisparo', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Velocidadinicial Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_VelocidadInicial', 'Arm Velocidadinicial:') !!}
    {!! Form::text('Arm_VelocidadInicial', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Mira Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_Mira', 'Arm Mira:') !!}
    {!! Form::text('Arm_Mira', null, ['class' => 'form-control']) !!}
</div>

<!--- Teq Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Teq_Id', 'Teq Id:') !!}
    {!! Form::text('Teq_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Estado Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_Estado', 'Arm Estado:') !!}
    {!! Form::text('Arm_Estado', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
