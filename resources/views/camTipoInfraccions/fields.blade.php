<!--- Tin Articulo Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Tin_Articulo', 'Tin Articulo:') !!}
    {!! Form::text('Tin_Articulo', null, ['class' => 'form-control']) !!}
</div>

<!--- Tin Descripcion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Tin_Descripcion', 'Tin Descripcion:') !!}
    {!! Form::text('Tin_Descripcion', null, ['class' => 'form-control']) !!}
</div>

<!--- Tin Monto Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Tin_Monto', 'Tin Monto:') !!}
    {!! Form::text('Tin_Monto', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
