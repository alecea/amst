@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camTipoInfraccion, ['route' => ['camTipoInfraccions.update', $camTipoInfraccion->Tin_Id], 'method' => 'patch']) !!}

        @include('camTipoInfraccions.fields')

    {!! Form::close() !!}
</div>
@endsection
