@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_TipoInfraccions</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camTipoInfraccions.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camTipoInfraccions->isEmpty())
                <div class="well text-center">No Cam_TipoInfraccions found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Tin Articulo</th>
			<th>Tin Descripcion</th>
			<th>Tin Monto</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camTipoInfraccions as $camTipoInfraccion)
                        <tr>
                            <td>{!! $camTipoInfraccion->Tin_Articulo !!}</td>
					<td>{!! $camTipoInfraccion->Tin_Descripcion !!}</td>
					<td>{!! $camTipoInfraccion->Tin_Monto !!}</td>
                            <td>
                                <a href="{!! route('camTipoInfraccions.edit', [$camTipoInfraccion->Tin_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camTipoInfraccions.delete', [$camTipoInfraccion->Tin_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_TipoInfraccion?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
