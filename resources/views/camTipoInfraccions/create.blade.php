@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camTipoInfraccions.store']) !!}

        @include('camTipoInfraccions.fields')

    {!! Form::close() !!}
</div>
@endsection
