@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_AsigEquipamientos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camAsigEquipamientos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camAsigEquipamientos->isEmpty())
                <div class="well text-center">No Cam_AsigEquipamientos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Emp Id</th>
			<th>Aeq Fecha</th>
			<th>Aeq Hora</th>
			<th>Aeq Observaciones</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camAsigEquipamientos as $camAsigEquipamiento)
                        <tr>
                            <td>{!! $camAsigEquipamiento->Emp_Id !!}</td>
					<td>{!! $camAsigEquipamiento->Aeq_Fecha !!}</td>
					<td>{!! $camAsigEquipamiento->Aeq_Hora !!}</td>
					<td>{!! $camAsigEquipamiento->Aeq_Observaciones !!}</td>
                            <td>
                                <a href="{!! route('camAsigEquipamientos.edit', [$camAsigEquipamiento->Aeq_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camAsigEquipamientos.delete', [$camAsigEquipamiento->Aeq_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_AsigEquipamiento?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
