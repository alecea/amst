@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camAsigEquipamiento, ['route' => ['camAsigEquipamientos.update', $camAsigEquipamiento->Aeq_Id], 'method' => 'patch']) !!}

        @include('camAsigEquipamientos.fields')

    {!! Form::close() !!}
</div>
@endsection
