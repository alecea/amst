<!--- Emp Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Id', 'Emp Id:') !!}
    {!! Form::text('Emp_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Aeq Fecha Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Aeq_Fecha', 'Aeq Fecha:') !!}
    {!! Form::text('Aeq_Fecha', null, ['class' => 'form-control']) !!}
</div>

<!--- Aeq Hora Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Aeq_Hora', 'Aeq Hora:') !!}
    {!! Form::text('Aeq_Hora', null, ['class' => 'form-control']) !!}
</div>

<!--- Aeq Observaciones Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Aeq_Observaciones', 'Aeq Observaciones:') !!}
    {!! Form::text('Aeq_Observaciones', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
