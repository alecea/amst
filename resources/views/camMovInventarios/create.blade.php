@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camMovInventarios.store']) !!}

        @include('camMovInventarios.fields')

    {!! Form::close() !!}
</div>
@endsection
