@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camMovInventario, ['route' => ['camMovInventarios.update', $camMovInventario->Min_Id], 'method' => 'patch']) !!}

        @include('camMovInventarios.fields')

    {!! Form::close() !!}
</div>
@endsection
