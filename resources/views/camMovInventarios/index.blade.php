@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_MovInventarios</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camMovInventarios.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camMovInventarios->isEmpty())
                <div class="well text-center">No Cam_MovInventarios found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Min Fecha</th>
			<th>Min Tipomov</th>
			<th>Com Id</th>
			<th>Equ Id</th>
			<th>Arm Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camMovInventarios as $camMovInventario)
                        <tr>
                            <td>{!! $camMovInventario->Min_Fecha !!}</td>
					<td>{!! $camMovInventario->Min_TipoMov !!}</td>
					<td>{!! $camMovInventario->Com_Id !!}</td>
					<td>{!! $camMovInventario->Equ_Id !!}</td>
					<td>{!! $camMovInventario->Arm_Id !!}</td>
                            <td>
                                <a href="{!! route('camMovInventarios.edit', [$camMovInventario->Min_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camMovInventarios.delete', [$camMovInventario->Min_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_MovInventario?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
