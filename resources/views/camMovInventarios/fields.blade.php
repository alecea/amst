<!--- Min Fecha Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Min_Fecha', 'Min Fecha:') !!}
    {!! Form::text('Min_Fecha', null, ['class' => 'form-control']) !!}
</div>

<!--- Min Tipomov Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Min_TipoMov', 'Min Tipomov:') !!}
    {!! Form::text('Min_TipoMov', null, ['class' => 'form-control']) !!}
</div>

<!--- Com Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Com_Id', 'Com Id:') !!}
    {!! Form::text('Com_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Equ Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Equ_Id', 'Equ Id:') !!}
    {!! Form::text('Equ_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_Id', 'Arm Id:') !!}
    {!! Form::text('Arm_Id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
