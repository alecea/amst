@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Des_TipoVehiculos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('desTipoVehiculos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($desTipoVehiculos->isEmpty())
                <div class="well text-center">No Des_TipoVehiculos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Tve Descripcion</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($desTipoVehiculos as $desTipoVehiculo)
                        <tr>
                            <td>{!! $desTipoVehiculo->Tve_Descripcion !!}</td>
                            <td>
                                <a href="{!! route('desTipoVehiculos.edit', [$desTipoVehiculo->Tve_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('desTipoVehiculos.delete', [$desTipoVehiculo->Tve_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Des_TipoVehiculo?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
