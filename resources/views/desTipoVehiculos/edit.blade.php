@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($desTipoVehiculo, ['route' => ['desTipoVehiculos.update', $desTipoVehiculo->Tve_Id], 'method' => 'patch']) !!}

        @include('desTipoVehiculos.fields')

    {!! Form::close() !!}
</div>
@endsection
