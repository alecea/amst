@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'desTipoVehiculos.store']) !!}

        @include('desTipoVehiculos.fields')

    {!! Form::close() !!}
</div>
@endsection
