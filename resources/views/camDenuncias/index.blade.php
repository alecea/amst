@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_Denuncias</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camDenuncias.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camDenuncias->isEmpty())
                <div class="well text-center">No Cam_Denuncias found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Per Id</th>
			<th>Den Fecha</th>
			<th>Den Hora</th>
			<th>Aop Id</th>
			<th>Zon Id</th>
			<th>Den Direccion</th>
			<th>Den Observaciondenuncia</th>
			<th>Den Procede</th>
			<th>Den Justificacionprocedencia</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camDenuncias as $camDenuncia)
                        <tr>
                            <td>{!! $camDenuncia->Per_Id !!}</td>
					<td>{!! $camDenuncia->Den_Fecha !!}</td>
					<td>{!! $camDenuncia->Den_Hora !!}</td>
					<td>{!! $camDenuncia->Aop_Id !!}</td>
					<td>{!! $camDenuncia->Zon_Id !!}</td>
					<td>{!! $camDenuncia->Den_Direccion !!}</td>
					<td>{!! $camDenuncia->Den_ObservacionDenuncia !!}</td>
					<td>{!! $camDenuncia->Den_Procede !!}</td>
					<td>{!! $camDenuncia->Den_JustificacionProcedencia !!}</td>
                            <td>
                                <a href="{!! route('camDenuncias.edit', [$camDenuncia->Den_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camDenuncias.delete', [$camDenuncia->Den_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_Denuncia?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
