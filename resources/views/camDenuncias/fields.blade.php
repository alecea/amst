<!--- Per Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Per_Id', 'Per Id:') !!}
    {!! Form::text('Per_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Den Fecha Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Den_Fecha', 'Den Fecha:') !!}
    {!! Form::text('Den_Fecha', null, ['class' => 'form-control']) !!}
</div>

<!--- Den Hora Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Den_Hora', 'Den Hora:') !!}
    {!! Form::text('Den_Hora', null, ['class' => 'form-control']) !!}
</div>

<!--- Aop Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Aop_Id', 'Aop Id:') !!}
    {!! Form::text('Aop_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Zon Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Zon_Id', 'Zon Id:') !!}
    {!! Form::text('Zon_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Den Direccion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Den_Direccion', 'Den Direccion:') !!}
    {!! Form::text('Den_Direccion', null, ['class' => 'form-control']) !!}
</div>

<!--- Den Observaciondenuncia Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Den_ObservacionDenuncia', 'Den Observaciondenuncia:') !!}
    {!! Form::text('Den_ObservacionDenuncia', null, ['class' => 'form-control']) !!}
</div>

<!--- Den Procede Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Den_Procede', 'Den Procede:') !!}
    {!! Form::text('Den_Procede', null, ['class' => 'form-control']) !!}
</div>

<!--- Den Justificacionprocedencia Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Den_JustificacionProcedencia', 'Den Justificacionprocedencia:') !!}
    {!! Form::text('Den_JustificacionProcedencia', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
