@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camDenuncias.store']) !!}

        @include('camDenuncias.fields')

    {!! Form::close() !!}
</div>
@endsection
