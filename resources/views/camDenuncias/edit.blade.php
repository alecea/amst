@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camDenuncia, ['route' => ['camDenuncias.update', $camDenuncia->Den_Id], 'method' => 'patch']) !!}

        @include('camDenuncias.fields')

    {!! Form::close() !!}
</div>
@endsection
