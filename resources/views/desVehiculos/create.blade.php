@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'desVehiculos.store']) !!}

        @include('desVehiculos.fields')

    {!! Form::close() !!}
</div>
@endsection
