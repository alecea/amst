@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Des_Vehiculos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('desVehiculos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($desVehiculos->isEmpty())
                <div class="well text-center">No Des_Vehiculos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Veh Marca</th>
			<th>Veh Anniofabricacion</th>
			<th>Veh Nomatricula</th>
			<th>Tve Id</th>
			<th>Veh Annio</th>
			<th>Veh Modelo</th>
			<th>Veh Kilometraje</th>
			<th>Veh Color</th>
			<th>Veh Cilindraje</th>
			<th>Veh Nomotor</th>
			<th>Veh Nochasis</th>
			<th>Veh Fechaadquisicion</th>
			<th>Veh Actibo</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($desVehiculos as $desVehiculo)
                        <tr>
                            <td>{!! $desVehiculo->Veh_Marca !!}</td>
					<td>{!! $desVehiculo->Veh_AnnioFabricacion !!}</td>
					<td>{!! $desVehiculo->Veh_NoMatricula !!}</td>
					<td>{!! $desVehiculo->Tve_Id !!}</td>
					<td>{!! $desVehiculo->Veh_Annio !!}</td>
					<td>{!! $desVehiculo->Veh_Modelo !!}</td>
					<td>{!! $desVehiculo->Veh_Kilometraje !!}</td>
					<td>{!! $desVehiculo->Veh_Color !!}</td>
					<td>{!! $desVehiculo->Veh_Cilindraje !!}</td>
					<td>{!! $desVehiculo->Veh_NoMotor !!}</td>
					<td>{!! $desVehiculo->Veh_NoChasis !!}</td>
					<td>{!! $desVehiculo->Veh_FechaAdquisicion !!}</td>
					<td>{!! $desVehiculo->Veh_Actibo !!}</td>
                            <td>
                                <a href="{!! route('desVehiculos.edit', [$desVehiculo->Veh_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('desVehiculos.delete', [$desVehiculo->Veh_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Des_Vehiculo?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
