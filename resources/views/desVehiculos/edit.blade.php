@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($desVehiculo, ['route' => ['desVehiculos.update', $desVehiculo->Veh_Id], 'method' => 'patch']) !!}

        @include('desVehiculos.fields')

    {!! Form::close() !!}
</div>
@endsection
