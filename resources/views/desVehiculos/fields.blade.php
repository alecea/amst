<!--- Veh Marca Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_Marca', 'Veh Marca:') !!}
    {!! Form::text('Veh_Marca', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Anniofabricacion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_AnnioFabricacion', 'Veh Anniofabricacion:') !!}
    {!! Form::text('Veh_AnnioFabricacion', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Nomatricula Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_NoMatricula', 'Veh Nomatricula:') !!}
    {!! Form::text('Veh_NoMatricula', null, ['class' => 'form-control']) !!}
</div>

<!--- Tve Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Tve_Id', 'Tve Id:') !!}
    {!! Form::text('Tve_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Annio Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_Annio', 'Veh Annio:') !!}
    {!! Form::text('Veh_Annio', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Modelo Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_Modelo', 'Veh Modelo:') !!}
    {!! Form::text('Veh_Modelo', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Kilometraje Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_Kilometraje', 'Veh Kilometraje:') !!}
    {!! Form::text('Veh_Kilometraje', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Color Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_Color', 'Veh Color:') !!}
    {!! Form::text('Veh_Color', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Cilindraje Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_Cilindraje', 'Veh Cilindraje:') !!}
    {!! Form::text('Veh_Cilindraje', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Nomotor Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_NoMotor', 'Veh Nomotor:') !!}
    {!! Form::text('Veh_NoMotor', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Nochasis Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_NoChasis', 'Veh Nochasis:') !!}
    {!! Form::text('Veh_NoChasis', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Fechaadquisicion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_FechaAdquisicion', 'Veh Fechaadquisicion:') !!}
    {!! Form::text('Veh_FechaAdquisicion', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Actibo Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_Actibo', 'Veh Actibo:') !!}
    {!! Form::text('Veh_Actibo', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
