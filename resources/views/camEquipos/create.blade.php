@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camEquipos.store']) !!}

        @include('camEquipos.fields')

    {!! Form::close() !!}
</div>
@endsection
