@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camEquipo, ['route' => ['camEquipos.update', $camEquipo->Equ_Id], 'method' => 'patch']) !!}

        @include('camEquipos.fields')

    {!! Form::close() !!}
</div>
@endsection
