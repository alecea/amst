<!--- Equ Numeroserie Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Equ_NumeroSerie', 'Equ Numeroserie:') !!}
    {!! Form::text('Equ_NumeroSerie', null, ['class' => 'form-control']) !!}
</div>

<!--- Equ Descripcion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Equ_Descripcion', 'Equ Descripcion:') !!}
    {!! Form::text('Equ_Descripcion', null, ['class' => 'form-control']) !!}
</div>

<!--- Teq Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Teq_Id', 'Teq Id:') !!}
    {!! Form::text('Teq_Id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
