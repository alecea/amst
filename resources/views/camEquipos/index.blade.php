@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_Equipos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camEquipos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camEquipos->isEmpty())
                <div class="well text-center">No Cam_Equipos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Equ Numeroserie</th>
			<th>Equ Descripcion</th>
			<th>Teq Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camEquipos as $camEquipo)
                        <tr>
                            <td>{!! $camEquipo->Equ_NumeroSerie !!}</td>
					<td>{!! $camEquipo->Equ_Descripcion !!}</td>
					<td>{!! $camEquipo->Teq_Id !!}</td>
                            <td>
                                <a href="{!! route('camEquipos.edit', [$camEquipo->Equ_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camEquipos.delete', [$camEquipo->Equ_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_Equipo?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
