@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Des_EstadoVehiculos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('desEstadoVehiculos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($desEstadoVehiculos->isEmpty())
                <div class="well text-center">No Des_EstadoVehiculos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Eve Descripcion</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($desEstadoVehiculos as $desEstadoVehiculo)
                        <tr>
                            <td>{!! $desEstadoVehiculo->Eve_Descripcion !!}</td>
                            <td>
                                <a href="{!! route('desEstadoVehiculos.edit', [$desEstadoVehiculo->Eve_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('desEstadoVehiculos.delete', [$desEstadoVehiculo->Eve_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Des_EstadoVehiculo?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
