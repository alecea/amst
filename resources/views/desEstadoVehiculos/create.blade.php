@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'desEstadoVehiculos.store']) !!}

        @include('desEstadoVehiculos.fields')

    {!! Form::close() !!}
</div>
@endsection
