@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($desEstadoVehiculo, ['route' => ['desEstadoVehiculos.update', $desEstadoVehiculo->Eve_Id], 'method' => 'patch']) !!}

        @include('desEstadoVehiculos.fields')

    {!! Form::close() !!}
</div>
@endsection
