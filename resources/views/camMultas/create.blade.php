@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camMultas.store']) !!}

        @include('camMultas.fields')

    {!! Form::close() !!}
</div>
@endsection
