<!--- Mul Estado Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Mul_Estado', 'Mul Estado:') !!}
    {!! Form::text('Mul_Estado', null, ['class' => 'form-control']) !!}
</div>

<!--- Omu Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Omu_Id', 'Omu Id:') !!}
    {!! Form::text('Omu_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Inf Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Inf_Id', 'Inf Id:') !!}
    {!! Form::text('Inf_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Mul Fecha Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Mul_Fecha', 'Mul Fecha:') !!}
    {!! Form::text('Mul_Fecha', null, ['class' => 'form-control']) !!}
</div>

<!--- Mul Hora Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Mul_Hora', 'Mul Hora:') !!}
    {!! Form::text('Mul_Hora', null, ['class' => 'form-control']) !!}
</div>

<!--- Tin Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Tin_Id', 'Tin Id:') !!}
    {!! Form::text('Tin_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Per Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Per_Id', 'Per Id:') !!}
    {!! Form::text('Per_Id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
