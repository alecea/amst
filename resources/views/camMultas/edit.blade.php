@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camMulta, ['route' => ['camMultas.update', $camMulta->Mul_Id], 'method' => 'patch']) !!}

        @include('camMultas.fields')

    {!! Form::close() !!}
</div>
@endsection
