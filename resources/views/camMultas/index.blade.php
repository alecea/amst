@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_Multas</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camMultas.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camMultas->isEmpty())
                <div class="well text-center">No Cam_Multas found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Mul Estado</th>
			<th>Omu Id</th>
			<th>Inf Id</th>
			<th>Mul Fecha</th>
			<th>Mul Hora</th>
			<th>Tin Id</th>
			<th>Per Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camMultas as $camMulta)
                        <tr>
                            <td>{!! $camMulta->Mul_Estado !!}</td>
					<td>{!! $camMulta->Omu_Id !!}</td>
					<td>{!! $camMulta->Inf_Id !!}</td>
					<td>{!! $camMulta->Mul_Fecha !!}</td>
					<td>{!! $camMulta->Mul_Hora !!}</td>
					<td>{!! $camMulta->Tin_Id !!}</td>
					<td>{!! $camMulta->Per_Id !!}</td>
                            <td>
                                <a href="{!! route('camMultas.edit', [$camMulta->Mul_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camMultas.delete', [$camMulta->Mul_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_Multa?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
