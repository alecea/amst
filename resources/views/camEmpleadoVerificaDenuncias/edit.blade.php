@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camEmpleadoVerificaDenuncia, ['route' => ['camEmpleadoVerificaDenuncias.update', $camEmpleadoVerificaDenuncia->Evd_Id], 'method' => 'patch']) !!}

        @include('camEmpleadoVerificaDenuncias.fields')

    {!! Form::close() !!}
</div>
@endsection
