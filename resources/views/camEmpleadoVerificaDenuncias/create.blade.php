@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camEmpleadoVerificaDenuncias.store']) !!}

        @include('camEmpleadoVerificaDenuncias.fields')

    {!! Form::close() !!}
</div>
@endsection
