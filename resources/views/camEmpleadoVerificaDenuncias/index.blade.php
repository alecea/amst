@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_EmpleadoVerificaDenuncias</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camEmpleadoVerificaDenuncias.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camEmpleadoVerificaDenuncias->isEmpty())
                <div class="well text-center">No Cam_EmpleadoVerificaDenuncias found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Den Id</th>
			<th>Emp Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camEmpleadoVerificaDenuncias as $camEmpleadoVerificaDenuncia)
                        <tr>
                            <td>{!! $camEmpleadoVerificaDenuncia->Den_Id !!}</td>
					<td>{!! $camEmpleadoVerificaDenuncia->Emp_Id !!}</td>
                            <td>
                                <a href="{!! route('camEmpleadoVerificaDenuncias.edit', [$camEmpleadoVerificaDenuncia->Evd_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camEmpleadoVerificaDenuncias.delete', [$camEmpleadoVerificaDenuncia->Evd_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_EmpleadoVerificaDenuncia?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
