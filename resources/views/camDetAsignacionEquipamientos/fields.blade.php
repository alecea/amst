<!--- Cantidad Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Cantidad', 'Cantidad:') !!}
    {!! Form::text('Cantidad', null, ['class' => 'form-control']) !!}
</div>

<!--- Aeq Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Aeq_Id', 'Aeq Id:') !!}
    {!! Form::text('Aeq_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Teq Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Teq_Id', 'Teq Id:') !!}
    {!! Form::text('Teq_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Equ Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Equ_Id', 'Equ Id:') !!}
    {!! Form::text('Equ_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Arm Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Arm_Id', 'Arm Id:') !!}
    {!! Form::text('Arm_Id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
