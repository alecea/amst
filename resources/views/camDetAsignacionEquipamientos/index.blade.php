@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_DetAsignacionEquipamientos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camDetAsignacionEquipamientos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camDetAsignacionEquipamientos->isEmpty())
                <div class="well text-center">No Cam_DetAsignacionEquipamientos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Cantidad</th>
			<th>Aeq Id</th>
			<th>Teq Id</th>
			<th>Equ Id</th>
			<th>Arm Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camDetAsignacionEquipamientos as $camDetAsignacionEquipamiento)
                        <tr>
                            <td>{!! $camDetAsignacionEquipamiento->Cantidad !!}</td>
					<td>{!! $camDetAsignacionEquipamiento->Aeq_Id !!}</td>
					<td>{!! $camDetAsignacionEquipamiento->Teq_Id !!}</td>
					<td>{!! $camDetAsignacionEquipamiento->Equ_Id !!}</td>
					<td>{!! $camDetAsignacionEquipamiento->Arm_Id !!}</td>
                            <td>
                                <a href="{!! route('camDetAsignacionEquipamientos.edit', [$camDetAsignacionEquipamiento->Das_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camDetAsignacionEquipamientos.delete', [$camDetAsignacionEquipamiento->Das_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_DetAsignacionEquipamiento?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
