@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camDetAsignacionEquipamientos.store']) !!}

        @include('camDetAsignacionEquipamientos.fields')

    {!! Form::close() !!}
</div>
@endsection
