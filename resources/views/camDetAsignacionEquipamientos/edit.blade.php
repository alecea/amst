@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camDetAsignacionEquipamiento, ['route' => ['camDetAsignacionEquipamientos.update', $camDetAsignacionEquipamiento->Das_Id], 'method' => 'patch']) !!}

        @include('camDetAsignacionEquipamientos.fields')

    {!! Form::close() !!}
</div>
@endsection
