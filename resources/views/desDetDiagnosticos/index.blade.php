@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Des_DetDiagnosticos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('desDetDiagnosticos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($desDetDiagnosticos->isEmpty())
                <div class="well text-center">No Des_DetDiagnosticos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Dta Id</th>
			<th>Pve Id</th>
			<th>Ded Observacion</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($desDetDiagnosticos as $desDetDiagnostico)
                        <tr>
                            <td>{!! $desDetDiagnostico->Dta_Id !!}</td>
					<td>{!! $desDetDiagnostico->Pve_Id !!}</td>
					<td>{!! $desDetDiagnostico->Ded_Observacion !!}</td>
                            <td>
                                <a href="{!! route('desDetDiagnosticos.edit', [$desDetDiagnostico->Ded_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('desDetDiagnosticos.delete', [$desDetDiagnostico->Ded_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Des_DetDiagnostico?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
