@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($desDetDiagnostico, ['route' => ['desDetDiagnosticos.update', $desDetDiagnostico->Ded_Id], 'method' => 'patch']) !!}

        @include('desDetDiagnosticos.fields')

    {!! Form::close() !!}
</div>
@endsection
