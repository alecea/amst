<!--- Dta Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Dta_Id', 'Dta Id:') !!}
    {!! Form::text('Dta_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Pve Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Pve_Id', 'Pve Id:') !!}
    {!! Form::text('Pve_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Ded Observacion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Ded_Observacion', 'Ded Observacion:') !!}
    {!! Form::text('Ded_Observacion', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
