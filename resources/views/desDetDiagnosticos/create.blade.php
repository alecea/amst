@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'desDetDiagnosticos.store']) !!}

        @include('desDetDiagnosticos.fields')

    {!! Form::close() !!}
</div>
@endsection
