@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camInventario, ['route' => ['camInventarios.update', $camInventario->Inv_Id], 'method' => 'patch']) !!}

        @include('camInventarios.fields')

    {!! Form::close() !!}
</div>
@endsection
