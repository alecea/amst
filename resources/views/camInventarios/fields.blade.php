<!--- Teq Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Teq_Id', 'Teq Id:') !!}
    {!! Form::text('Teq_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Inv Cantidad Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Inv_Cantidad', 'Inv Cantidad:') !!}
    {!! Form::text('Inv_Cantidad', null, ['class' => 'form-control']) !!}
</div>

<!--- Com Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Com_Id', 'Com Id:') !!}
    {!! Form::text('Com_Id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
