@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_Inventarios</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camInventarios.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camInventarios->isEmpty())
                <div class="well text-center">No Cam_Inventarios found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Teq Id</th>
			<th>Inv Cantidad</th>
			<th>Com Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camInventarios as $camInventario)
                        <tr>
                            <td>{!! $camInventario->Teq_Id !!}</td>
					<td>{!! $camInventario->Inv_Cantidad !!}</td>
					<td>{!! $camInventario->Com_Id !!}</td>
                            <td>
                                <a href="{!! route('camInventarios.edit', [$camInventario->Inv_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camInventarios.delete', [$camInventario->Inv_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_Inventario?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
