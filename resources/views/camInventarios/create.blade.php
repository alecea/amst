@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camInventarios.store']) !!}

        @include('camInventarios.fields')

    {!! Form::close() !!}
</div>
@endsection
