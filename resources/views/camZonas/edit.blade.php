@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camZona, ['route' => ['camZonas.update', $camZona->Zon_Id], 'method' => 'patch']) !!}

        @include('camZonas.fields')

    {!! Form::close() !!}
</div>
@endsection
