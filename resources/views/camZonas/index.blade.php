@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_Zonas</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camZonas.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camZonas->isEmpty())
                <div class="well text-center">No Cam_Zonas found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Zon Nombre</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camZonas as $camZona)
                        <tr>
                            <td>{!! $camZona->Zon_Nombre !!}</td>
                            <td>
                                <a href="{!! route('camZonas.edit', [$camZona->Zon_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camZonas.delete', [$camZona->Zon_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_Zona?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
