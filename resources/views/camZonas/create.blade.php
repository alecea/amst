@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camZonas.store']) !!}

        @include('camZonas.fields')

    {!! Form::close() !!}
</div>
@endsection
