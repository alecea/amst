@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camOrigenMultas.store']) !!}

        @include('camOrigenMultas.fields')

    {!! Form::close() !!}
</div>
@endsection
