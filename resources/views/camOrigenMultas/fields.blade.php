<!--- Omu Tipo Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Omu_Tipo', 'Omu Tipo:') !!}
    {!! Form::text('Omu_Tipo', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
