@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_OrigenMultas</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camOrigenMultas.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camOrigenMultas->isEmpty())
                <div class="well text-center">No Cam_OrigenMultas found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Omu Tipo</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camOrigenMultas as $camOrigenMulta)
                        <tr>
                            <td>{!! $camOrigenMulta->Omu_Tipo !!}</td>
                            <td>
                                <a href="{!! route('camOrigenMultas.edit', [$camOrigenMulta->Omu_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camOrigenMultas.delete', [$camOrigenMulta->Omu_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_OrigenMulta?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
