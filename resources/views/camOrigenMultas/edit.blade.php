@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camOrigenMulta, ['route' => ['camOrigenMultas.update', $camOrigenMulta->Omu_Id], 'method' => 'patch']) !!}

        @include('camOrigenMultas.fields')

    {!! Form::close() !!}
</div>
@endsection
