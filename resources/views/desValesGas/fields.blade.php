<!--- Emp Idsolicita Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_IdSolicita', 'Emp Idsolicita:') !!}
    {!! Form::text('Emp_IdSolicita', null, ['class' => 'form-control']) !!}
</div>

<!--- Via Observacion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Via_Observacion', 'Via Observacion:') !!}
    {!! Form::text('Via_Observacion', null, ['class' => 'form-control']) !!}
</div>

<!--- Via Cantidadsolicitada Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Via_CantidadSolicitada', 'Via Cantidadsolicitada:') !!}
    {!! Form::text('Via_CantidadSolicitada', null, ['class' => 'form-control']) !!}
</div>

<!--- Via Autorizado Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Via_Autorizado', 'Via Autorizado:') !!}
    {!! Form::text('Via_Autorizado', null, ['class' => 'form-control']) !!}
</div>

<!--- Via Fechaentrega Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Via_FechaEntrega', 'Via Fechaentrega:') !!}
    {!! Form::text('Via_FechaEntrega', null, ['class' => 'form-control']) !!}
</div>

<!--- Via Cantidadentregada Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Via_CantidadEntregada', 'Via Cantidadentregada:') !!}
    {!! Form::text('Via_CantidadEntregada', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Idautoriza Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_IdAutoriza', 'Emp Idautoriza:') !!}
    {!! Form::text('Emp_IdAutoriza', null, ['class' => 'form-control']) !!}
</div>

<!--- Veh Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Veh_Id', 'Veh Id:') !!}
    {!! Form::text('Veh_Id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
