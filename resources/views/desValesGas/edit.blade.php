@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($desValesGas, ['route' => ['desValesGas.update', $desValesGas->id], 'method' => 'patch']) !!}

        @include('desValesGas.fields')

    {!! Form::close() !!}
</div>
@endsection
