@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'desValesGas.store']) !!}

        @include('desValesGas.fields')

    {!! Form::close() !!}
</div>
@endsection
