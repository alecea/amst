@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Des_ValesGas</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('desValesGas.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($desValesGas->isEmpty())
                <div class="well text-center">No Des_ValesGas found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Emp Idsolicita</th>
			<th>Via Observacion</th>
			<th>Via Cantidadsolicitada</th>
			<th>Via Autorizado</th>
			<th>Via Fechaentrega</th>
			<th>Via Cantidadentregada</th>
			<th>Emp Idautoriza</th>
			<th>Veh Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($desValesGas as $desValesGas)
                        <tr>
                            <td>{!! $desValesGas->Emp_IdSolicita !!}</td>
					<td>{!! $desValesGas->Via_Observacion !!}</td>
					<td>{!! $desValesGas->Via_CantidadSolicitada !!}</td>
					<td>{!! $desValesGas->Via_Autorizado !!}</td>
					<td>{!! $desValesGas->Via_FechaEntrega !!}</td>
					<td>{!! $desValesGas->Via_CantidadEntregada !!}</td>
					<td>{!! $desValesGas->Emp_IdAutoriza !!}</td>
					<td>{!! $desValesGas->Veh_Id !!}</td>
                            <td>
                                <a href="{!! route('desValesGas.edit', [$desValesGas->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('desValesGas.delete', [$desValesGas->id]) !!}" onclick="return confirm('Are you sure wants to delete this Des_ValesGas?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection