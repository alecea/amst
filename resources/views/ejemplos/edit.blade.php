@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($ejemplo, ['route' => ['ejemplos.update', $ejemplo->id], 'method' => 'patch']) !!}

        @include('ejemplos.fields')

    {!! Form::close() !!}
</div>
@endsection
