@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'ejemplos.store']) !!}

        @include('ejemplos.fields')

    {!! Form::close() !!}
</div>
@endsection
