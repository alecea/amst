@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">ejemplos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('ejemplos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($ejemplos->isEmpty())
                <div class="well text-center">No ejemplos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Ruta</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>
                     
                    @foreach($ejemplos as $ejemplo)
                        <tr>
                            <td>{!! $ejemplo->ruta !!}</td>
                            <td>
                                <a href="{!! route('ejemplos.edit', [$ejemplo->id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('ejemplos.delete', [$ejemplo->id]) !!}" onclick="return confirm('Are you sure wants to delete this ejemplo?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection