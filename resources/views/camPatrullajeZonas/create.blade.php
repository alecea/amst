@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camPatrullajeZonas.store']) !!}

        @include('camPatrullajeZonas.fields')

    {!! Form::close() !!}
</div>
@endsection
