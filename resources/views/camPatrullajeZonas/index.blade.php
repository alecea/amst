@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_PatrullajeZonas</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camPatrullajeZonas.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camPatrullajeZonas->isEmpty())
                <div class="well text-center">No Cam_PatrullajeZonas found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Zon Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camPatrullajeZonas as $camPatrullajeZona)
                        <tr>
                            <td>{!! $camPatrullajeZona->Zon_Id !!}</td>
                            <td>
                                <a href="{!! route('camPatrullajeZonas.edit', [$camPatrullajeZona->Pat_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camPatrullajeZonas.delete', [$camPatrullajeZona->Pat_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_PatrullajeZona?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
