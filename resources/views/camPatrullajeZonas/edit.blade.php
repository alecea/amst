@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camPatrullajeZona, ['route' => ['camPatrullajeZonas.update', $camPatrullajeZona->Pat_Id], 'method' => 'patch']) !!}

        @include('camPatrullajeZonas.fields')

    {!! Form::close() !!}
</div>
@endsection
