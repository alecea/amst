@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camTipoEquipamientos.store']) !!}

        @include('camTipoEquipamientos.fields')

    {!! Form::close() !!}
</div>
@endsection
