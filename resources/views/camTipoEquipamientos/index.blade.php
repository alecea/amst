@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_TipoEquipamientos</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camTipoEquipamientos.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camTipoEquipamientos->isEmpty())
                <div class="well text-center">No Cam_TipoEquipamientos found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Teq Descripcion</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camTipoEquipamientos as $camTipoEquipamiento)
                        <tr>
                            <td>{!! $camTipoEquipamiento->Teq_Descripcion !!}</td>
                            <td>
                                <a href="{!! route('camTipoEquipamientos.edit', [$camTipoEquipamiento->Teq_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camTipoEquipamientos.delete', [$camTipoEquipamiento->Teq_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_TipoEquipamiento?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
