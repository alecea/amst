@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camTipoEquipamiento, ['route' => ['camTipoEquipamientos.update', $camTipoEquipamiento->Teq_Id], 'method' => 'patch']) !!}

        @include('camTipoEquipamientos.fields')

    {!! Form::close() !!}
</div>
@endsection
