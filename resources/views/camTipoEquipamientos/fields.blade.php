<!--- Teq Descripcion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Teq_Descripcion', 'Teq Descripcion:') !!}
    {!! Form::text('Teq_Descripcion', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
