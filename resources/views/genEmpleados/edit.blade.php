@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($genEmpleado, ['route' => ['genEmpleados.update', $genEmpleado->Emp_Id], 'method' => 'patch']) !!}

        @include('genEmpleados.fields')

    {!! Form::close() !!}
</div>
@endsection
