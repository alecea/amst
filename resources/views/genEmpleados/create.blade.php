@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'genEmpleados.store']) !!}

        @include('genEmpleados.fields')

    {!! Form::close() !!}
</div>
@endsection
