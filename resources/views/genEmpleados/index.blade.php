@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Gen_Empleados</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('genEmpleados.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($genEmpleados->isEmpty())
                <div class="well text-center">No Gen_Empleados found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Emp Primernombre</th>
			<th>Emp Segundonombre</th>
			<th>Emp Primerapellido</th>
			<th>Emp Segundoapellido</th>
			<th>Emp Telefono</th>
			<th>Emp Celular</th>
			<th>Emp Dui</th>
			<th>Emp Direccion</th>
			<th>Emp Cargo</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($genEmpleados as $genEmpleado)
                        <tr>
                            <td>{!! $genEmpleado->Emp_PrimerNombre !!}</td>
					<td>{!! $genEmpleado->Emp_SegundoNombre !!}</td>
					<td>{!! $genEmpleado->Emp_PrimerApellido !!}</td>
					<td>{!! $genEmpleado->Emp_SegundoApellido !!}</td>
					<td>{!! $genEmpleado->Emp_Telefono !!}</td>
					<td>{!! $genEmpleado->Emp_Celular !!}</td>
					<td>{!! $genEmpleado->Emp_Dui !!}</td>
					<td>{!! $genEmpleado->Emp_Direccion !!}</td>
					<td>{!! $genEmpleado->Emp_Cargo !!}</td>
                            <td>
                                <a href="{!! route('genEmpleados.edit', [$genEmpleado->Emp_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('genEmpleados.delete', [$genEmpleado->Emp_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Gen_Empleado?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
