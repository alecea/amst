<!--- Emp Primernombre Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_PrimerNombre', 'Emp Primernombre:') !!}
    {!! Form::text('Emp_PrimerNombre', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Segundonombre Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_SegundoNombre', 'Emp Segundonombre:') !!}
    {!! Form::text('Emp_SegundoNombre', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Primerapellido Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_PrimerApellido', 'Emp Primerapellido:') !!}
    {!! Form::text('Emp_PrimerApellido', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Segundoapellido Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_SegundoApellido', 'Emp Segundoapellido:') !!}
    {!! Form::text('Emp_SegundoApellido', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Telefono Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Telefono', 'Emp Telefono:') !!}
    {!! Form::text('Emp_Telefono', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Celular Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Celular', 'Emp Celular:') !!}
    {!! Form::text('Emp_Celular', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Dui Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Dui', 'Emp Dui:') !!}
    {!! Form::text('Emp_Dui', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Direccion Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Direccion', 'Emp Direccion:') !!}
    {!! Form::text('Emp_Direccion', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Cargo Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Cargo', 'Emp Cargo:') !!}
    {!! Form::text('Emp_Cargo', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
