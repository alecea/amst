<!--- Pat Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Pat_Id', 'Pat Id:') !!}
    {!! Form::text('Pat_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Id', 'Emp Id:') !!}
    {!! Form::text('Emp_Id', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
