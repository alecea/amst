@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_EmpleadoPatrullajes</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camEmpleadoPatrullajes.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camEmpleadoPatrullajes->isEmpty())
                <div class="well text-center">No Cam_EmpleadoPatrullajes found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Pat Id</th>
			<th>Emp Id</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camEmpleadoPatrullajes as $camEmpleadoPatrullaje)
                        <tr>
                            <td>{!! $camEmpleadoPatrullaje->Pat_Id !!}</td>
					<td>{!! $camEmpleadoPatrullaje->Emp_Id !!}</td>
                            <td>
                                <a href="{!! route('camEmpleadoPatrullajes.edit', [$camEmpleadoPatrullaje->Epa_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camEmpleadoPatrullajes.delete', [$camEmpleadoPatrullaje->Epa_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_EmpleadoPatrullaje?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
