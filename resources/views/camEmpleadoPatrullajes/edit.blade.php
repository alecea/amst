@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camEmpleadoPatrullaje, ['route' => ['camEmpleadoPatrullajes.update', $camEmpleadoPatrullaje->Epa_Id], 'method' => 'patch']) !!}

        @include('camEmpleadoPatrullajes.fields')

    {!! Form::close() !!}
</div>
@endsection
