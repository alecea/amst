@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camEmpleadoPatrullajes.store']) !!}

        @include('camEmpleadoPatrullajes.fields')

    {!! Form::close() !!}
</div>
@endsection
