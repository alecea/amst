<!--- Tur Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Tur_Id', 'Tur Id:') !!}
    {!! Form::text('Tur_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Emp Id Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Emp_Id', 'Emp Id:') !!}
    {!! Form::text('Emp_Id', null, ['class' => 'form-control']) !!}
</div>

<!--- Pat Observaciones Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Pat_Observaciones', 'Pat Observaciones:') !!}
    {!! Form::text('Pat_Observaciones', null, ['class' => 'form-control']) !!}
</div>

<!--- Pat Fecha Field --->
<div class="form-group col-sm-6 col-lg-4">
    {!! Form::label('Pat_Fecha', 'Pat Fecha:') !!}
    {!! Form::text('Pat_Fecha', null, ['class' => 'form-control']) !!}
</div>


<!--- Submit Field --->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
</div>
