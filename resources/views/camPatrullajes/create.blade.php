@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::open(['route' => 'camPatrullajes.store']) !!}

        @include('camPatrullajes.fields')

    {!! Form::close() !!}
</div>
@endsection
