@extends('app')

@section('content')
<div class="container">

    @include('common.errors')

    {!! Form::model($camPatrullaje, ['route' => ['camPatrullajes.update', $camPatrullaje->Pat_Id], 'method' => 'patch']) !!}

        @include('camPatrullajes.fields')

    {!! Form::close() !!}
</div>
@endsection
