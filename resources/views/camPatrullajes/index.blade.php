@extends('app')

@section('content')

    <div class="container">

        @include('flash::message')

        <div class="row">
            <h1 class="pull-left">Cam_Patrullajes</h1>
            <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('camPatrullajes.create') !!}">Add New</a>
        </div>

        <div class="row">
            @if($camPatrullajes->isEmpty())
                <div class="well text-center">No Cam_Patrullajes found.</div>
            @else
                <table class="table">
                    <thead>
                    <th>Tur Id</th>
			<th>Emp Id</th>
			<th>Pat Observaciones</th>
			<th>Pat Fecha</th>
                    <th width="50px">Action</th>
                    </thead>
                    <tbody>

                    @foreach($camPatrullajes as $camPatrullaje)
                        <tr>
                            <td>{!! $camPatrullaje->Tur_Id !!}</td>
					<td>{!! $camPatrullaje->Emp_Id !!}</td>
					<td>{!! $camPatrullaje->Pat_Observaciones !!}</td>
					<td>{!! $camPatrullaje->Pat_Fecha !!}</td>
                            <td>
                                <a href="{!! route('camPatrullajes.edit', [$camPatrullaje->Pat_Id]) !!}"><i class="glyphicon glyphicon-edit"></i></a>
                                <a href="{!! route('camPatrullajes.delete', [$camPatrullaje->Pat_Id]) !!}" onclick="return confirm('Are you sure wants to delete this Cam_Patrullaje?')"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            @endif
        </div>
    </div>
@endsection
