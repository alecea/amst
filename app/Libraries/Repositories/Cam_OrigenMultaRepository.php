<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_OrigenMulta;
use Illuminate\Support\Facades\Schema;

class Cam_OrigenMultaRepository
{

	/**
	 * Returns all Cam_OrigenMultas
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_OrigenMulta::all();
	}

	public function search($input)
    {
        $query = Cam_OrigenMulta::query();

        $columns = Schema::getColumnListing('cam__origen_multas');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_OrigenMulta into database
	 *
	 * @param array $input
	 *
	 * @return Cam_OrigenMulta
	 */
	public function store($input)
	{
		return Cam_OrigenMulta::create($input);
	}

	/**
	 * Find Cam_OrigenMulta by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_OrigenMulta
	 */
	public function findCam_OrigenMultaById($id)
	{
		return Cam_OrigenMulta::find($id);
	}

	/**
	 * Updates Cam_OrigenMulta into database
	 *
	 * @param Cam_OrigenMulta $camOrigenMulta
	 * @param array $input
	 *
	 * @return Cam_OrigenMulta
	 */
	public function update($camOrigenMulta, $input)
	{
		$camOrigenMulta->fill($input);
		$camOrigenMulta->save();

		return $camOrigenMulta;
	}
}