<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_PatrullajeZona;
use Illuminate\Support\Facades\Schema;

class Cam_PatrullajeZonaRepository
{

	/**
	 * Returns all Cam_PatrullajeZonas
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_PatrullajeZona::all();
	}

	public function search($input)
    {
        $query = Cam_PatrullajeZona::query();

        $columns = Schema::getColumnListing('cam__patrullaje_zonas');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_PatrullajeZona into database
	 *
	 * @param array $input
	 *
	 * @return Cam_PatrullajeZona
	 */
	public function store($input)
	{
		return Cam_PatrullajeZona::create($input);
	}

	/**
	 * Find Cam_PatrullajeZona by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_PatrullajeZona
	 */
	public function findCam_PatrullajeZonaById($id)
	{
		return Cam_PatrullajeZona::find($id);
	}

	/**
	 * Updates Cam_PatrullajeZona into database
	 *
	 * @param Cam_PatrullajeZona $camPatrullajeZona
	 * @param array $input
	 *
	 * @return Cam_PatrullajeZona
	 */
	public function update($camPatrullajeZona, $input)
	{
		$camPatrullajeZona->fill($input);
		$camPatrullajeZona->save();

		return $camPatrullajeZona;
	}
}