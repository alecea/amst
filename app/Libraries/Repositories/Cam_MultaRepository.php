<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Multa;
use Illuminate\Support\Facades\Schema;

class Cam_MultaRepository
{

	/**
	 * Returns all Cam_Multas
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Multa::all();
	}

	public function search($input)
    {
        $query = Cam_Multa::query();

        $columns = Schema::getColumnListing('cam__multas');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Multa into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Multa
	 */
	public function store($input)
	{
		return Cam_Multa::create($input);
	}

	/**
	 * Find Cam_Multa by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Multa
	 */
	public function findCam_MultaById($id)
	{
		return Cam_Multa::find($id);
	}

	/**
	 * Updates Cam_Multa into database
	 *
	 * @param Cam_Multa $camMulta
	 * @param array $input
	 *
	 * @return Cam_Multa
	 */
	public function update($camMulta, $input)
	{
		$camMulta->fill($input);
		$camMulta->save();

		return $camMulta;
	}
}