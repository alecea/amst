<?php

namespace App\Libraries\Repositories;


use App\Models\Des_ValesGas;
use Illuminate\Support\Facades\Schema;

class Des_ValesGasRepository
{

	/**
	 * Returns all Des_ValesGas
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Des_ValesGas::all();
	}

	public function search($input)
    {
        $query = Des_ValesGas::query();

        $columns = Schema::getColumnListing('des__vales_gas');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Des_ValesGas into database
	 *
	 * @param array $input
	 *
	 * @return Des_ValesGas
	 */
	public function store($input)
	{
		return Des_ValesGas::create($input);
	}

	/**
	 * Find Des_ValesGas by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Des_ValesGas
	 */
	public function findDes_ValesGasById($id)
	{
		return Des_ValesGas::find($id);
	}

	/**
	 * Updates Des_ValesGas into database
	 *
	 * @param Des_ValesGas $desValesGas
	 * @param array $input
	 *
	 * @return Des_ValesGas
	 */
	public function update($desValesGas, $input)
	{
		$desValesGas->fill($input);
		$desValesGas->save();

		return $desValesGas;
	}
}