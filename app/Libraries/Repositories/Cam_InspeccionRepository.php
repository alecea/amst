<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Inspeccion;
use Illuminate\Support\Facades\Schema;

class Cam_InspeccionRepository
{

	/**
	 * Returns all Cam_Inspeccions
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Inspeccion::all();
	}

	public function search($input)
    {
        $query = Cam_Inspeccion::query();

        $columns = Schema::getColumnListing('cam__inspeccions');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Inspeccion into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Inspeccion
	 */
	public function store($input)
	{
		return Cam_Inspeccion::create($input);
	}

	/**
	 * Find Cam_Inspeccion by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Inspeccion
	 */
	public function findCam_InspeccionById($id)
	{
		return Cam_Inspeccion::find($id);
	}

	/**
	 * Updates Cam_Inspeccion into database
	 *
	 * @param Cam_Inspeccion $camInspeccion
	 * @param array $input
	 *
	 * @return Cam_Inspeccion
	 */
	public function update($camInspeccion, $input)
	{
		$camInspeccion->fill($input);
		$camInspeccion->save();

		return $camInspeccion;
	}
}