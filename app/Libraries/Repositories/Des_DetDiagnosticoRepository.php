<?php

namespace App\Libraries\Repositories;


use App\Models\Des_DetDiagnostico;
use Illuminate\Support\Facades\Schema;

class Des_DetDiagnosticoRepository
{

	/**
	 * Returns all Des_DetDiagnosticos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Des_DetDiagnostico::all();
	}

	public function search($input)
    {
        $query = Des_DetDiagnostico::query();

        $columns = Schema::getColumnListing('des__det_diagnosticos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Des_DetDiagnostico into database
	 *
	 * @param array $input
	 *
	 * @return Des_DetDiagnostico
	 */
	public function store($input)
	{
		return Des_DetDiagnostico::create($input);
	}

	/**
	 * Find Des_DetDiagnostico by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Des_DetDiagnostico
	 */
	public function findDes_DetDiagnosticoById($id)
	{
		return Des_DetDiagnostico::find($id);
	}

	/**
	 * Updates Des_DetDiagnostico into database
	 *
	 * @param Des_DetDiagnostico $desDetDiagnostico
	 * @param array $input
	 *
	 * @return Des_DetDiagnostico
	 */
	public function update($desDetDiagnostico, $input)
	{
		$desDetDiagnostico->fill($input);
		$desDetDiagnostico->save();

		return $desDetDiagnostico;
	}
}