<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_TipoInfraccion;
use Illuminate\Support\Facades\Schema;

class Cam_TipoInfraccionRepository
{

	/**
	 * Returns all Cam_TipoInfraccions
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_TipoInfraccion::all();
	}

	public function search($input)
    {
        $query = Cam_TipoInfraccion::query();

        $columns = Schema::getColumnListing('cam__tipo_infraccions');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_TipoInfraccion into database
	 *
	 * @param array $input
	 *
	 * @return Cam_TipoInfraccion
	 */
	public function store($input)
	{
		return Cam_TipoInfraccion::create($input);
	}

	/**
	 * Find Cam_TipoInfraccion by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_TipoInfraccion
	 */
	public function findCam_TipoInfraccionById($id)
	{
		return Cam_TipoInfraccion::find($id);
	}

	/**
	 * Updates Cam_TipoInfraccion into database
	 *
	 * @param Cam_TipoInfraccion $camTipoInfraccion
	 * @param array $input
	 *
	 * @return Cam_TipoInfraccion
	 */
	public function update($camTipoInfraccion, $input)
	{
		$camTipoInfraccion->fill($input);
		$camTipoInfraccion->save();

		return $camTipoInfraccion;
	}
}