<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_TipoEquipamiento;
use Illuminate\Support\Facades\Schema;

class Cam_TipoEquipamientoRepository
{

	/**
	 * Returns all Cam_TipoEquipamientos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_TipoEquipamiento::all();
	}

	public function search($input)
    {
        $query = Cam_TipoEquipamiento::query();

        $columns = Schema::getColumnListing('cam__tipo_equipamientos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_TipoEquipamiento into database
	 *
	 * @param array $input
	 *
	 * @return Cam_TipoEquipamiento
	 */
	public function store($input)
	{
		return Cam_TipoEquipamiento::create($input);
	}

	/**
	 * Find Cam_TipoEquipamiento by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_TipoEquipamiento
	 */
	public function findCam_TipoEquipamientoById($id)
	{
		return Cam_TipoEquipamiento::find($id);
	}

	/**
	 * Updates Cam_TipoEquipamiento into database
	 *
	 * @param Cam_TipoEquipamiento $camTipoEquipamiento
	 * @param array $input
	 *
	 * @return Cam_TipoEquipamiento
	 */
	public function update($camTipoEquipamiento, $input)
	{
		$camTipoEquipamiento->fill($input);
		$camTipoEquipamiento->save();

		return $camTipoEquipamiento;
	}
}