<?php

namespace App\Libraries\Repositories;


use App\Models\Des_Vehiculo;
use Illuminate\Support\Facades\Schema;

class Des_VehiculoRepository
{

	/**
	 * Returns all Des_Vehiculos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Des_Vehiculo::all();
	}

	public function search($input)
    {
        $query = Des_Vehiculo::query();

        $columns = Schema::getColumnListing('des__vehiculos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Des_Vehiculo into database
	 *
	 * @param array $input
	 *
	 * @return Des_Vehiculo
	 */
	public function store($input)
	{
		return Des_Vehiculo::create($input);
	}

	/**
	 * Find Des_Vehiculo by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Des_Vehiculo
	 */
	public function findDes_VehiculoById($id)
	{
		return Des_Vehiculo::find($id);
	}

	/**
	 * Updates Des_Vehiculo into database
	 *
	 * @param Des_Vehiculo $desVehiculo
	 * @param array $input
	 *
	 * @return Des_Vehiculo
	 */
	public function update($desVehiculo, $input)
	{
		$desVehiculo->fill($input);
		$desVehiculo->save();

		return $desVehiculo;
	}
}