<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_MovInventario;
use Illuminate\Support\Facades\Schema;

class Cam_MovInventarioRepository
{

	/**
	 * Returns all Cam_MovInventarios
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_MovInventario::all();
	}

	public function search($input)
    {
        $query = Cam_MovInventario::query();

        $columns = Schema::getColumnListing('cam__mov_inventarios');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_MovInventario into database
	 *
	 * @param array $input
	 *
	 * @return Cam_MovInventario
	 */
	public function store($input)
	{
		return Cam_MovInventario::create($input);
	}

	/**
	 * Find Cam_MovInventario by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_MovInventario
	 */
	public function findCam_MovInventarioById($id)
	{
		return Cam_MovInventario::find($id);
	}

	/**
	 * Updates Cam_MovInventario into database
	 *
	 * @param Cam_MovInventario $camMovInventario
	 * @param array $input
	 *
	 * @return Cam_MovInventario
	 */
	public function update($camMovInventario, $input)
	{
		$camMovInventario->fill($input);
		$camMovInventario->save();

		return $camMovInventario;
	}
}