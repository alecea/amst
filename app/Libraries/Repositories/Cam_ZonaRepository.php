<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Zona;
use Illuminate\Support\Facades\Schema;

class Cam_ZonaRepository
{

	/**
	 * Returns all Cam_Zonas
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Zona::all();
	}

	public function search($input)
    {
        $query = Cam_Zona::query();

        $columns = Schema::getColumnListing('cam__zonas');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Zona into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Zona
	 */
	public function store($input)
	{
		return Cam_Zona::create($input);
	}

	/**
	 * Find Cam_Zona by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Zona
	 */
	public function findCam_ZonaById($id)
	{
		return Cam_Zona::find($id);
	}

	/**
	 * Updates Cam_Zona into database
	 *
	 * @param Cam_Zona $camZona
	 * @param array $input
	 *
	 * @return Cam_Zona
	 */
	public function update($camZona, $input)
	{
		$camZona->fill($input);
		$camZona->save();

		return $camZona;
	}
}