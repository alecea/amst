<?php

namespace App\Libraries\Repositories;


use App\Models\ejemplo;
use Illuminate\Support\Facades\Schema;

class ejemploRepository
{

	/**
	 * Returns all ejemplos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return ejemplo::all();
	}

	public function search($input)
    {
        $query = ejemplo::query();

        $columns = Schema::getColumnListing('ejemplos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores ejemplo into database
	 *
	 * @param array $input
	 *
	 * @return ejemplo
	 */
	public function store($input)
	{
		return ejemplo::create($input);
	}

	/**
	 * Find ejemplo by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|ejemplo
	 */
	public function findejemploById($id)
	{
		return ejemplo::find($id);
	}

	/**
	 * Updates ejemplo into database
	 *
	 * @param ejemplo $ejemplo
	 * @param array $input
	 *
	 * @return ejemplo
	 */
	public function update($ejemplo, $input)
	{
		$ejemplo->fill($input);
		$ejemplo->save();

		return $ejemplo;
	}
}