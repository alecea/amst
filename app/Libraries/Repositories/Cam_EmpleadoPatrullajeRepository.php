<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_EmpleadoPatrullaje;
use Illuminate\Support\Facades\Schema;

class Cam_EmpleadoPatrullajeRepository
{

	/**
	 * Returns all Cam_EmpleadoPatrullajes
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_EmpleadoPatrullaje::all();
	}

	public function search($input)
    {
        $query = Cam_EmpleadoPatrullaje::query();

        $columns = Schema::getColumnListing('cam__empleado_patrullajes');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_EmpleadoPatrullaje into database
	 *
	 * @param array $input
	 *
	 * @return Cam_EmpleadoPatrullaje
	 */
	public function store($input)
	{
		return Cam_EmpleadoPatrullaje::create($input);
	}

	/**
	 * Find Cam_EmpleadoPatrullaje by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_EmpleadoPatrullaje
	 */
	public function findCam_EmpleadoPatrullajeById($id)
	{
		return Cam_EmpleadoPatrullaje::find($id);
	}

	/**
	 * Updates Cam_EmpleadoPatrullaje into database
	 *
	 * @param Cam_EmpleadoPatrullaje $camEmpleadoPatrullaje
	 * @param array $input
	 *
	 * @return Cam_EmpleadoPatrullaje
	 */
	public function update($camEmpleadoPatrullaje, $input)
	{
		$camEmpleadoPatrullaje->fill($input);
		$camEmpleadoPatrullaje->save();

		return $camEmpleadoPatrullaje;
	}
}