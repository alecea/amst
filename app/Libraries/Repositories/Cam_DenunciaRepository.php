<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Denuncia;
use Illuminate\Support\Facades\Schema;

class Cam_DenunciaRepository
{

	/**
	 * Returns all Cam_Denuncias
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Denuncia::all();
	}

	public function search($input)
    {
        $query = Cam_Denuncia::query();

        $columns = Schema::getColumnListing('cam__denuncias');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Denuncia into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Denuncia
	 */
	public function store($input)
	{
		return Cam_Denuncia::create($input);
	}

	/**
	 * Find Cam_Denuncia by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Denuncia
	 */
	public function findCam_DenunciaById($id)
	{
		return Cam_Denuncia::find($id);
	}

	/**
	 * Updates Cam_Denuncia into database
	 *
	 * @param Cam_Denuncia $camDenuncia
	 * @param array $input
	 *
	 * @return Cam_Denuncia
	 */
	public function update($camDenuncia, $input)
	{
		$camDenuncia->fill($input);
		$camDenuncia->save();

		return $camDenuncia;
	}
}