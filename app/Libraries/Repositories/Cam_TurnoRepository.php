<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Turno;
use Illuminate\Support\Facades\Schema;

class Cam_TurnoRepository
{

	/**
	 * Returns all Cam_Turnos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Turno::all();
	}

	public function search($input)
    {
        $query = Cam_Turno::query();

        $columns = Schema::getColumnListing('cam__turnos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Turno into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Turno
	 */
	public function store($input)
	{
		return Cam_Turno::create($input);
	}

	/**
	 * Find Cam_Turno by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Turno
	 */
	public function findCam_TurnoById($id)
	{
		return Cam_Turno::find($id);
	}

	/**
	 * Updates Cam_Turno into database
	 *
	 * @param Cam_Turno $camTurno
	 * @param array $input
	 *
	 * @return Cam_Turno
	 */
	public function update($camTurno, $input)
	{
		$camTurno->fill($input);
		$camTurno->save();

		return $camTurno;
	}
}