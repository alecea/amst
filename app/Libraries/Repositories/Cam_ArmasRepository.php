<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Armas;
use Illuminate\Support\Facades\Schema;

class Cam_ArmasRepository
{

	/**
	 * Returns all Cam_Armas
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Armas::all();
	}

	public function search($input)
    {
        $query = Cam_Armas::query();

        $columns = Schema::getColumnListing('cam__armas');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Armas into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Armas
	 */
	public function store($input)
	{
		return Cam_Armas::create($input);
	}

	/**
	 * Find Cam_Armas by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Armas
	 */
	public function findCam_ArmasById($id)
	{
		return Cam_Armas::find($id);
	}

	/**
	 * Updates Cam_Armas into database
	 *
	 * @param Cam_Armas $camArmas
	 * @param array $input
	 *
	 * @return Cam_Armas
	 */
	public function update($camArmas, $input)
	{
		$camArmas->fill($input);
		$camArmas->save();

		return $camArmas;
	}
}