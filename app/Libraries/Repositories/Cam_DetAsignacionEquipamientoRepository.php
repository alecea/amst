<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_DetAsignacionEquipamiento;
use Illuminate\Support\Facades\Schema;

class Cam_DetAsignacionEquipamientoRepository
{

	/**
	 * Returns all Cam_DetAsignacionEquipamientos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_DetAsignacionEquipamiento::all();
	}

	public function search($input)
    {
        $query = Cam_DetAsignacionEquipamiento::query();

        $columns = Schema::getColumnListing('cam__det_asignacion_equipamientos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_DetAsignacionEquipamiento into database
	 *
	 * @param array $input
	 *
	 * @return Cam_DetAsignacionEquipamiento
	 */
	public function store($input)
	{
		return Cam_DetAsignacionEquipamiento::create($input);
	}

	/**
	 * Find Cam_DetAsignacionEquipamiento by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_DetAsignacionEquipamiento
	 */
	public function findCam_DetAsignacionEquipamientoById($id)
	{
		return Cam_DetAsignacionEquipamiento::find($id);
	}

	/**
	 * Updates Cam_DetAsignacionEquipamiento into database
	 *
	 * @param Cam_DetAsignacionEquipamiento $camDetAsignacionEquipamiento
	 * @param array $input
	 *
	 * @return Cam_DetAsignacionEquipamiento
	 */
	public function update($camDetAsignacionEquipamiento, $input)
	{
		$camDetAsignacionEquipamiento->fill($input);
		$camDetAsignacionEquipamiento->save();

		return $camDetAsignacionEquipamiento;
	}
}