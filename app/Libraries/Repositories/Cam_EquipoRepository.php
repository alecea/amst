<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Equipo;
use Illuminate\Support\Facades\Schema;

class Cam_EquipoRepository
{

	/**
	 * Returns all Cam_Equipos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Equipo::all();
	}

	public function search($input)
    {
        $query = Cam_Equipo::query();

        $columns = Schema::getColumnListing('cam__equipos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Equipo into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Equipo
	 */
	public function store($input)
	{
		return Cam_Equipo::create($input);
	}

	/**
	 * Find Cam_Equipo by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Equipo
	 */
	public function findCam_EquipoById($id)
	{
		return Cam_Equipo::find($id);
	}

	/**
	 * Updates Cam_Equipo into database
	 *
	 * @param Cam_Equipo $camEquipo
	 * @param array $input
	 *
	 * @return Cam_Equipo
	 */
	public function update($camEquipo, $input)
	{
		$camEquipo->fill($input);
		$camEquipo->save();

		return $camEquipo;
	}
}