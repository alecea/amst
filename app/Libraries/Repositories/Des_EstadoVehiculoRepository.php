<?php

namespace App\Libraries\Repositories;


use App\Models\Des_EstadoVehiculo;
use Illuminate\Support\Facades\Schema;

class Des_EstadoVehiculoRepository
{

	/**
	 * Returns all Des_EstadoVehiculos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Des_EstadoVehiculo::all();
	}

	public function search($input)
    {
        $query = Des_EstadoVehiculo::query();

        $columns = Schema::getColumnListing('des__estado_vehiculos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Des_EstadoVehiculo into database
	 *
	 * @param array $input
	 *
	 * @return Des_EstadoVehiculo
	 */
	public function store($input)
	{
		return Des_EstadoVehiculo::create($input);
	}

	/**
	 * Find Des_EstadoVehiculo by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Des_EstadoVehiculo
	 */
	public function findDes_EstadoVehiculoById($id)
	{
		return Des_EstadoVehiculo::find($id);
	}

	/**
	 * Updates Des_EstadoVehiculo into database
	 *
	 * @param Des_EstadoVehiculo $desEstadoVehiculo
	 * @param array $input
	 *
	 * @return Des_EstadoVehiculo
	 */
	public function update($desEstadoVehiculo, $input)
	{
		$desEstadoVehiculo->fill($input);
		$desEstadoVehiculo->save();

		return $desEstadoVehiculo;
	}
}