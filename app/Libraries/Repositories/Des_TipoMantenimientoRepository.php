<?php

namespace App\Libraries\Repositories;


use App\Models\Des_TipoMantenimiento;
use Illuminate\Support\Facades\Schema;

class Des_TipoMantenimientoRepository
{

	/**
	 * Returns all Des_TipoMantenimientos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Des_TipoMantenimiento::all();
	}

	public function search($input)
    {
        $query = Des_TipoMantenimiento::query();

        $columns = Schema::getColumnListing('des__tipo_mantenimientos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Des_TipoMantenimiento into database
	 *
	 * @param array $input
	 *
	 * @return Des_TipoMantenimiento
	 */
	public function store($input)
	{
		return Des_TipoMantenimiento::create($input);
	}

	/**
	 * Find Des_TipoMantenimiento by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Des_TipoMantenimiento
	 */
	public function findDes_TipoMantenimientoById($id)
	{
		return Des_TipoMantenimiento::find($id);
	}

	/**
	 * Updates Des_TipoMantenimiento into database
	 *
	 * @param Des_TipoMantenimiento $desTipoMantenimiento
	 * @param array $input
	 *
	 * @return Des_TipoMantenimiento
	 */
	public function update($desTipoMantenimiento, $input)
	{
		$desTipoMantenimiento->fill($input);
		$desTipoMantenimiento->save();

		return $desTipoMantenimiento;
	}
}