<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Inventario;
use Illuminate\Support\Facades\Schema;

class Cam_InventarioRepository
{

	/**
	 * Returns all Cam_Inventarios
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Inventario::all();
	}

	public function search($input)
    {
        $query = Cam_Inventario::query();

        $columns = Schema::getColumnListing('cam__inventarios');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Inventario into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Inventario
	 */
	public function store($input)
	{
		return Cam_Inventario::create($input);
	}

	/**
	 * Find Cam_Inventario by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Inventario
	 */
	public function findCam_InventarioById($id)
	{
		return Cam_Inventario::find($id);
	}

	/**
	 * Updates Cam_Inventario into database
	 *
	 * @param Cam_Inventario $camInventario
	 * @param array $input
	 *
	 * @return Cam_Inventario
	 */
	public function update($camInventario, $input)
	{
		$camInventario->fill($input);
		$camInventario->save();

		return $camInventario;
	}
}