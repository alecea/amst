<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_Patrullaje;
use Illuminate\Support\Facades\Schema;

class Cam_PatrullajeRepository
{

	/**
	 * Returns all Cam_Patrullajes
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_Patrullaje::all();
	}

	public function search($input)
    {
        $query = Cam_Patrullaje::query();

        $columns = Schema::getColumnListing('cam__patrullajes');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_Patrullaje into database
	 *
	 * @param array $input
	 *
	 * @return Cam_Patrullaje
	 */
	public function store($input)
	{
		return Cam_Patrullaje::create($input);
	}

	/**
	 * Find Cam_Patrullaje by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_Patrullaje
	 */
	public function findCam_PatrullajeById($id)
	{
		return Cam_Patrullaje::find($id);
	}

	/**
	 * Updates Cam_Patrullaje into database
	 *
	 * @param Cam_Patrullaje $camPatrullaje
	 * @param array $input
	 *
	 * @return Cam_Patrullaje
	 */
	public function update($camPatrullaje, $input)
	{
		$camPatrullaje->fill($input);
		$camPatrullaje->save();

		return $camPatrullaje;
	}
}