<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_AsigEquipamiento;
use Illuminate\Support\Facades\Schema;

class Cam_AsigEquipamientoRepository
{

	/**
	 * Returns all Cam_AsigEquipamientos
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_AsigEquipamiento::all();
	}

	public function search($input)
    {
        $query = Cam_AsigEquipamiento::query();

        $columns = Schema::getColumnListing('cam__asig_equipamientos');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_AsigEquipamiento into database
	 *
	 * @param array $input
	 *
	 * @return Cam_AsigEquipamiento
	 */
	public function store($input)
	{
		return Cam_AsigEquipamiento::create($input);
	}

	/**
	 * Find Cam_AsigEquipamiento by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_AsigEquipamiento
	 */
	public function findCam_AsigEquipamientoById($id)
	{
		return Cam_AsigEquipamiento::find($id);
	}

	/**
	 * Updates Cam_AsigEquipamiento into database
	 *
	 * @param Cam_AsigEquipamiento $camAsigEquipamiento
	 * @param array $input
	 *
	 * @return Cam_AsigEquipamiento
	 */
	public function update($camAsigEquipamiento, $input)
	{
		$camAsigEquipamiento->fill($input);
		$camAsigEquipamiento->save();

		return $camAsigEquipamiento;
	}
}