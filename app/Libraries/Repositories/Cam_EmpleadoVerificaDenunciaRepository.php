<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_EmpleadoVerificaDenuncia;
use Illuminate\Support\Facades\Schema;

class Cam_EmpleadoVerificaDenunciaRepository
{

	/**
	 * Returns all Cam_EmpleadoVerificaDenuncias
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_EmpleadoVerificaDenuncia::all();
	}

	public function search($input)
    {
        $query = Cam_EmpleadoVerificaDenuncia::query();

        $columns = Schema::getColumnListing('cam__empleado_verifica_denuncias');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_EmpleadoVerificaDenuncia into database
	 *
	 * @param array $input
	 *
	 * @return Cam_EmpleadoVerificaDenuncia
	 */
	public function store($input)
	{
		return Cam_EmpleadoVerificaDenuncia::create($input);
	}

	/**
	 * Find Cam_EmpleadoVerificaDenuncia by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_EmpleadoVerificaDenuncia
	 */
	public function findCam_EmpleadoVerificaDenunciaById($id)
	{
		return Cam_EmpleadoVerificaDenuncia::find($id);
	}

	/**
	 * Updates Cam_EmpleadoVerificaDenuncia into database
	 *
	 * @param Cam_EmpleadoVerificaDenuncia $camEmpleadoVerificaDenuncia
	 * @param array $input
	 *
	 * @return Cam_EmpleadoVerificaDenuncia
	 */
	public function update($camEmpleadoVerificaDenuncia, $input)
	{
		$camEmpleadoVerificaDenuncia->fill($input);
		$camEmpleadoVerificaDenuncia->save();

		return $camEmpleadoVerificaDenuncia;
	}
}