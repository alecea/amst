<?php

namespace App\Libraries\Repositories;


use App\Models\Cam_AccionOperativa;
use Illuminate\Support\Facades\Schema;

class Cam_AccionOperativaRepository
{

	/**
	 * Returns all Cam_AccionOperativas
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Cam_AccionOperativa::all();
	}

	public function search($input)
    {
        $query = Cam_AccionOperativa::query();

        $columns = Schema::getColumnListing('cam__accion_operativas');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Cam_AccionOperativa into database
	 *
	 * @param array $input
	 *
	 * @return Cam_AccionOperativa
	 */
	public function store($input)
	{
		return Cam_AccionOperativa::create($input);
	}

	/**
	 * Find Cam_AccionOperativa by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Cam_AccionOperativa
	 */
	public function findCam_AccionOperativaById($id)
	{
		return Cam_AccionOperativa::find($id);
	}

	/**
	 * Updates Cam_AccionOperativa into database
	 *
	 * @param Cam_AccionOperativa $camAccionOperativa
	 * @param array $input
	 *
	 * @return Cam_AccionOperativa
	 */
	public function update($camAccionOperativa, $input)
	{
		$camAccionOperativa->fill($input);
		$camAccionOperativa->save();

		return $camAccionOperativa;
	}
}