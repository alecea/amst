<?php

namespace App\Libraries\Repositories;


use App\Models\Des_DiagnosticoTaller;
use Illuminate\Support\Facades\Schema;

class Des_DiagnosticoTallerRepository
{

	/**
	 * Returns all Des_DiagnosticoTallers
	 *
	 * @return \Illuminate\Database\Eloquent\Collection|static[]
	 */
	public function all()
	{
		return Des_DiagnosticoTaller::all();
	}

	public function search($input)
    {
        $query = Des_DiagnosticoTaller::query();

        $columns = Schema::getColumnListing('des__diagnostico_tallers');
        $attributes = array();

        foreach($columns as $attribute){
            if(isset($input[$attribute]))
            {
                $query->where($attribute, $input[$attribute]);
                $attributes[$attribute] =  $input[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        return [$query->get(), $attributes];

    }

	/**
	 * Stores Des_DiagnosticoTaller into database
	 *
	 * @param array $input
	 *
	 * @return Des_DiagnosticoTaller
	 */
	public function store($input)
	{
		return Des_DiagnosticoTaller::create($input);
	}

	/**
	 * Find Des_DiagnosticoTaller by given id
	 *
	 * @param int $id
	 *
	 * @return \Illuminate\Support\Collection|null|static|Des_DiagnosticoTaller
	 */
	public function findDes_DiagnosticoTallerById($id)
	{
		return Des_DiagnosticoTaller::find($id);
	}

	/**
	 * Updates Des_DiagnosticoTaller into database
	 *
	 * @param Des_DiagnosticoTaller $desDiagnosticoTaller
	 * @param array $input
	 *
	 * @return Des_DiagnosticoTaller
	 */
	public function update($desDiagnosticoTaller, $input)
	{
		$desDiagnosticoTaller->fill($input);
		$desDiagnosticoTaller->save();

		return $desDiagnosticoTaller;
	}
}