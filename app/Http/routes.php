<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', 'WelcomeController@index');
//
//Route::get('home', 'HomeController@index');
//
//Route::controllers([
//	'auth' => 'Auth\AuthController',
//	'password' => 'Auth\PasswordController',
//]);

/*
|--------------------------------------------------------------------------
| AMST
|--------------------------------------------------------------------------
| Colocar aqui el registro de rutas del sistema en general
|
*/

Route::get('/', function() {
	return View::make('amst.home.login');
});

Route::get('/home', function() {
	return View::make('amst.home.inicio');
});

Route::get('/clinica', function(Request $request) {

	Session::put('system', $request->query->get('system'));
	return View::make('amst.clinica.index');

});

Route::get('/clinica/expediente/create', function() {

	return View::make('amst.clinica.example.create');

});

Route::get('/cam', function(Request $request) {

	Session::put('system', $request->query->get('system'));
	return View::make('amst.cam.index');

});

Route::get('/transporte', function(Request $request) {

	Session::put('system', $request->query->get('system'));
	return View::make('amst.transporte.index');

});


Route::resource('ejemplos', 'ejemploController');

Route::get('ejemplos/{id}/delete', [
    'as' => 'ejemplos.delete',
    'uses' => 'ejemploController@destroy',
]);


Route::resource('camTurnos', 'Cam_TurnoController');

Route::get('camTurnos/{id}/delete', [
    'as' => 'camTurnos.delete',
    'uses' => 'Cam_TurnoController@destroy',
]);


Route::resource('desTipoVehiculos', 'Des_TipoVehiculoController');

Route::get('desTipoVehiculos/{id}/delete', [
    'as' => 'desTipoVehiculos.delete',
    'uses' => 'Des_TipoVehiculoController@destroy',
]);


Route::resource('genEmpleados', 'Gen_EmpleadoController');

Route::get('genEmpleados/{id}/delete', [
    'as' => 'genEmpleados.delete',
    'uses' => 'Gen_EmpleadoController@destroy',
]);


Route::resource('desVehiculos', 'Des_VehiculoController');

Route::get('desVehiculos/{id}/delete', [
    'as' => 'desVehiculos.delete',
    'uses' => 'Des_VehiculoController@destroy',
]);


Route::resource('genEmpleados', 'Gen_EmpleadoController');

Route::get('genEmpleados/{id}/delete', [
    'as' => 'genEmpleados.delete',
    'uses' => 'Gen_EmpleadoController@destroy',
]);


Route::resource('desDetDiagnosticos', 'Des_DetDiagnosticoController');

Route::get('desDetDiagnosticos/{id}/delete', [
    'as' => 'desDetDiagnosticos.delete',
    'uses' => 'Des_DetDiagnosticoController@destroy',
]);


Route::resource('desDiagnosticoTallers', 'Des_DiagnosticoTallerController');

Route::get('desDiagnosticoTallers/{id}/delete', [
    'as' => 'desDiagnosticoTallers.delete',
    'uses' => 'Des_DiagnosticoTallerController@destroy',
]);


Route::resource('desEstadoVehiculos', 'Des_EstadoVehiculoController');

Route::get('desEstadoVehiculos/{id}/delete', [
    'as' => 'desEstadoVehiculos.delete',
    'uses' => 'Des_EstadoVehiculoController@destroy',
]);
