<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDes_TipoMantenimientoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Des_TipoMantenimientoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Des_TipoMantenimientoController extends AppBaseController
{

	/** @var  Des_TipoMantenimientoRepository */
	private $desTipoMantenimientoRepository;

	function __construct(Des_TipoMantenimientoRepository $desTipoMantenimientoRepo)
	{
		$this->desTipoMantenimientoRepository = $desTipoMantenimientoRepo;
	}

	/**
	 * Display a listing of the Des_TipoMantenimiento.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->desTipoMantenimientoRepository->search($input);

		$desTipoMantenimientos = $result[0];

		$attributes = $result[1];

		return view('desTipoMantenimientos.index')
		    ->with('desTipoMantenimientos', $desTipoMantenimientos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Des_TipoMantenimiento.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('desTipoMantenimientos.create');
	}

	/**
	 * Store a newly created Des_TipoMantenimiento in storage.
	 *
	 * @param CreateDes_TipoMantenimientoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDes_TipoMantenimientoRequest $request)
	{
        $input = $request->all();

		$desTipoMantenimiento = $this->desTipoMantenimientoRepository->store($input);

		Flash::message('Des_TipoMantenimiento saved successfully.');

		return redirect(route('desTipoMantenimientos.index'));
	}

	/**
	 * Display the specified Des_TipoMantenimiento.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$desTipoMantenimiento = $this->desTipoMantenimientoRepository->findDes_TipoMantenimientoById($id);

		if(empty($desTipoMantenimiento))
		{
			Flash::error('Des_TipoMantenimiento not found');
			return redirect(route('desTipoMantenimientos.index'));
		}

		return view('desTipoMantenimientos.show')->with('desTipoMantenimiento', $desTipoMantenimiento);
	}

	/**
	 * Show the form for editing the specified Des_TipoMantenimiento.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$desTipoMantenimiento = $this->desTipoMantenimientoRepository->findDes_TipoMantenimientoById($id);

		if(empty($desTipoMantenimiento))
		{
			Flash::error('Des_TipoMantenimiento not found');
			return redirect(route('desTipoMantenimientos.index'));
		}

		return view('desTipoMantenimientos.edit')->with('desTipoMantenimiento', $desTipoMantenimiento);
	}

	/**
	 * Update the specified Des_TipoMantenimiento in storage.
	 *
	 * @param  int    $id
	 * @param CreateDes_TipoMantenimientoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateDes_TipoMantenimientoRequest $request)
	{
		$desTipoMantenimiento = $this->desTipoMantenimientoRepository->findDes_TipoMantenimientoById($id);

		if(empty($desTipoMantenimiento))
		{
			Flash::error('Des_TipoMantenimiento not found');
			return redirect(route('desTipoMantenimientos.index'));
		}

		$desTipoMantenimiento = $this->desTipoMantenimientoRepository->update($desTipoMantenimiento, $request->all());

		Flash::message('Des_TipoMantenimiento updated successfully.');

		return redirect(route('desTipoMantenimientos.index'));
	}

	/**
	 * Remove the specified Des_TipoMantenimiento from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$desTipoMantenimiento = $this->desTipoMantenimientoRepository->findDes_TipoMantenimientoById($id);

		if(empty($desTipoMantenimiento))
		{
			Flash::error('Des_TipoMantenimiento not found');
			return redirect(route('desTipoMantenimientos.index'));
		}

		$desTipoMantenimiento->delete();

		Flash::message('Des_TipoMantenimiento deleted successfully.');

		return redirect(route('desTipoMantenimientos.index'));
	}

}
