<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_TipoInfraccionRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_TipoInfraccionRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_TipoInfraccionController extends AppBaseController
{

	/** @var  Cam_TipoInfraccionRepository */
	private $camTipoInfraccionRepository;

	function __construct(Cam_TipoInfraccionRepository $camTipoInfraccionRepo)
	{
		$this->camTipoInfraccionRepository = $camTipoInfraccionRepo;
	}

	/**
	 * Display a listing of the Cam_TipoInfraccion.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camTipoInfraccionRepository->search($input);

		$camTipoInfraccions = $result[0];

		$attributes = $result[1];

		return view('camTipoInfraccions.index')
		    ->with('camTipoInfraccions', $camTipoInfraccions)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_TipoInfraccion.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camTipoInfraccions.create');
	}

	/**
	 * Store a newly created Cam_TipoInfraccion in storage.
	 *
	 * @param CreateCam_TipoInfraccionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_TipoInfraccionRequest $request)
	{
        $input = $request->all();

		$camTipoInfraccion = $this->camTipoInfraccionRepository->store($input);

		Flash::message('Cam_TipoInfraccion saved successfully.');

		return redirect(route('camTipoInfraccions.index'));
	}

	/**
	 * Display the specified Cam_TipoInfraccion.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camTipoInfraccion = $this->camTipoInfraccionRepository->findCam_TipoInfraccionById($id);

		if(empty($camTipoInfraccion))
		{
			Flash::error('Cam_TipoInfraccion not found');
			return redirect(route('camTipoInfraccions.index'));
		}

		return view('camTipoInfraccions.show')->with('camTipoInfraccion', $camTipoInfraccion);
	}

	/**
	 * Show the form for editing the specified Cam_TipoInfraccion.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camTipoInfraccion = $this->camTipoInfraccionRepository->findCam_TipoInfraccionById($id);

		if(empty($camTipoInfraccion))
		{
			Flash::error('Cam_TipoInfraccion not found');
			return redirect(route('camTipoInfraccions.index'));
		}

		return view('camTipoInfraccions.edit')->with('camTipoInfraccion', $camTipoInfraccion);
	}

	/**
	 * Update the specified Cam_TipoInfraccion in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_TipoInfraccionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_TipoInfraccionRequest $request)
	{
		$camTipoInfraccion = $this->camTipoInfraccionRepository->findCam_TipoInfraccionById($id);

		if(empty($camTipoInfraccion))
		{
			Flash::error('Cam_TipoInfraccion not found');
			return redirect(route('camTipoInfraccions.index'));
		}

		$camTipoInfraccion = $this->camTipoInfraccionRepository->update($camTipoInfraccion, $request->all());

		Flash::message('Cam_TipoInfraccion updated successfully.');

		return redirect(route('camTipoInfraccions.index'));
	}

	/**
	 * Remove the specified Cam_TipoInfraccion from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camTipoInfraccion = $this->camTipoInfraccionRepository->findCam_TipoInfraccionById($id);

		if(empty($camTipoInfraccion))
		{
			Flash::error('Cam_TipoInfraccion not found');
			return redirect(route('camTipoInfraccions.index'));
		}

		$camTipoInfraccion->delete();

		Flash::message('Cam_TipoInfraccion deleted successfully.');

		return redirect(route('camTipoInfraccions.index'));
	}

}
