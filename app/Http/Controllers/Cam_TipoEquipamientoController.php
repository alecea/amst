<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_TipoEquipamientoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_TipoEquipamientoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_TipoEquipamientoController extends AppBaseController
{

	/** @var  Cam_TipoEquipamientoRepository */
	private $camTipoEquipamientoRepository;

	function __construct(Cam_TipoEquipamientoRepository $camTipoEquipamientoRepo)
	{
		$this->camTipoEquipamientoRepository = $camTipoEquipamientoRepo;
	}

	/**
	 * Display a listing of the Cam_TipoEquipamiento.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camTipoEquipamientoRepository->search($input);

		$camTipoEquipamientos = $result[0];

		$attributes = $result[1];

		return view('camTipoEquipamientos.index')
		    ->with('camTipoEquipamientos', $camTipoEquipamientos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_TipoEquipamiento.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camTipoEquipamientos.create');
	}

	/**
	 * Store a newly created Cam_TipoEquipamiento in storage.
	 *
	 * @param CreateCam_TipoEquipamientoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_TipoEquipamientoRequest $request)
	{
        $input = $request->all();

		$camTipoEquipamiento = $this->camTipoEquipamientoRepository->store($input);

		Flash::message('Cam_TipoEquipamiento saved successfully.');

		return redirect(route('camTipoEquipamientos.index'));
	}

	/**
	 * Display the specified Cam_TipoEquipamiento.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camTipoEquipamiento = $this->camTipoEquipamientoRepository->findCam_TipoEquipamientoById($id);

		if(empty($camTipoEquipamiento))
		{
			Flash::error('Cam_TipoEquipamiento not found');
			return redirect(route('camTipoEquipamientos.index'));
		}

		return view('camTipoEquipamientos.show')->with('camTipoEquipamiento', $camTipoEquipamiento);
	}

	/**
	 * Show the form for editing the specified Cam_TipoEquipamiento.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camTipoEquipamiento = $this->camTipoEquipamientoRepository->findCam_TipoEquipamientoById($id);

		if(empty($camTipoEquipamiento))
		{
			Flash::error('Cam_TipoEquipamiento not found');
			return redirect(route('camTipoEquipamientos.index'));
		}

		return view('camTipoEquipamientos.edit')->with('camTipoEquipamiento', $camTipoEquipamiento);
	}

	/**
	 * Update the specified Cam_TipoEquipamiento in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_TipoEquipamientoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_TipoEquipamientoRequest $request)
	{
		$camTipoEquipamiento = $this->camTipoEquipamientoRepository->findCam_TipoEquipamientoById($id);

		if(empty($camTipoEquipamiento))
		{
			Flash::error('Cam_TipoEquipamiento not found');
			return redirect(route('camTipoEquipamientos.index'));
		}

		$camTipoEquipamiento = $this->camTipoEquipamientoRepository->update($camTipoEquipamiento, $request->all());

		Flash::message('Cam_TipoEquipamiento updated successfully.');

		return redirect(route('camTipoEquipamientos.index'));
	}

	/**
	 * Remove the specified Cam_TipoEquipamiento from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camTipoEquipamiento = $this->camTipoEquipamientoRepository->findCam_TipoEquipamientoById($id);

		if(empty($camTipoEquipamiento))
		{
			Flash::error('Cam_TipoEquipamiento not found');
			return redirect(route('camTipoEquipamientos.index'));
		}

		$camTipoEquipamiento->delete();

		Flash::message('Cam_TipoEquipamiento deleted successfully.');

		return redirect(route('camTipoEquipamientos.index'));
	}

}
