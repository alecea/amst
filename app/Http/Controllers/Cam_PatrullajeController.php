<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_PatrullajeRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_PatrullajeRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_PatrullajeController extends AppBaseController
{

	/** @var  Cam_PatrullajeRepository */
	private $camPatrullajeRepository;

	function __construct(Cam_PatrullajeRepository $camPatrullajeRepo)
	{
		$this->camPatrullajeRepository = $camPatrullajeRepo;
	}

	/**
	 * Display a listing of the Cam_Patrullaje.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camPatrullajeRepository->search($input);

		$camPatrullajes = $result[0];

		$attributes = $result[1];

		return view('camPatrullajes.index')
		    ->with('camPatrullajes', $camPatrullajes)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Patrullaje.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camPatrullajes.create');
	}

	/**
	 * Store a newly created Cam_Patrullaje in storage.
	 *
	 * @param CreateCam_PatrullajeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_PatrullajeRequest $request)
	{
        $input = $request->all();

		$camPatrullaje = $this->camPatrullajeRepository->store($input);

		Flash::message('Cam_Patrullaje saved successfully.');

		return redirect(route('camPatrullajes.index'));
	}

	/**
	 * Display the specified Cam_Patrullaje.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camPatrullaje = $this->camPatrullajeRepository->findCam_PatrullajeById($id);

		if(empty($camPatrullaje))
		{
			Flash::error('Cam_Patrullaje not found');
			return redirect(route('camPatrullajes.index'));
		}

		return view('camPatrullajes.show')->with('camPatrullaje', $camPatrullaje);
	}

	/**
	 * Show the form for editing the specified Cam_Patrullaje.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camPatrullaje = $this->camPatrullajeRepository->findCam_PatrullajeById($id);

		if(empty($camPatrullaje))
		{
			Flash::error('Cam_Patrullaje not found');
			return redirect(route('camPatrullajes.index'));
		}

		return view('camPatrullajes.edit')->with('camPatrullaje', $camPatrullaje);
	}

	/**
	 * Update the specified Cam_Patrullaje in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_PatrullajeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_PatrullajeRequest $request)
	{
		$camPatrullaje = $this->camPatrullajeRepository->findCam_PatrullajeById($id);

		if(empty($camPatrullaje))
		{
			Flash::error('Cam_Patrullaje not found');
			return redirect(route('camPatrullajes.index'));
		}

		$camPatrullaje = $this->camPatrullajeRepository->update($camPatrullaje, $request->all());

		Flash::message('Cam_Patrullaje updated successfully.');

		return redirect(route('camPatrullajes.index'));
	}

	/**
	 * Remove the specified Cam_Patrullaje from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camPatrullaje = $this->camPatrullajeRepository->findCam_PatrullajeById($id);

		if(empty($camPatrullaje))
		{
			Flash::error('Cam_Patrullaje not found');
			return redirect(route('camPatrullajes.index'));
		}

		$camPatrullaje->delete();

		Flash::message('Cam_Patrullaje deleted successfully.');

		return redirect(route('camPatrullajes.index'));
	}

}
