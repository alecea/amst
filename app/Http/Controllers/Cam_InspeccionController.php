<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_InspeccionRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_InspeccionRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_InspeccionController extends AppBaseController
{

	/** @var  Cam_InspeccionRepository */
	private $camInspeccionRepository;

	function __construct(Cam_InspeccionRepository $camInspeccionRepo)
	{
		$this->camInspeccionRepository = $camInspeccionRepo;
	}

	/**
	 * Display a listing of the Cam_Inspeccion.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camInspeccionRepository->search($input);

		$camInspeccions = $result[0];

		$attributes = $result[1];

		return view('amst.cam.index')
		    ->with('camInspeccions', $camInspeccions)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Inspeccion.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('amst.cam.camInspeccions.create');
	}

	/**
	 * Store a newly created Cam_Inspeccion in storage.
	 *
	 * @param CreateCam_InspeccionRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_InspeccionRequest $request)
	{
        $input = $request->all();

		$camInspeccion = $this->camInspeccionRepository->store($input);

		Flash::message('Cam_Inspeccion saved successfully.');

		return redirect(route('camInspeccions.index'));
	}

	/**
	 * Display the specified Cam_Inspeccion.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camInspeccion = $this->camInspeccionRepository->findCam_InspeccionById($id);

		if(empty($camInspeccion))
		{
			Flash::error('Cam_Inspeccion not found');
			return redirect(route('camInspeccions.index'));
		}

		return view('camInspeccions.show')->with('camInspeccion', $camInspeccion);
	}

	/**
	 * Show the form for editing the specified Cam_Inspeccion.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camInspeccion = $this->camInspeccionRepository->findCam_InspeccionById($id);

		if(empty($camInspeccion))
		{
			Flash::error('Cam_Inspeccion not found');
			return redirect(route('camInspeccions.index'));
		}

		return view('camInspeccions.edit')->with('camInspeccion', $camInspeccion);
	}

	/**
	 * Update the specified Cam_Inspeccion in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_InspeccionRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_InspeccionRequest $request)
	{
		$camInspeccion = $this->camInspeccionRepository->findCam_InspeccionById($id);

		if(empty($camInspeccion))
		{
			Flash::error('Cam_Inspeccion not found');
			return redirect(route('camInspeccions.index'));
		}

		$camInspeccion = $this->camInspeccionRepository->update($camInspeccion, $request->all());

		Flash::message('Cam_Inspeccion updated successfully.');

		return redirect(route('camInspeccions.index'));
	}

	/**
	 * Remove the specified Cam_Inspeccion from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camInspeccion = $this->camInspeccionRepository->findCam_InspeccionById($id);

		if(empty($camInspeccion))
		{
			Flash::error('Cam_Inspeccion not found');
			return redirect(route('camInspeccions.index'));
		}

		$camInspeccion->delete();

		Flash::message('Cam_Inspeccion deleted successfully.');

		return redirect(route('camInspeccions.index'));
	}

}
