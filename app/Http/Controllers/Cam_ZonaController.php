<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_ZonaRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_ZonaRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_ZonaController extends AppBaseController
{

	/** @var  Cam_ZonaRepository */
	private $camZonaRepository;

	function __construct(Cam_ZonaRepository $camZonaRepo)
	{
		$this->camZonaRepository = $camZonaRepo;
	}

	/**
	 * Display a listing of the Cam_Zona.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camZonaRepository->search($input);

		$camZonas = $result[0];

		$attributes = $result[1];

		return view('camZonas.index')
		    ->with('camZonas', $camZonas)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Zona.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camZonas.create');
	}

	/**
	 * Store a newly created Cam_Zona in storage.
	 *
	 * @param CreateCam_ZonaRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_ZonaRequest $request)
	{
        $input = $request->all();

		$camZona = $this->camZonaRepository->store($input);

		Flash::message('Cam_Zona saved successfully.');

		return redirect(route('camZonas.index'));
	}

	/**
	 * Display the specified Cam_Zona.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camZona = $this->camZonaRepository->findCam_ZonaById($id);

		if(empty($camZona))
		{
			Flash::error('Cam_Zona not found');
			return redirect(route('camZonas.index'));
		}

		return view('camZonas.show')->with('camZona', $camZona);
	}

	/**
	 * Show the form for editing the specified Cam_Zona.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camZona = $this->camZonaRepository->findCam_ZonaById($id);

		if(empty($camZona))
		{
			Flash::error('Cam_Zona not found');
			return redirect(route('camZonas.index'));
		}

		return view('camZonas.edit')->with('camZona', $camZona);
	}

	/**
	 * Update the specified Cam_Zona in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_ZonaRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_ZonaRequest $request)
	{
		$camZona = $this->camZonaRepository->findCam_ZonaById($id);

		if(empty($camZona))
		{
			Flash::error('Cam_Zona not found');
			return redirect(route('camZonas.index'));
		}

		$camZona = $this->camZonaRepository->update($camZona, $request->all());

		Flash::message('Cam_Zona updated successfully.');

		return redirect(route('camZonas.index'));
	}

	/**
	 * Remove the specified Cam_Zona from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camZona = $this->camZonaRepository->findCam_ZonaById($id);

		if(empty($camZona))
		{
			Flash::error('Cam_Zona not found');
			return redirect(route('camZonas.index'));
		}

		$camZona->delete();

		Flash::message('Cam_Zona deleted successfully.');

		return redirect(route('camZonas.index'));
	}

}
