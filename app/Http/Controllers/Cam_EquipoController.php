<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_EquipoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_EquipoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_EquipoController extends AppBaseController
{

	/** @var  Cam_EquipoRepository */
	private $camEquipoRepository;

	function __construct(Cam_EquipoRepository $camEquipoRepo)
	{
		$this->camEquipoRepository = $camEquipoRepo;
	}

	/**
	 * Display a listing of the Cam_Equipo.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camEquipoRepository->search($input);

		$camEquipos = $result[0];

		$attributes = $result[1];

		return view('camEquipos.index')
		    ->with('camEquipos', $camEquipos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Equipo.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camEquipos.create');
	}

	/**
	 * Store a newly created Cam_Equipo in storage.
	 *
	 * @param CreateCam_EquipoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_EquipoRequest $request)
	{
        $input = $request->all();

		$camEquipo = $this->camEquipoRepository->store($input);

		Flash::message('Cam_Equipo saved successfully.');

		return redirect(route('camEquipos.index'));
	}

	/**
	 * Display the specified Cam_Equipo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camEquipo = $this->camEquipoRepository->findCam_EquipoById($id);

		if(empty($camEquipo))
		{
			Flash::error('Cam_Equipo not found');
			return redirect(route('camEquipos.index'));
		}

		return view('camEquipos.show')->with('camEquipo', $camEquipo);
	}

	/**
	 * Show the form for editing the specified Cam_Equipo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camEquipo = $this->camEquipoRepository->findCam_EquipoById($id);

		if(empty($camEquipo))
		{
			Flash::error('Cam_Equipo not found');
			return redirect(route('camEquipos.index'));
		}

		return view('camEquipos.edit')->with('camEquipo', $camEquipo);
	}

	/**
	 * Update the specified Cam_Equipo in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_EquipoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_EquipoRequest $request)
	{
		$camEquipo = $this->camEquipoRepository->findCam_EquipoById($id);

		if(empty($camEquipo))
		{
			Flash::error('Cam_Equipo not found');
			return redirect(route('camEquipos.index'));
		}

		$camEquipo = $this->camEquipoRepository->update($camEquipo, $request->all());

		Flash::message('Cam_Equipo updated successfully.');

		return redirect(route('camEquipos.index'));
	}

	/**
	 * Remove the specified Cam_Equipo from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camEquipo = $this->camEquipoRepository->findCam_EquipoById($id);

		if(empty($camEquipo))
		{
			Flash::error('Cam_Equipo not found');
			return redirect(route('camEquipos.index'));
		}

		$camEquipo->delete();

		Flash::message('Cam_Equipo deleted successfully.');

		return redirect(route('camEquipos.index'));
	}

}
