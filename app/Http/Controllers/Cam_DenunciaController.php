<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_DenunciaRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_DenunciaRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_DenunciaController extends AppBaseController
{

	/** @var  Cam_DenunciaRepository */
	private $camDenunciaRepository;

	function __construct(Cam_DenunciaRepository $camDenunciaRepo)
	{
		$this->camDenunciaRepository = $camDenunciaRepo;
	}

	/**
	 * Display a listing of the Cam_Denuncia.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camDenunciaRepository->search($input);

		$camDenuncias = $result[0];

		$attributes = $result[1];

		return view('camDenuncias.index')
		    ->with('camDenuncias', $camDenuncias)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Denuncia.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camDenuncias.create');
	}

	/**
	 * Store a newly created Cam_Denuncia in storage.
	 *
	 * @param CreateCam_DenunciaRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_DenunciaRequest $request)
	{
        $input = $request->all();

		$camDenuncia = $this->camDenunciaRepository->store($input);

		Flash::message('Cam_Denuncia saved successfully.');

		return redirect(route('camDenuncias.index'));
	}

	/**
	 * Display the specified Cam_Denuncia.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camDenuncia = $this->camDenunciaRepository->findCam_DenunciaById($id);

		if(empty($camDenuncia))
		{
			Flash::error('Cam_Denuncia not found');
			return redirect(route('camDenuncias.index'));
		}

		return view('camDenuncias.show')->with('camDenuncia', $camDenuncia);
	}

	/**
	 * Show the form for editing the specified Cam_Denuncia.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camDenuncia = $this->camDenunciaRepository->findCam_DenunciaById($id);

		if(empty($camDenuncia))
		{
			Flash::error('Cam_Denuncia not found');
			return redirect(route('camDenuncias.index'));
		}

		return view('camDenuncias.edit')->with('camDenuncia', $camDenuncia);
	}

	/**
	 * Update the specified Cam_Denuncia in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_DenunciaRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_DenunciaRequest $request)
	{
		$camDenuncia = $this->camDenunciaRepository->findCam_DenunciaById($id);

		if(empty($camDenuncia))
		{
			Flash::error('Cam_Denuncia not found');
			return redirect(route('camDenuncias.index'));
		}

		$camDenuncia = $this->camDenunciaRepository->update($camDenuncia, $request->all());

		Flash::message('Cam_Denuncia updated successfully.');

		return redirect(route('camDenuncias.index'));
	}

	/**
	 * Remove the specified Cam_Denuncia from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camDenuncia = $this->camDenunciaRepository->findCam_DenunciaById($id);

		if(empty($camDenuncia))
		{
			Flash::error('Cam_Denuncia not found');
			return redirect(route('camDenuncias.index'));
		}

		$camDenuncia->delete();

		Flash::message('Cam_Denuncia deleted successfully.');

		return redirect(route('camDenuncias.index'));
	}

}
