<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_ArmasRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_ArmasRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_ArmasController extends AppBaseController
{

	/** @var  Cam_ArmasRepository */
	private $camArmasRepository;

	function __construct(Cam_ArmasRepository $camArmasRepo)
	{
		$this->camArmasRepository = $camArmasRepo;
	}

	/**
	 * Display a listing of the Cam_Armas.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camArmasRepository->search($input);

		$camArmas = $result[0];

		$attributes = $result[1];

		return view('camArmas.index')
		    ->with('camArmas', $camArmas)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Armas.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('amst.cam.camArmas.create');
	}

	/**
	 * Store a newly created Cam_Armas in storage.
	 *
	 * @param CreateCam_ArmasRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_ArmasRequest $request)
	{
        $input = $request->all();

		$camArmas = $this->camArmasRepository->store($input);

		Flash::message('Cam_Armas saved successfully.');

		return redirect(route('camArmas.index'));
	}

	/**
	 * Display the specified Cam_Armas.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camArmas = $this->camArmasRepository->findCam_ArmasById($id);

		if(empty($camArmas))
		{
			Flash::error('Cam_Armas not found');
			return redirect(route('camArmas.index'));
		}

		return view('camArmas.show')->with('camArmas', $camArmas);
	}

	/**
	 * Show the form for editing the specified Cam_Armas.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camArmas = $this->camArmasRepository->findCam_ArmasById($id);

		if(empty($camArmas))
		{
			Flash::error('Cam_Armas not found');
			return redirect(route('camArmas.index'));
		}

		return view('camArmas.edit')->with('camArmas', $camArmas);
	}

	/**
	 * Update the specified Cam_Armas in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_ArmasRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_ArmasRequest $request)
	{
		$camArmas = $this->camArmasRepository->findCam_ArmasById($id);

		if(empty($camArmas))
		{
			Flash::error('Cam_Armas not found');
			return redirect(route('camArmas.index'));
		}

		$camArmas = $this->camArmasRepository->update($camArmas, $request->all());

		Flash::message('Cam_Armas updated successfully.');

		return redirect(route('camArmas.index'));
	}

	/**
	 * Remove the specified Cam_Armas from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camArmas = $this->camArmasRepository->findCam_ArmasById($id);

		if(empty($camArmas))
		{
			Flash::error('Cam_Armas not found');
			return redirect(route('camArmas.index'));
		}

		$camArmas->delete();

		Flash::message('Cam_Armas deleted successfully.');

		return redirect(route('camArmas.index'));
	}

}
