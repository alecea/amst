<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDes_DetDiagnosticoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Des_DetDiagnosticoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Des_DetDiagnosticoController extends AppBaseController
{

	/** @var  Des_DetDiagnosticoRepository */
	private $desDetDiagnosticoRepository;

	function __construct(Des_DetDiagnosticoRepository $desDetDiagnosticoRepo)
	{
		$this->desDetDiagnosticoRepository = $desDetDiagnosticoRepo;
	}

	/**
	 * Display a listing of the Des_DetDiagnostico.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->desDetDiagnosticoRepository->search($input);

		$desDetDiagnosticos = $result[0];

		$attributes = $result[1];

		return view('desDetDiagnosticos.index')
		    ->with('desDetDiagnosticos', $desDetDiagnosticos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Des_DetDiagnostico.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('desDetDiagnosticos.create');
	}

	/**
	 * Store a newly created Des_DetDiagnostico in storage.
	 *
	 * @param CreateDes_DetDiagnosticoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDes_DetDiagnosticoRequest $request)
	{
        $input = $request->all();

		$desDetDiagnostico = $this->desDetDiagnosticoRepository->store($input);

		Flash::message('Des_DetDiagnostico saved successfully.');

		return redirect(route('desDetDiagnosticos.index'));
	}

	/**
	 * Display the specified Des_DetDiagnostico.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$desDetDiagnostico = $this->desDetDiagnosticoRepository->findDes_DetDiagnosticoById($id);

		if(empty($desDetDiagnostico))
		{
			Flash::error('Des_DetDiagnostico not found');
			return redirect(route('desDetDiagnosticos.index'));
		}

		return view('desDetDiagnosticos.show')->with('desDetDiagnostico', $desDetDiagnostico);
	}

	/**
	 * Show the form for editing the specified Des_DetDiagnostico.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$desDetDiagnostico = $this->desDetDiagnosticoRepository->findDes_DetDiagnosticoById($id);

		if(empty($desDetDiagnostico))
		{
			Flash::error('Des_DetDiagnostico not found');
			return redirect(route('desDetDiagnosticos.index'));
		}

		return view('desDetDiagnosticos.edit')->with('desDetDiagnostico', $desDetDiagnostico);
	}

	/**
	 * Update the specified Des_DetDiagnostico in storage.
	 *
	 * @param  int    $id
	 * @param CreateDes_DetDiagnosticoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateDes_DetDiagnosticoRequest $request)
	{
		$desDetDiagnostico = $this->desDetDiagnosticoRepository->findDes_DetDiagnosticoById($id);

		if(empty($desDetDiagnostico))
		{
			Flash::error('Des_DetDiagnostico not found');
			return redirect(route('desDetDiagnosticos.index'));
		}

		$desDetDiagnostico = $this->desDetDiagnosticoRepository->update($desDetDiagnostico, $request->all());

		Flash::message('Des_DetDiagnostico updated successfully.');

		return redirect(route('desDetDiagnosticos.index'));
	}

	/**
	 * Remove the specified Des_DetDiagnostico from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$desDetDiagnostico = $this->desDetDiagnosticoRepository->findDes_DetDiagnosticoById($id);

		if(empty($desDetDiagnostico))
		{
			Flash::error('Des_DetDiagnostico not found');
			return redirect(route('desDetDiagnosticos.index'));
		}

		$desDetDiagnostico->delete();

		Flash::message('Des_DetDiagnostico deleted successfully.');

		return redirect(route('desDetDiagnosticos.index'));
	}

}
