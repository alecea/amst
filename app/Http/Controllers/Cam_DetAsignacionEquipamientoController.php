<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_DetAsignacionEquipamientoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_DetAsignacionEquipamientoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_DetAsignacionEquipamientoController extends AppBaseController
{

	/** @var  Cam_DetAsignacionEquipamientoRepository */
	private $camDetAsignacionEquipamientoRepository;

	function __construct(Cam_DetAsignacionEquipamientoRepository $camDetAsignacionEquipamientoRepo)
	{
		$this->camDetAsignacionEquipamientoRepository = $camDetAsignacionEquipamientoRepo;
	}

	/**
	 * Display a listing of the Cam_DetAsignacionEquipamiento.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camDetAsignacionEquipamientoRepository->search($input);

		$camDetAsignacionEquipamientos = $result[0];

		$attributes = $result[1];

		return view('camDetAsignacionEquipamientos.index')
		    ->with('camDetAsignacionEquipamientos', $camDetAsignacionEquipamientos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_DetAsignacionEquipamiento.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camDetAsignacionEquipamientos.create');
	}

	/**
	 * Store a newly created Cam_DetAsignacionEquipamiento in storage.
	 *
	 * @param CreateCam_DetAsignacionEquipamientoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_DetAsignacionEquipamientoRequest $request)
	{
        $input = $request->all();

		$camDetAsignacionEquipamiento = $this->camDetAsignacionEquipamientoRepository->store($input);

		Flash::message('Cam_DetAsignacionEquipamiento saved successfully.');

		return redirect(route('camDetAsignacionEquipamientos.index'));
	}

	/**
	 * Display the specified Cam_DetAsignacionEquipamiento.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camDetAsignacionEquipamiento = $this->camDetAsignacionEquipamientoRepository->findCam_DetAsignacionEquipamientoById($id);

		if(empty($camDetAsignacionEquipamiento))
		{
			Flash::error('Cam_DetAsignacionEquipamiento not found');
			return redirect(route('camDetAsignacionEquipamientos.index'));
		}

		return view('camDetAsignacionEquipamientos.show')->with('camDetAsignacionEquipamiento', $camDetAsignacionEquipamiento);
	}

	/**
	 * Show the form for editing the specified Cam_DetAsignacionEquipamiento.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camDetAsignacionEquipamiento = $this->camDetAsignacionEquipamientoRepository->findCam_DetAsignacionEquipamientoById($id);

		if(empty($camDetAsignacionEquipamiento))
		{
			Flash::error('Cam_DetAsignacionEquipamiento not found');
			return redirect(route('camDetAsignacionEquipamientos.index'));
		}

		return view('camDetAsignacionEquipamientos.edit')->with('camDetAsignacionEquipamiento', $camDetAsignacionEquipamiento);
	}

	/**
	 * Update the specified Cam_DetAsignacionEquipamiento in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_DetAsignacionEquipamientoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_DetAsignacionEquipamientoRequest $request)
	{
		$camDetAsignacionEquipamiento = $this->camDetAsignacionEquipamientoRepository->findCam_DetAsignacionEquipamientoById($id);

		if(empty($camDetAsignacionEquipamiento))
		{
			Flash::error('Cam_DetAsignacionEquipamiento not found');
			return redirect(route('camDetAsignacionEquipamientos.index'));
		}

		$camDetAsignacionEquipamiento = $this->camDetAsignacionEquipamientoRepository->update($camDetAsignacionEquipamiento, $request->all());

		Flash::message('Cam_DetAsignacionEquipamiento updated successfully.');

		return redirect(route('camDetAsignacionEquipamientos.index'));
	}

	/**
	 * Remove the specified Cam_DetAsignacionEquipamiento from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camDetAsignacionEquipamiento = $this->camDetAsignacionEquipamientoRepository->findCam_DetAsignacionEquipamientoById($id);

		if(empty($camDetAsignacionEquipamiento))
		{
			Flash::error('Cam_DetAsignacionEquipamiento not found');
			return redirect(route('camDetAsignacionEquipamientos.index'));
		}

		$camDetAsignacionEquipamiento->delete();

		Flash::message('Cam_DetAsignacionEquipamiento deleted successfully.');

		return redirect(route('camDetAsignacionEquipamientos.index'));
	}

}
