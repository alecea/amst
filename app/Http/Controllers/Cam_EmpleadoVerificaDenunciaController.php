<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_EmpleadoVerificaDenunciaRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_EmpleadoVerificaDenunciaRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_EmpleadoVerificaDenunciaController extends AppBaseController
{

	/** @var  Cam_EmpleadoVerificaDenunciaRepository */
	private $camEmpleadoVerificaDenunciaRepository;

	function __construct(Cam_EmpleadoVerificaDenunciaRepository $camEmpleadoVerificaDenunciaRepo)
	{
		$this->camEmpleadoVerificaDenunciaRepository = $camEmpleadoVerificaDenunciaRepo;
	}

	/**
	 * Display a listing of the Cam_EmpleadoVerificaDenuncia.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camEmpleadoVerificaDenunciaRepository->search($input);

		$camEmpleadoVerificaDenuncias = $result[0];

		$attributes = $result[1];

		return view('camEmpleadoVerificaDenuncias.index')
		    ->with('camEmpleadoVerificaDenuncias', $camEmpleadoVerificaDenuncias)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_EmpleadoVerificaDenuncia.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camEmpleadoVerificaDenuncias.create');
	}

	/**
	 * Store a newly created Cam_EmpleadoVerificaDenuncia in storage.
	 *
	 * @param CreateCam_EmpleadoVerificaDenunciaRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_EmpleadoVerificaDenunciaRequest $request)
	{
        $input = $request->all();

		$camEmpleadoVerificaDenuncia = $this->camEmpleadoVerificaDenunciaRepository->store($input);

		Flash::message('Cam_EmpleadoVerificaDenuncia saved successfully.');

		return redirect(route('camEmpleadoVerificaDenuncias.index'));
	}

	/**
	 * Display the specified Cam_EmpleadoVerificaDenuncia.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camEmpleadoVerificaDenuncia = $this->camEmpleadoVerificaDenunciaRepository->findCam_EmpleadoVerificaDenunciaById($id);

		if(empty($camEmpleadoVerificaDenuncia))
		{
			Flash::error('Cam_EmpleadoVerificaDenuncia not found');
			return redirect(route('camEmpleadoVerificaDenuncias.index'));
		}

		return view('camEmpleadoVerificaDenuncias.show')->with('camEmpleadoVerificaDenuncia', $camEmpleadoVerificaDenuncia);
	}

	/**
	 * Show the form for editing the specified Cam_EmpleadoVerificaDenuncia.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camEmpleadoVerificaDenuncia = $this->camEmpleadoVerificaDenunciaRepository->findCam_EmpleadoVerificaDenunciaById($id);

		if(empty($camEmpleadoVerificaDenuncia))
		{
			Flash::error('Cam_EmpleadoVerificaDenuncia not found');
			return redirect(route('camEmpleadoVerificaDenuncias.index'));
		}

		return view('camEmpleadoVerificaDenuncias.edit')->with('camEmpleadoVerificaDenuncia', $camEmpleadoVerificaDenuncia);
	}

	/**
	 * Update the specified Cam_EmpleadoVerificaDenuncia in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_EmpleadoVerificaDenunciaRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_EmpleadoVerificaDenunciaRequest $request)
	{
		$camEmpleadoVerificaDenuncia = $this->camEmpleadoVerificaDenunciaRepository->findCam_EmpleadoVerificaDenunciaById($id);

		if(empty($camEmpleadoVerificaDenuncia))
		{
			Flash::error('Cam_EmpleadoVerificaDenuncia not found');
			return redirect(route('camEmpleadoVerificaDenuncias.index'));
		}

		$camEmpleadoVerificaDenuncia = $this->camEmpleadoVerificaDenunciaRepository->update($camEmpleadoVerificaDenuncia, $request->all());

		Flash::message('Cam_EmpleadoVerificaDenuncia updated successfully.');

		return redirect(route('camEmpleadoVerificaDenuncias.index'));
	}

	/**
	 * Remove the specified Cam_EmpleadoVerificaDenuncia from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camEmpleadoVerificaDenuncia = $this->camEmpleadoVerificaDenunciaRepository->findCam_EmpleadoVerificaDenunciaById($id);

		if(empty($camEmpleadoVerificaDenuncia))
		{
			Flash::error('Cam_EmpleadoVerificaDenuncia not found');
			return redirect(route('camEmpleadoVerificaDenuncias.index'));
		}

		$camEmpleadoVerificaDenuncia->delete();

		Flash::message('Cam_EmpleadoVerificaDenuncia deleted successfully.');

		return redirect(route('camEmpleadoVerificaDenuncias.index'));
	}

}
