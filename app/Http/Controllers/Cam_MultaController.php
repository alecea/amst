<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_MultaRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_MultaRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_MultaController extends AppBaseController
{

	/** @var  Cam_MultaRepository */
	private $camMultaRepository;

	function __construct(Cam_MultaRepository $camMultaRepo)
	{
		$this->camMultaRepository = $camMultaRepo;
	}

	/**
	 * Display a listing of the Cam_Multa.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camMultaRepository->search($input);

		$camMultas = $result[0];

		$attributes = $result[1];

		return view('camMultas.index')
		    ->with('camMultas', $camMultas)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Multa.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camMultas.create');
	}

	/**
	 * Store a newly created Cam_Multa in storage.
	 *
	 * @param CreateCam_MultaRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_MultaRequest $request)
	{
        $input = $request->all();

		$camMulta = $this->camMultaRepository->store($input);

		Flash::message('Cam_Multa saved successfully.');

		return redirect(route('camMultas.index'));
	}

	/**
	 * Display the specified Cam_Multa.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camMulta = $this->camMultaRepository->findCam_MultaById($id);

		if(empty($camMulta))
		{
			Flash::error('Cam_Multa not found');
			return redirect(route('camMultas.index'));
		}

		return view('camMultas.show')->with('camMulta', $camMulta);
	}

	/**
	 * Show the form for editing the specified Cam_Multa.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camMulta = $this->camMultaRepository->findCam_MultaById($id);

		if(empty($camMulta))
		{
			Flash::error('Cam_Multa not found');
			return redirect(route('camMultas.index'));
		}

		return view('camMultas.edit')->with('camMulta', $camMulta);
	}

	/**
	 * Update the specified Cam_Multa in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_MultaRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_MultaRequest $request)
	{
		$camMulta = $this->camMultaRepository->findCam_MultaById($id);

		if(empty($camMulta))
		{
			Flash::error('Cam_Multa not found');
			return redirect(route('camMultas.index'));
		}

		$camMulta = $this->camMultaRepository->update($camMulta, $request->all());

		Flash::message('Cam_Multa updated successfully.');

		return redirect(route('camMultas.index'));
	}

	/**
	 * Remove the specified Cam_Multa from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camMulta = $this->camMultaRepository->findCam_MultaById($id);

		if(empty($camMulta))
		{
			Flash::error('Cam_Multa not found');
			return redirect(route('camMultas.index'));
		}

		$camMulta->delete();

		Flash::message('Cam_Multa deleted successfully.');

		return redirect(route('camMultas.index'));
	}

}
