<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateejemploRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\ejemploRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class ejemploController extends AppBaseController
{

	/** @var  ejemploRepository */
	private $ejemploRepository;

	function __construct(ejemploRepository $ejemploRepo)
	{
		$this->ejemploRepository = $ejemploRepo;
	}

	/**
	 * Display a listing of the ejemplo.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->ejemploRepository->search($input);

		$ejemplos = $result[0];

		$attributes = $result[1];

		return view('ejemplos.index')
		    ->with('ejemplos', $ejemplos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new ejemplo.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('ejemplos.create');
	}

	/**
	 * Store a newly created ejemplo in storage.
	 *
	 * @param CreateejemploRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateejemploRequest $request)
	{
        $input = $request->all();

		$ejemplo = $this->ejemploRepository->store($input);

		Flash::message('ejemplo saved successfully.');

		return redirect(route('ejemplos.index'));
	}

	/**
	 * Display the specified ejemplo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$ejemplo = $this->ejemploRepository->findejemploById($id);

		if(empty($ejemplo))
		{
			Flash::error('ejemplo not found');
			return redirect(route('ejemplos.index'));
		}

		return view('ejemplos.show')->with('ejemplo', $ejemplo);
	}

	/**
	 * Show the form for editing the specified ejemplo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$ejemplo = $this->ejemploRepository->findejemploById($id);

		if(empty($ejemplo))
		{
			Flash::error('ejemplo not found');
			return redirect(route('ejemplos.index'));
		}

		return view('ejemplos.edit')->with('ejemplo', $ejemplo);
	}

	/**
	 * Update the specified ejemplo in storage.
	 *
	 * @param  int    $id
	 * @param CreateejemploRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateejemploRequest $request)
	{
		$ejemplo = $this->ejemploRepository->findejemploById($id);

		if(empty($ejemplo))
		{
			Flash::error('ejemplo not found');
			return redirect(route('ejemplos.index'));
		}

		$ejemplo = $this->ejemploRepository->update($ejemplo, $request->all());

		Flash::message('ejemplo updated successfully.');

		return redirect(route('ejemplos.index'));
	}

	/**
	 * Remove the specified ejemplo from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$ejemplo = $this->ejemploRepository->findejemploById($id);

		if(empty($ejemplo))
		{
			Flash::error('ejemplo not found');
			return redirect(route('ejemplos.index'));
		}

		$ejemplo->delete();

		Flash::message('ejemplo deleted successfully.');

		return redirect(route('ejemplos.index'));
	}

}
