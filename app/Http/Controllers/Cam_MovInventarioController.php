<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_MovInventarioRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_MovInventarioRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_MovInventarioController extends AppBaseController
{

	/** @var  Cam_MovInventarioRepository */
	private $camMovInventarioRepository;

	function __construct(Cam_MovInventarioRepository $camMovInventarioRepo)
	{
		$this->camMovInventarioRepository = $camMovInventarioRepo;
	}

	/**
	 * Display a listing of the Cam_MovInventario.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camMovInventarioRepository->search($input);

		$camMovInventarios = $result[0];

		$attributes = $result[1];

		return view('camMovInventarios.index')
		    ->with('camMovInventarios', $camMovInventarios)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_MovInventario.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camMovInventarios.create');
	}

	/**
	 * Store a newly created Cam_MovInventario in storage.
	 *
	 * @param CreateCam_MovInventarioRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_MovInventarioRequest $request)
	{
        $input = $request->all();

		$camMovInventario = $this->camMovInventarioRepository->store($input);

		Flash::message('Cam_MovInventario saved successfully.');

		return redirect(route('camMovInventarios.index'));
	}

	/**
	 * Display the specified Cam_MovInventario.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camMovInventario = $this->camMovInventarioRepository->findCam_MovInventarioById($id);

		if(empty($camMovInventario))
		{
			Flash::error('Cam_MovInventario not found');
			return redirect(route('camMovInventarios.index'));
		}

		return view('camMovInventarios.show')->with('camMovInventario', $camMovInventario);
	}

	/**
	 * Show the form for editing the specified Cam_MovInventario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camMovInventario = $this->camMovInventarioRepository->findCam_MovInventarioById($id);

		if(empty($camMovInventario))
		{
			Flash::error('Cam_MovInventario not found');
			return redirect(route('camMovInventarios.index'));
		}

		return view('camMovInventarios.edit')->with('camMovInventario', $camMovInventario);
	}

	/**
	 * Update the specified Cam_MovInventario in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_MovInventarioRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_MovInventarioRequest $request)
	{
		$camMovInventario = $this->camMovInventarioRepository->findCam_MovInventarioById($id);

		if(empty($camMovInventario))
		{
			Flash::error('Cam_MovInventario not found');
			return redirect(route('camMovInventarios.index'));
		}

		$camMovInventario = $this->camMovInventarioRepository->update($camMovInventario, $request->all());

		Flash::message('Cam_MovInventario updated successfully.');

		return redirect(route('camMovInventarios.index'));
	}

	/**
	 * Remove the specified Cam_MovInventario from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camMovInventario = $this->camMovInventarioRepository->findCam_MovInventarioById($id);

		if(empty($camMovInventario))
		{
			Flash::error('Cam_MovInventario not found');
			return redirect(route('camMovInventarios.index'));
		}

		$camMovInventario->delete();

		Flash::message('Cam_MovInventario deleted successfully.');

		return redirect(route('camMovInventarios.index'));
	}

}
