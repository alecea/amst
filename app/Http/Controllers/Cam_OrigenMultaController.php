<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_OrigenMultaRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_OrigenMultaRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_OrigenMultaController extends AppBaseController
{

	/** @var  Cam_OrigenMultaRepository */
	private $camOrigenMultaRepository;

	function __construct(Cam_OrigenMultaRepository $camOrigenMultaRepo)
	{
		$this->camOrigenMultaRepository = $camOrigenMultaRepo;
	}

	/**
	 * Display a listing of the Cam_OrigenMulta.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camOrigenMultaRepository->search($input);

		$camOrigenMultas = $result[0];

		$attributes = $result[1];

		return view('camOrigenMultas.index')
		    ->with('camOrigenMultas', $camOrigenMultas)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_OrigenMulta.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camOrigenMultas.create');
	}

	/**
	 * Store a newly created Cam_OrigenMulta in storage.
	 *
	 * @param CreateCam_OrigenMultaRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_OrigenMultaRequest $request)
	{
        $input = $request->all();

		$camOrigenMulta = $this->camOrigenMultaRepository->store($input);

		Flash::message('Cam_OrigenMulta saved successfully.');

		return redirect(route('camOrigenMultas.index'));
	}

	/**
	 * Display the specified Cam_OrigenMulta.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camOrigenMulta = $this->camOrigenMultaRepository->findCam_OrigenMultaById($id);

		if(empty($camOrigenMulta))
		{
			Flash::error('Cam_OrigenMulta not found');
			return redirect(route('camOrigenMultas.index'));
		}

		return view('camOrigenMultas.show')->with('camOrigenMulta', $camOrigenMulta);
	}

	/**
	 * Show the form for editing the specified Cam_OrigenMulta.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camOrigenMulta = $this->camOrigenMultaRepository->findCam_OrigenMultaById($id);

		if(empty($camOrigenMulta))
		{
			Flash::error('Cam_OrigenMulta not found');
			return redirect(route('camOrigenMultas.index'));
		}

		return view('camOrigenMultas.edit')->with('camOrigenMulta', $camOrigenMulta);
	}

	/**
	 * Update the specified Cam_OrigenMulta in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_OrigenMultaRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_OrigenMultaRequest $request)
	{
		$camOrigenMulta = $this->camOrigenMultaRepository->findCam_OrigenMultaById($id);

		if(empty($camOrigenMulta))
		{
			Flash::error('Cam_OrigenMulta not found');
			return redirect(route('camOrigenMultas.index'));
		}

		$camOrigenMulta = $this->camOrigenMultaRepository->update($camOrigenMulta, $request->all());

		Flash::message('Cam_OrigenMulta updated successfully.');

		return redirect(route('camOrigenMultas.index'));
	}

	/**
	 * Remove the specified Cam_OrigenMulta from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camOrigenMulta = $this->camOrigenMultaRepository->findCam_OrigenMultaById($id);

		if(empty($camOrigenMulta))
		{
			Flash::error('Cam_OrigenMulta not found');
			return redirect(route('camOrigenMultas.index'));
		}

		$camOrigenMulta->delete();

		Flash::message('Cam_OrigenMulta deleted successfully.');

		return redirect(route('camOrigenMultas.index'));
	}

}
