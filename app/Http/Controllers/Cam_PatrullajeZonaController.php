<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_PatrullajeZonaRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_PatrullajeZonaRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_PatrullajeZonaController extends AppBaseController
{

	/** @var  Cam_PatrullajeZonaRepository */
	private $camPatrullajeZonaRepository;

	function __construct(Cam_PatrullajeZonaRepository $camPatrullajeZonaRepo)
	{
		$this->camPatrullajeZonaRepository = $camPatrullajeZonaRepo;
	}

	/**
	 * Display a listing of the Cam_PatrullajeZona.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camPatrullajeZonaRepository->search($input);

		$camPatrullajeZonas = $result[0];

		$attributes = $result[1];

		return view('camPatrullajeZonas.index')
		    ->with('camPatrullajeZonas', $camPatrullajeZonas)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_PatrullajeZona.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camPatrullajeZonas.create');
	}

	/**
	 * Store a newly created Cam_PatrullajeZona in storage.
	 *
	 * @param CreateCam_PatrullajeZonaRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_PatrullajeZonaRequest $request)
	{
        $input = $request->all();

		$camPatrullajeZona = $this->camPatrullajeZonaRepository->store($input);

		Flash::message('Cam_PatrullajeZona saved successfully.');

		return redirect(route('camPatrullajeZonas.index'));
	}

	/**
	 * Display the specified Cam_PatrullajeZona.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camPatrullajeZona = $this->camPatrullajeZonaRepository->findCam_PatrullajeZonaById($id);

		if(empty($camPatrullajeZona))
		{
			Flash::error('Cam_PatrullajeZona not found');
			return redirect(route('camPatrullajeZonas.index'));
		}

		return view('camPatrullajeZonas.show')->with('camPatrullajeZona', $camPatrullajeZona);
	}

	/**
	 * Show the form for editing the specified Cam_PatrullajeZona.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camPatrullajeZona = $this->camPatrullajeZonaRepository->findCam_PatrullajeZonaById($id);

		if(empty($camPatrullajeZona))
		{
			Flash::error('Cam_PatrullajeZona not found');
			return redirect(route('camPatrullajeZonas.index'));
		}

		return view('camPatrullajeZonas.edit')->with('camPatrullajeZona', $camPatrullajeZona);
	}

	/**
	 * Update the specified Cam_PatrullajeZona in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_PatrullajeZonaRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_PatrullajeZonaRequest $request)
	{
		$camPatrullajeZona = $this->camPatrullajeZonaRepository->findCam_PatrullajeZonaById($id);

		if(empty($camPatrullajeZona))
		{
			Flash::error('Cam_PatrullajeZona not found');
			return redirect(route('camPatrullajeZonas.index'));
		}

		$camPatrullajeZona = $this->camPatrullajeZonaRepository->update($camPatrullajeZona, $request->all());

		Flash::message('Cam_PatrullajeZona updated successfully.');

		return redirect(route('camPatrullajeZonas.index'));
	}

	/**
	 * Remove the specified Cam_PatrullajeZona from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camPatrullajeZona = $this->camPatrullajeZonaRepository->findCam_PatrullajeZonaById($id);

		if(empty($camPatrullajeZona))
		{
			Flash::error('Cam_PatrullajeZona not found');
			return redirect(route('camPatrullajeZonas.index'));
		}

		$camPatrullajeZona->delete();

		Flash::message('Cam_PatrullajeZona deleted successfully.');

		return redirect(route('camPatrullajeZonas.index'));
	}

}
