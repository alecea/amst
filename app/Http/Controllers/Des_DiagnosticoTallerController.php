<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDes_DiagnosticoTallerRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Des_DiagnosticoTallerRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Des_DiagnosticoTallerController extends AppBaseController
{

	/** @var  Des_DiagnosticoTallerRepository */
	private $desDiagnosticoTallerRepository;

	function __construct(Des_DiagnosticoTallerRepository $desDiagnosticoTallerRepo)
	{
		$this->desDiagnosticoTallerRepository = $desDiagnosticoTallerRepo;
	}

	/**
	 * Display a listing of the Des_DiagnosticoTaller.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->desDiagnosticoTallerRepository->search($input);

		$desDiagnosticoTallers = $result[0];

		$attributes = $result[1];

		return view('desDiagnosticoTallers.index')
		    ->with('desDiagnosticoTallers', $desDiagnosticoTallers)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Des_DiagnosticoTaller.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('desDiagnosticoTallers.create');
	}

	/**
	 * Store a newly created Des_DiagnosticoTaller in storage.
	 *
	 * @param CreateDes_DiagnosticoTallerRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDes_DiagnosticoTallerRequest $request)
	{
        $input = $request->all();

		$desDiagnosticoTaller = $this->desDiagnosticoTallerRepository->store($input);

		Flash::message('Des_DiagnosticoTaller saved successfully.');

		return redirect(route('desDiagnosticoTallers.index'));
	}

	/**
	 * Display the specified Des_DiagnosticoTaller.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$desDiagnosticoTaller = $this->desDiagnosticoTallerRepository->findDes_DiagnosticoTallerById($id);

		if(empty($desDiagnosticoTaller))
		{
			Flash::error('Des_DiagnosticoTaller not found');
			return redirect(route('desDiagnosticoTallers.index'));
		}

		return view('desDiagnosticoTallers.show')->with('desDiagnosticoTaller', $desDiagnosticoTaller);
	}

	/**
	 * Show the form for editing the specified Des_DiagnosticoTaller.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$desDiagnosticoTaller = $this->desDiagnosticoTallerRepository->findDes_DiagnosticoTallerById($id);

		if(empty($desDiagnosticoTaller))
		{
			Flash::error('Des_DiagnosticoTaller not found');
			return redirect(route('desDiagnosticoTallers.index'));
		}

		return view('desDiagnosticoTallers.edit')->with('desDiagnosticoTaller', $desDiagnosticoTaller);
	}

	/**
	 * Update the specified Des_DiagnosticoTaller in storage.
	 *
	 * @param  int    $id
	 * @param CreateDes_DiagnosticoTallerRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateDes_DiagnosticoTallerRequest $request)
	{
		$desDiagnosticoTaller = $this->desDiagnosticoTallerRepository->findDes_DiagnosticoTallerById($id);

		if(empty($desDiagnosticoTaller))
		{
			Flash::error('Des_DiagnosticoTaller not found');
			return redirect(route('desDiagnosticoTallers.index'));
		}

		$desDiagnosticoTaller = $this->desDiagnosticoTallerRepository->update($desDiagnosticoTaller, $request->all());

		Flash::message('Des_DiagnosticoTaller updated successfully.');

		return redirect(route('desDiagnosticoTallers.index'));
	}

	/**
	 * Remove the specified Des_DiagnosticoTaller from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$desDiagnosticoTaller = $this->desDiagnosticoTallerRepository->findDes_DiagnosticoTallerById($id);

		if(empty($desDiagnosticoTaller))
		{
			Flash::error('Des_DiagnosticoTaller not found');
			return redirect(route('desDiagnosticoTallers.index'));
		}

		$desDiagnosticoTaller->delete();

		Flash::message('Des_DiagnosticoTaller deleted successfully.');

		return redirect(route('desDiagnosticoTallers.index'));
	}

}
