<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_EmpleadoPatrullajeRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_EmpleadoPatrullajeRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_EmpleadoPatrullajeController extends AppBaseController
{

	/** @var  Cam_EmpleadoPatrullajeRepository */
	private $camEmpleadoPatrullajeRepository;

	function __construct(Cam_EmpleadoPatrullajeRepository $camEmpleadoPatrullajeRepo)
	{
		$this->camEmpleadoPatrullajeRepository = $camEmpleadoPatrullajeRepo;
	}

	/**
	 * Display a listing of the Cam_EmpleadoPatrullaje.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camEmpleadoPatrullajeRepository->search($input);

		$camEmpleadoPatrullajes = $result[0];

		$attributes = $result[1];

		return view('camEmpleadoPatrullajes.index')
		    ->with('camEmpleadoPatrullajes', $camEmpleadoPatrullajes)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_EmpleadoPatrullaje.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camEmpleadoPatrullajes.create');
	}

	/**
	 * Store a newly created Cam_EmpleadoPatrullaje in storage.
	 *
	 * @param CreateCam_EmpleadoPatrullajeRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_EmpleadoPatrullajeRequest $request)
	{
        $input = $request->all();

		$camEmpleadoPatrullaje = $this->camEmpleadoPatrullajeRepository->store($input);

		Flash::message('Cam_EmpleadoPatrullaje saved successfully.');

		return redirect(route('camEmpleadoPatrullajes.index'));
	}

	/**
	 * Display the specified Cam_EmpleadoPatrullaje.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camEmpleadoPatrullaje = $this->camEmpleadoPatrullajeRepository->findCam_EmpleadoPatrullajeById($id);

		if(empty($camEmpleadoPatrullaje))
		{
			Flash::error('Cam_EmpleadoPatrullaje not found');
			return redirect(route('camEmpleadoPatrullajes.index'));
		}

		return view('camEmpleadoPatrullajes.show')->with('camEmpleadoPatrullaje', $camEmpleadoPatrullaje);
	}

	/**
	 * Show the form for editing the specified Cam_EmpleadoPatrullaje.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camEmpleadoPatrullaje = $this->camEmpleadoPatrullajeRepository->findCam_EmpleadoPatrullajeById($id);

		if(empty($camEmpleadoPatrullaje))
		{
			Flash::error('Cam_EmpleadoPatrullaje not found');
			return redirect(route('camEmpleadoPatrullajes.index'));
		}

		return view('camEmpleadoPatrullajes.edit')->with('camEmpleadoPatrullaje', $camEmpleadoPatrullaje);
	}

	/**
	 * Update the specified Cam_EmpleadoPatrullaje in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_EmpleadoPatrullajeRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_EmpleadoPatrullajeRequest $request)
	{
		$camEmpleadoPatrullaje = $this->camEmpleadoPatrullajeRepository->findCam_EmpleadoPatrullajeById($id);

		if(empty($camEmpleadoPatrullaje))
		{
			Flash::error('Cam_EmpleadoPatrullaje not found');
			return redirect(route('camEmpleadoPatrullajes.index'));
		}

		$camEmpleadoPatrullaje = $this->camEmpleadoPatrullajeRepository->update($camEmpleadoPatrullaje, $request->all());

		Flash::message('Cam_EmpleadoPatrullaje updated successfully.');

		return redirect(route('camEmpleadoPatrullajes.index'));
	}

	/**
	 * Remove the specified Cam_EmpleadoPatrullaje from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camEmpleadoPatrullaje = $this->camEmpleadoPatrullajeRepository->findCam_EmpleadoPatrullajeById($id);

		if(empty($camEmpleadoPatrullaje))
		{
			Flash::error('Cam_EmpleadoPatrullaje not found');
			return redirect(route('camEmpleadoPatrullajes.index'));
		}

		$camEmpleadoPatrullaje->delete();

		Flash::message('Cam_EmpleadoPatrullaje deleted successfully.');

		return redirect(route('camEmpleadoPatrullajes.index'));
	}

}
