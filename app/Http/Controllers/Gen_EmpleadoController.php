<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateGen_EmpleadoRequest;
use App\Models\Gen_Empleado;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;
use Schema;

class Gen_EmpleadoController extends AppBaseController
{

	/**
	 * Display a listing of the Post.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$query = Gen_Empleado::query();
        $columns = Schema::getColumnListing('$TABLE_NAME$');
        $attributes = array();

        foreach($columns as $attribute){
            if($request[$attribute] == true)
            {
                $query->where($attribute, $request[$attribute]);
                $attributes[$attribute] =  $request[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        $genEmpleados = $query->get();

        return view('genEmpleados.index')
            ->with('genEmpleados', $genEmpleados)
            ->with('attributes', $attributes);
	}

	/**
	 * Show the form for creating a new Gen_Empleado.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('genEmpleados.create');
	}

	/**
	 * Store a newly created Gen_Empleado in storage.
	 *
	 * @param CreateGen_EmpleadoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateGen_EmpleadoRequest $request)
	{
        $input = $request->all();

		$genEmpleado = Gen_Empleado::create($input);

		Flash::message('Gen_Empleado saved successfully.');

		return redirect(route('genEmpleados.index'));
	}

	/**
	 * Display the specified Gen_Empleado.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$genEmpleado = genEmpleados::find($id);

		if(empty($genEmpleado))
		{
			Flash::error('Gen_Empleado not found');
			return redirect(route('genEmpleados.index'));
		}

		return view('genEmpleados.show')->with('genEmpleado', $genEmpleado);
	}

	/**
	 * Show the form for editing the specified Gen_Empleado.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$genEmpleado = Gen_Empleado::find($id);

		if(empty($genEmpleado))
		{
			Flash::error('Gen_Empleado not found');
			return redirect(route('genEmpleados.index'));
		}

		return view('genEmpleados.edit')->with('genEmpleado', $genEmpleado);
	}

	/**
	 * Update the specified Gen_Empleado in storage.
	 *
	 * @param  int    $id
	 * @param CreateGen_EmpleadoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateGen_EmpleadoRequest $request)
	{
		/** @var Gen_Empleado $genEmpleado */
		$genEmpleado = Gen_Empleado::find($id);

		if(empty($genEmpleado))
		{
			Flash::error('Gen_Empleado not found');
			return redirect(route('genEmpleados.index'));
		}

		$genEmpleado->fill($request->all());
		$genEmpleado->save();

		Flash::message('Gen_Empleado updated successfully.');

		return redirect(route('genEmpleados.index'));
	}

	/**
	 * Remove the specified Gen_Empleado from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var Gen_Empleado $genEmpleado */
		$genEmpleado = Gen_Empleado::find($id);

		if(empty($genEmpleado))
		{
			Flash::error('Gen_Empleado not found');
			return redirect(route('genEmpleados.index'));
		}

		$genEmpleado->delete();

		Flash::message('Gen_Empleado deleted successfully.');

		return redirect(route('genEmpleados.index'));
	}
}
