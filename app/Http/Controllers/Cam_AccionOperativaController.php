<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_AccionOperativaRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_AccionOperativaRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_AccionOperativaController extends AppBaseController
{

	/** @var  Cam_AccionOperativaRepository */
	private $camAccionOperativaRepository;

	function __construct(Cam_AccionOperativaRepository $camAccionOperativaRepo)
	{
		$this->camAccionOperativaRepository = $camAccionOperativaRepo;
	}

	/**
	 * Display a listing of the Cam_AccionOperativa.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camAccionOperativaRepository->search($input);

		$camAccionOperativas = $result[0];

		$attributes = $result[1];

		return view('camAccionOperativas.index')
		    ->with('camAccionOperativas', $camAccionOperativas)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_AccionOperativa.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camAccionOperativas.create');
	}

	/**
	 * Store a newly created Cam_AccionOperativa in storage.
	 *
	 * @param CreateCam_AccionOperativaRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_AccionOperativaRequest $request)
	{
        $input = $request->all();

		$camAccionOperativa = $this->camAccionOperativaRepository->store($input);

		Flash::message('Cam_AccionOperativa saved successfully.');

		return redirect(route('camAccionOperativas.index'));
	}

	/**
	 * Display the specified Cam_AccionOperativa.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camAccionOperativa = $this->camAccionOperativaRepository->findCam_AccionOperativaById($id);

		if(empty($camAccionOperativa))
		{
			Flash::error('Cam_AccionOperativa not found');
			return redirect(route('camAccionOperativas.index'));
		}

		return view('camAccionOperativas.show')->with('camAccionOperativa', $camAccionOperativa);
	}

	/**
	 * Show the form for editing the specified Cam_AccionOperativa.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camAccionOperativa = $this->camAccionOperativaRepository->findCam_AccionOperativaById($id);

		if(empty($camAccionOperativa))
		{
			Flash::error('Cam_AccionOperativa not found');
			return redirect(route('camAccionOperativas.index'));
		}

		return view('camAccionOperativas.edit')->with('camAccionOperativa', $camAccionOperativa);
	}

	/**
	 * Update the specified Cam_AccionOperativa in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_AccionOperativaRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_AccionOperativaRequest $request)
	{
		$camAccionOperativa = $this->camAccionOperativaRepository->findCam_AccionOperativaById($id);

		if(empty($camAccionOperativa))
		{
			Flash::error('Cam_AccionOperativa not found');
			return redirect(route('camAccionOperativas.index'));
		}

		$camAccionOperativa = $this->camAccionOperativaRepository->update($camAccionOperativa, $request->all());

		Flash::message('Cam_AccionOperativa updated successfully.');

		return redirect(route('camAccionOperativas.index'));
	}

	/**
	 * Remove the specified Cam_AccionOperativa from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camAccionOperativa = $this->camAccionOperativaRepository->findCam_AccionOperativaById($id);

		if(empty($camAccionOperativa))
		{
			Flash::error('Cam_AccionOperativa not found');
			return redirect(route('camAccionOperativas.index'));
		}

		$camAccionOperativa->delete();

		Flash::message('Cam_AccionOperativa deleted successfully.');

		return redirect(route('camAccionOperativas.index'));
	}

}
