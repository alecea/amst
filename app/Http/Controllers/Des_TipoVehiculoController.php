<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDes_TipoVehiculoRequest;
use App\Models\Des_TipoVehiculo;
use Illuminate\Http\Request;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;
use Schema;

class Des_TipoVehiculoController extends AppBaseController
{

	/**
	 * Display a listing of the Post.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
		$query = Des_TipoVehiculo::query();
        $columns = Schema::getColumnListing('$TABLE_NAME$');
        $attributes = array();

        foreach($columns as $attribute){
            if($request[$attribute] == true)
            {
                $query->where($attribute, $request[$attribute]);
                $attributes[$attribute] =  $request[$attribute];
            }else{
                $attributes[$attribute] =  null;
            }
        };

        $desTipoVehiculos = $query->get();

        return view('desTipoVehiculos.index')
            ->with('desTipoVehiculos', $desTipoVehiculos)
            ->with('attributes', $attributes);
	}

	/**
	 * Show the form for creating a new Des_TipoVehiculo.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('desTipoVehiculos.create');
	}

	/**
	 * Store a newly created Des_TipoVehiculo in storage.
	 *
	 * @param CreateDes_TipoVehiculoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDes_TipoVehiculoRequest $request)
	{
        $input = $request->all();

		$desTipoVehiculo = Des_TipoVehiculo::create($input);

		Flash::message('Des_TipoVehiculo saved successfully.');

		return redirect(route('desTipoVehiculos.index'));
	}

	/**
	 * Display the specified Des_TipoVehiculo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$desTipoVehiculo = Des_TipoVehiculo::find($id);

		if(empty($desTipoVehiculo))
		{
			Flash::error('Des_TipoVehiculo not found');
			return redirect(route('desTipoVehiculos.index'));
		}

		return view('desTipoVehiculos.show')->with('desTipoVehiculo', $desTipoVehiculo);
	}

	/**
	 * Show the form for editing the specified Des_TipoVehiculo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$desTipoVehiculo = Des_TipoVehiculo::find($id);

		if(empty($desTipoVehiculo))
		{
			Flash::error('Des_TipoVehiculo not found');
			return redirect(route('desTipoVehiculos.index'));
		}

		return view('desTipoVehiculos.edit')->with('desTipoVehiculo', $desTipoVehiculo);
	}

	/**
	 * Update the specified Des_TipoVehiculo in storage.
	 *
	 * @param  int    $id
	 * @param CreateDes_TipoVehiculoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateDes_TipoVehiculoRequest $request)
	{
		/** @var Des_TipoVehiculo $desTipoVehiculo */
		$desTipoVehiculo = Des_TipoVehiculo::find($id);

		if(empty($desTipoVehiculo))
		{
			Flash::error('Des_TipoVehiculo not found');
			return redirect(route('desTipoVehiculos.index'));
		}

		$desTipoVehiculo->fill($request->all());
		$desTipoVehiculo->save();

		Flash::message('Des_TipoVehiculo updated successfully.');

		return redirect(route('desTipoVehiculos.index'));
	}

	/**
	 * Remove the specified Des_TipoVehiculo from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		/** @var Des_TipoVehiculo $desTipoVehiculo */
		$desTipoVehiculo = Des_TipoVehiculo::find($id);

		if(empty($desTipoVehiculo))
		{
			Flash::error('Des_TipoVehiculo not found');
			return redirect(route('desTipoVehiculos.index'));
		}

		$desTipoVehiculo->delete();

		Flash::message('Des_TipoVehiculo deleted successfully.');

		return redirect(route('desTipoVehiculos.index'));
	}
}
