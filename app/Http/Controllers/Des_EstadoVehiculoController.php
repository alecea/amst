<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDes_EstadoVehiculoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Des_EstadoVehiculoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Des_EstadoVehiculoController extends AppBaseController
{

	/** @var  Des_EstadoVehiculoRepository */
	private $desEstadoVehiculoRepository;

	function __construct(Des_EstadoVehiculoRepository $desEstadoVehiculoRepo)
	{
		$this->desEstadoVehiculoRepository = $desEstadoVehiculoRepo;
	}

	/**
	 * Display a listing of the Des_EstadoVehiculo.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->desEstadoVehiculoRepository->search($input);

		$desEstadoVehiculos = $result[0];

		$attributes = $result[1];

		return view('desEstadoVehiculos.index')
		    ->with('desEstadoVehiculos', $desEstadoVehiculos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Des_EstadoVehiculo.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('desEstadoVehiculos.create');
	}

	/**
	 * Store a newly created Des_EstadoVehiculo in storage.
	 *
	 * @param CreateDes_EstadoVehiculoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDes_EstadoVehiculoRequest $request)
	{
        $input = $request->all();

		$desEstadoVehiculo = $this->desEstadoVehiculoRepository->store($input);

		Flash::message('Des_EstadoVehiculo saved successfully.');

		return redirect(route('desEstadoVehiculos.index'));
	}

	/**
	 * Display the specified Des_EstadoVehiculo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$desEstadoVehiculo = $this->desEstadoVehiculoRepository->findDes_EstadoVehiculoById($id);

		if(empty($desEstadoVehiculo))
		{
			Flash::error('Des_EstadoVehiculo not found');
			return redirect(route('desEstadoVehiculos.index'));
		}

		return view('desEstadoVehiculos.show')->with('desEstadoVehiculo', $desEstadoVehiculo);
	}

	/**
	 * Show the form for editing the specified Des_EstadoVehiculo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$desEstadoVehiculo = $this->desEstadoVehiculoRepository->findDes_EstadoVehiculoById($id);

		if(empty($desEstadoVehiculo))
		{
			Flash::error('Des_EstadoVehiculo not found');
			return redirect(route('desEstadoVehiculos.index'));
		}

		return view('desEstadoVehiculos.edit')->with('desEstadoVehiculo', $desEstadoVehiculo);
	}

	/**
	 * Update the specified Des_EstadoVehiculo in storage.
	 *
	 * @param  int    $id
	 * @param CreateDes_EstadoVehiculoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateDes_EstadoVehiculoRequest $request)
	{
		$desEstadoVehiculo = $this->desEstadoVehiculoRepository->findDes_EstadoVehiculoById($id);

		if(empty($desEstadoVehiculo))
		{
			Flash::error('Des_EstadoVehiculo not found');
			return redirect(route('desEstadoVehiculos.index'));
		}

		$desEstadoVehiculo = $this->desEstadoVehiculoRepository->update($desEstadoVehiculo, $request->all());

		Flash::message('Des_EstadoVehiculo updated successfully.');

		return redirect(route('desEstadoVehiculos.index'));
	}

	/**
	 * Remove the specified Des_EstadoVehiculo from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$desEstadoVehiculo = $this->desEstadoVehiculoRepository->findDes_EstadoVehiculoById($id);

		if(empty($desEstadoVehiculo))
		{
			Flash::error('Des_EstadoVehiculo not found');
			return redirect(route('desEstadoVehiculos.index'));
		}

		$desEstadoVehiculo->delete();

		Flash::message('Des_EstadoVehiculo deleted successfully.');

		return redirect(route('desEstadoVehiculos.index'));
	}

}
