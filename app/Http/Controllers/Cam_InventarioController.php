<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_InventarioRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_InventarioRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_InventarioController extends AppBaseController
{

	/** @var  Cam_InventarioRepository */
	private $camInventarioRepository;

	function __construct(Cam_InventarioRepository $camInventarioRepo)
	{
		$this->camInventarioRepository = $camInventarioRepo;
	}

	/**
	 * Display a listing of the Cam_Inventario.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camInventarioRepository->search($input);

		$camInventarios = $result[0];

		$attributes = $result[1];

		return view('camInventarios.index')
		    ->with('camInventarios', $camInventarios)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Inventario.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camInventarios.create');
	}

	/**
	 * Store a newly created Cam_Inventario in storage.
	 *
	 * @param CreateCam_InventarioRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_InventarioRequest $request)
	{
        $input = $request->all();

		$camInventario = $this->camInventarioRepository->store($input);

		Flash::message('Cam_Inventario saved successfully.');

		return redirect(route('camInventarios.index'));
	}

	/**
	 * Display the specified Cam_Inventario.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camInventario = $this->camInventarioRepository->findCam_InventarioById($id);

		if(empty($camInventario))
		{
			Flash::error('Cam_Inventario not found');
			return redirect(route('camInventarios.index'));
		}

		return view('camInventarios.show')->with('camInventario', $camInventario);
	}

	/**
	 * Show the form for editing the specified Cam_Inventario.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camInventario = $this->camInventarioRepository->findCam_InventarioById($id);

		if(empty($camInventario))
		{
			Flash::error('Cam_Inventario not found');
			return redirect(route('camInventarios.index'));
		}

		return view('camInventarios.edit')->with('camInventario', $camInventario);
	}

	/**
	 * Update the specified Cam_Inventario in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_InventarioRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_InventarioRequest $request)
	{
		$camInventario = $this->camInventarioRepository->findCam_InventarioById($id);

		if(empty($camInventario))
		{
			Flash::error('Cam_Inventario not found');
			return redirect(route('camInventarios.index'));
		}

		$camInventario = $this->camInventarioRepository->update($camInventario, $request->all());

		Flash::message('Cam_Inventario updated successfully.');

		return redirect(route('camInventarios.index'));
	}

	/**
	 * Remove the specified Cam_Inventario from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camInventario = $this->camInventarioRepository->findCam_InventarioById($id);

		if(empty($camInventario))
		{
			Flash::error('Cam_Inventario not found');
			return redirect(route('camInventarios.index'));
		}

		$camInventario->delete();

		Flash::message('Cam_Inventario deleted successfully.');

		return redirect(route('camInventarios.index'));
	}

}
