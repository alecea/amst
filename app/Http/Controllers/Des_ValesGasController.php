<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDes_ValesGasRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Des_ValesGasRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Des_ValesGasController extends AppBaseController
{

	/** @var  Des_ValesGasRepository */
	private $desValesGasRepository;

	function __construct(Des_ValesGasRepository $desValesGasRepo)
	{
		$this->desValesGasRepository = $desValesGasRepo;
	}

	/**
	 * Display a listing of the Des_ValesGas.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->desValesGasRepository->search($input);

		$desValesGas = $result[0];

		$attributes = $result[1];

		return view('desValesGas.index')
		    ->with('desValesGas', $desValesGas)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Des_ValesGas.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('desValesGas.create');
	}

	/**
	 * Store a newly created Des_ValesGas in storage.
	 *
	 * @param CreateDes_ValesGasRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDes_ValesGasRequest $request)
	{
        $input = $request->all();

		$desValesGas = $this->desValesGasRepository->store($input);

		Flash::message('Des_ValesGas saved successfully.');

		return redirect(route('desValesGas.index'));
	}

	/**
	 * Display the specified Des_ValesGas.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$desValesGas = $this->desValesGasRepository->findDes_ValesGasById($id);

		if(empty($desValesGas))
		{
			Flash::error('Des_ValesGas not found');
			return redirect(route('desValesGas.index'));
		}

		return view('desValesGas.show')->with('desValesGas', $desValesGas);
	}

	/**
	 * Show the form for editing the specified Des_ValesGas.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$desValesGas = $this->desValesGasRepository->findDes_ValesGasById($id);

		if(empty($desValesGas))
		{
			Flash::error('Des_ValesGas not found');
			return redirect(route('desValesGas.index'));
		}

		return view('desValesGas.edit')->with('desValesGas', $desValesGas);
	}

	/**
	 * Update the specified Des_ValesGas in storage.
	 *
	 * @param  int    $id
	 * @param CreateDes_ValesGasRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateDes_ValesGasRequest $request)
	{
		$desValesGas = $this->desValesGasRepository->findDes_ValesGasById($id);

		if(empty($desValesGas))
		{
			Flash::error('Des_ValesGas not found');
			return redirect(route('desValesGas.index'));
		}

		$desValesGas = $this->desValesGasRepository->update($desValesGas, $request->all());

		Flash::message('Des_ValesGas updated successfully.');

		return redirect(route('desValesGas.index'));
	}

	/**
	 * Remove the specified Des_ValesGas from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$desValesGas = $this->desValesGasRepository->findDes_ValesGasById($id);

		if(empty($desValesGas))
		{
			Flash::error('Des_ValesGas not found');
			return redirect(route('desValesGas.index'));
		}

		$desValesGas->delete();

		Flash::message('Des_ValesGas deleted successfully.');

		return redirect(route('desValesGas.index'));
	}

}
