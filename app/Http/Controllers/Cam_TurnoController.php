<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_TurnoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_TurnoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_TurnoController extends AppBaseController
{

	/** @var  Cam_TurnoRepository */
	private $camTurnoRepository;

	function __construct(Cam_TurnoRepository $camTurnoRepo)
	{
		$this->camTurnoRepository = $camTurnoRepo;
	}

	/**
	 * Display a listing of the Cam_Turno.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camTurnoRepository->search($input);

		$camTurnos = $result[0];

		$attributes = $result[1];

		return view('amst.cam.index')
		    ->with('camTurnos', $camTurnos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_Turno.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('amst.cam.camTurnos.create');
	}

	/**
	 * Store a newly created Cam_Turno in storage.
	 *
	 * @param CreateCam_TurnoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_TurnoRequest $request)
	{
        $input = $request->all();

		$camTurno = $this->camTurnoRepository->store($input);

		Flash::message('Cam_Turno saved successfully.');

		return redirect(route('camTurnos.index'));
	}

	/**
	 * Display the specified Cam_Turno.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camTurno = $this->camTurnoRepository->findCam_TurnoById($id);

		if(empty($camTurno))
		{
			Flash::error('Cam_Turno not found');
			return redirect(route('camTurnos.index'));
		}

		return view('camTurnos.show')->with('camTurno', $camTurno);
	}

	/**
	 * Show the form for editing the specified Cam_Turno.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camTurno = $this->camTurnoRepository->findCam_TurnoById($id);

		if(empty($camTurno))
		{
			Flash::error('Cam_Turno not found');
			return redirect(route('camTurnos.index'));
		}

		return view('camTurnos.edit')->with('camTurno', $camTurno);
	}

	/**
	 * Update the specified Cam_Turno in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_TurnoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_TurnoRequest $request)
	{
		$camTurno = $this->camTurnoRepository->findCam_TurnoById($id);

		if(empty($camTurno))
		{
			Flash::error('Cam_Turno not found');
			return redirect(route('camTurnos.index'));
		}

		$camTurno = $this->camTurnoRepository->update($camTurno, $request->all());

		Flash::message('Cam_Turno updated successfully.');

		return redirect(route('camTurnos.index'));
	}

	/**
	 * Remove the specified Cam_Turno from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camTurno = $this->camTurnoRepository->findCam_TurnoById($id);

		if(empty($camTurno))
		{
			Flash::error('Cam_Turno not found');
			return redirect(route('camTurnos.index'));
		}

		$camTurno->delete();

		Flash::message('Cam_Turno deleted successfully.');

		return redirect(route('camTurnos.index'));
	}

}
