<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateDes_VehiculoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Des_VehiculoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Des_VehiculoController extends AppBaseController
{

	/** @var  Des_VehiculoRepository */
	private $desVehiculoRepository;

	function __construct(Des_VehiculoRepository $desVehiculoRepo)
	{
		$this->desVehiculoRepository = $desVehiculoRepo;
	}

	/**
	 * Display a listing of the Des_Vehiculo.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->desVehiculoRepository->search($input);

		$desVehiculos = $result[0];

		$attributes = $result[1];

		return view('desVehiculos.index')
		    ->with('desVehiculos', $desVehiculos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Des_Vehiculo.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('desVehiculos.create');
	}

	/**
	 * Store a newly created Des_Vehiculo in storage.
	 *
	 * @param CreateDes_VehiculoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateDes_VehiculoRequest $request)
	{
        $input = $request->all();

		$desVehiculo = $this->desVehiculoRepository->store($input);

		Flash::message('Des_Vehiculo saved successfully.');

		return redirect(route('desVehiculos.index'));
	}

	/**
	 * Display the specified Des_Vehiculo.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$desVehiculo = $this->desVehiculoRepository->findDes_VehiculoById($id);

		if(empty($desVehiculo))
		{
			Flash::error('Des_Vehiculo not found');
			return redirect(route('desVehiculos.index'));
		}

		return view('desVehiculos.show')->with('desVehiculo', $desVehiculo);
	}

	/**
	 * Show the form for editing the specified Des_Vehiculo.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$desVehiculo = $this->desVehiculoRepository->findDes_VehiculoById($id);

		if(empty($desVehiculo))
		{
			Flash::error('Des_Vehiculo not found');
			return redirect(route('desVehiculos.index'));
		}

		return view('desVehiculos.edit')->with('desVehiculo', $desVehiculo);
	}

	/**
	 * Update the specified Des_Vehiculo in storage.
	 *
	 * @param  int    $id
	 * @param CreateDes_VehiculoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateDes_VehiculoRequest $request)
	{
		$desVehiculo = $this->desVehiculoRepository->findDes_VehiculoById($id);

		if(empty($desVehiculo))
		{
			Flash::error('Des_Vehiculo not found');
			return redirect(route('desVehiculos.index'));
		}

		$desVehiculo = $this->desVehiculoRepository->update($desVehiculo, $request->all());

		Flash::message('Des_Vehiculo updated successfully.');

		return redirect(route('desVehiculos.index'));
	}

	/**
	 * Remove the specified Des_Vehiculo from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$desVehiculo = $this->desVehiculoRepository->findDes_VehiculoById($id);

		if(empty($desVehiculo))
		{
			Flash::error('Des_Vehiculo not found');
			return redirect(route('desVehiculos.index'));
		}

		$desVehiculo->delete();

		Flash::message('Des_Vehiculo deleted successfully.');

		return redirect(route('desVehiculos.index'));
	}

}
