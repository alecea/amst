<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateCam_AsigEquipamientoRequest;
use Illuminate\Http\Request;
use App\Libraries\Repositories\Cam_AsigEquipamientoRepository;
use Mitul\Controller\AppBaseController;
use Response;
use Flash;

class Cam_AsigEquipamientoController extends AppBaseController
{

	/** @var  Cam_AsigEquipamientoRepository */
	private $camAsigEquipamientoRepository;

	function __construct(Cam_AsigEquipamientoRepository $camAsigEquipamientoRepo)
	{
		$this->camAsigEquipamientoRepository = $camAsigEquipamientoRepo;
	}

	/**
	 * Display a listing of the Cam_AsigEquipamiento.
	 *
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function index(Request $request)
	{
	    $input = $request->all();

		$result = $this->camAsigEquipamientoRepository->search($input);

		$camAsigEquipamientos = $result[0];

		$attributes = $result[1];

		return view('camAsigEquipamientos.index')
		    ->with('camAsigEquipamientos', $camAsigEquipamientos)
		    ->with('attributes', $attributes);;
	}

	/**
	 * Show the form for creating a new Cam_AsigEquipamiento.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('camAsigEquipamientos.create');
	}

	/**
	 * Store a newly created Cam_AsigEquipamiento in storage.
	 *
	 * @param CreateCam_AsigEquipamientoRequest $request
	 *
	 * @return Response
	 */
	public function store(CreateCam_AsigEquipamientoRequest $request)
	{
        $input = $request->all();

		$camAsigEquipamiento = $this->camAsigEquipamientoRepository->store($input);

		Flash::message('Cam_AsigEquipamiento saved successfully.');

		return redirect(route('camAsigEquipamientos.index'));
	}

	/**
	 * Display the specified Cam_AsigEquipamiento.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function show($id)
	{
		$camAsigEquipamiento = $this->camAsigEquipamientoRepository->findCam_AsigEquipamientoById($id);

		if(empty($camAsigEquipamiento))
		{
			Flash::error('Cam_AsigEquipamiento not found');
			return redirect(route('camAsigEquipamientos.index'));
		}

		return view('camAsigEquipamientos.show')->with('camAsigEquipamiento', $camAsigEquipamiento);
	}

	/**
	 * Show the form for editing the specified Cam_AsigEquipamiento.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$camAsigEquipamiento = $this->camAsigEquipamientoRepository->findCam_AsigEquipamientoById($id);

		if(empty($camAsigEquipamiento))
		{
			Flash::error('Cam_AsigEquipamiento not found');
			return redirect(route('camAsigEquipamientos.index'));
		}

		return view('camAsigEquipamientos.edit')->with('camAsigEquipamiento', $camAsigEquipamiento);
	}

	/**
	 * Update the specified Cam_AsigEquipamiento in storage.
	 *
	 * @param  int    $id
	 * @param CreateCam_AsigEquipamientoRequest $request
	 *
	 * @return Response
	 */
	public function update($id, CreateCam_AsigEquipamientoRequest $request)
	{
		$camAsigEquipamiento = $this->camAsigEquipamientoRepository->findCam_AsigEquipamientoById($id);

		if(empty($camAsigEquipamiento))
		{
			Flash::error('Cam_AsigEquipamiento not found');
			return redirect(route('camAsigEquipamientos.index'));
		}

		$camAsigEquipamiento = $this->camAsigEquipamientoRepository->update($camAsigEquipamiento, $request->all());

		Flash::message('Cam_AsigEquipamiento updated successfully.');

		return redirect(route('camAsigEquipamientos.index'));
	}

	/**
	 * Remove the specified Cam_AsigEquipamiento from storage.
	 *
	 * @param  int $id
	 *
	 * @return Response
	 */
	public function destroy($id)
	{
		$camAsigEquipamiento = $this->camAsigEquipamientoRepository->findCam_AsigEquipamientoById($id);

		if(empty($camAsigEquipamiento))
		{
			Flash::error('Cam_AsigEquipamiento not found');
			return redirect(route('camAsigEquipamientos.index'));
		}

		$camAsigEquipamiento->delete();

		Flash::message('Cam_AsigEquipamiento deleted successfully.');

		return redirect(route('camAsigEquipamientos.index'));
	}

}
