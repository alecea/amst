<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Turno extends Model
{

	public $table = "Cam_Turno";

	public $primaryKey = "Tur_Id";

	public $timestamps = false;

	public $fillable = [
	    "Tur_HorarioInicial",
		"Tur_HoraFinal",
		"Tur_Descripcion"
	];

	public static $rules = [
	    "Tur_HorarioInicial" => "required|max:5",
		"Tur_HoraFinal" => "required|max:5",
		"Tur_Descripcion" => "max:200"
	];

}
