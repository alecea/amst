<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gen_Empleado extends Model
{

	public $table = "gen_Empleado";

	public $primaryKey = "Emp_Id";

	public $timestamps = false;

	public $fillable = [
	    "Emp_PrimerNombre",
		"Emp_SegundoNombre",
		"Emp_PrimerApellido",
		"Emp_SegundoApellido",
		"Emp_Telefono",
		"Emp_Celular",
		"Emp_Dui",
		"Emp_Direccion",
		"Emp_Cargo"
	];

	public static $rules = [
	    "Emp_PrimerNombre" => "required",
		"Emp_SegundoNombre" => "required",
		"Emp_PrimerApellido" => "required",
		"Emp_SegundoApellido" => "required",
		"Emp_Telefono" => "required",
		"Emp_Celular" => "required",
		"Emp_Dui" => "required",
		"Emp_Direccion" => "required",
		"Emp_Cargo" => "required"
	];

}
