<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_PatrullajeZona extends Model
{

	public $table = "Cam_PatrullajeZona";

	public $primaryKey = "Pat_Id";

	public $timestamps = false;

	public $fillable = [
	    "Zon_Id"
	];

	public static $rules = [
	    "Zon_Id" => "required"
	];

}
