<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Des_DetDiagnostico extends Model
{

	public $table = "Des_DetDiagnostico";

	public $primaryKey = "Ded_Id";

	public $timestamps = false;

	public $fillable = [
	    "Dta_Id",
		"Pve_Id",
		"Ded_Observacion"
	];

	public static $rules = [
	    "Dta_Id" => "required",
		"Pve_Id" => "required",
		"Ded_Observacion" => "required"
	];

}
