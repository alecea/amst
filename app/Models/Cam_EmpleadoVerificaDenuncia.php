<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_EmpleadoVerificaDenuncia extends Model
{

	public $table = "Cam_EmpleadoVerificaDenuncia";

	public $primaryKey = "Evd_Id";

	public $timestamps = false;

	public $fillable = [
	    "Den_Id",
		"Emp_Id"
	];

	public static $rules = [
	    "Den_Id" => "required",
		"Emp_Id" => "required"
	];

}
