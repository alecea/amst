<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_AsigEquipamiento extends Model
{

	public $table = "Cam_AsigEquipamiento";

	public $primaryKey = "Aeq_Id";

	public $timestamps = false;

	public $fillable = [
	    "Emp_Id",
		"Aeq_Fecha",
		"Aeq_Hora",
		"Aeq_Observaciones"
	];

	public static $rules = [
	    "Emp_Id" => "required",
		"Aeq_Fecha" => "required",
		"Aeq_Hora" => "required"
	];

}
