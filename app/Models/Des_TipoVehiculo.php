<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Des_TipoVehiculo extends Model
{

	public $table = "des_tipoVehiculo";

	public $primaryKey = "Tve_Id";

	public $timestamps = false;

	public $fillable = [
	    "Tve_Descripcion"
	];

	public static $rules = [
	    "Tve_Descripcion" => "required"
	];

}
