<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Inventario extends Model
{

	public $table = "Cam_Inventario";

	public $primaryKey = "Inv_Id";

	public $timestamps = false;

	public $fillable = [
	    "Teq_Id",
		"Inv_Cantidad",
		"Com_Id"
	];

	public static $rules = [
	    "Teq_Id" => "required",
		"Inv_Cantidad" => "required",
		"Com_Id" => "required"
	];

}
