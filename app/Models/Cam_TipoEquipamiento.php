<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_TipoEquipamiento extends Model
{

	public $table = "Cam_TipoEquipamiento";

	public $primaryKey = "Teq_Id";

	public $timestamps = false;

	public $fillable = [
	    "Teq_Descripcion"
	];

	public static $rules = [
	    "Teq_Descripcion" => "required"
	];

}
