<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Denuncia extends Model
{

	public $table = "Cam_Denuncia";

	public $primaryKey = "Den_Id";

	public $timestamps = false;

	public $fillable = [
	    "Per_Id",
		"Den_Fecha",
		"Den_Hora",
		"Aop_Id",
		"Zon_Id",
		"Den_Direccion",
		"Den_ObservacionDenuncia",
		"Den_Procede",
		"Den_JustificacionProcedencia"
	];

	public static $rules = [
	    "Per_Id" => "required",
		"Den_Fecha" => "required",
		"Den_Hora" => "required",
		"Aop_Id" => "required",
		"Zon_Id" => "required",
		"Den_Direccion" => "required"
	];

}
