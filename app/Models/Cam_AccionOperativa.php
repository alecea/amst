<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_AccionOperativa extends Model
{

	public $table = "Cam_AccionOperativa";

	public $primaryKey = "Aop_Id";

	public $timestamps = false;

	public $fillable = [
	    "Aop_Tipo"
	];

	public static $rules = [
	    "Aop_Tipo" => "required"
	];

}
