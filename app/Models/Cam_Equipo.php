<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Equipo extends Model
{

	public $table = "Cam_Equipo";

	public $primaryKey = "Equ_Id";

	public $timestamps = false;

	public $fillable = [
	    "Equ_NumeroSerie",
		"Equ_Descripcion",
		"Teq_Id"
	];

	public static $rules = [
	    "Equ_NumeroSerie" => "required",
		"Equ_Descripcion" => "required",
    "Teq_Id" => "required"
	];

}
