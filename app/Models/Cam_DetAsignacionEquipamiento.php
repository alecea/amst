<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_DetAsignacionEquipamiento extends Model
{

	public $table = "Cam_DetAsignacionEquipamiento";

	public $primaryKey = "Das_Id";

	public $timestamps = false;

	public $fillable = [
	    "Cantidad",
		"Aeq_Id",
		"Teq_Id",
		"Equ_Id",
		"Arm_Id"
	];

	public static $rules = [
	    "Cantidad" => "required",
		"Aeq_Id" => "required",
		"Teq_Id" => "required"
	];

}
