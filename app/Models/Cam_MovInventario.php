<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_MovInventario extends Model
{

	public $table = "Cam_MovInventario";

	public $primaryKey = "Min_Id";

	public $timestamps = false;

	public $fillable = [
	    "Min_Fecha",
		"Min_TipoMov",
		"Com_Id",
		"Equ_Id",
		"Arm_Id"
	];

	public static $rules = [
	    "Min_Fecha" => "required",
		"Min_TipoMov" => "required",
		"Com_Id" => "required"
	];

}
