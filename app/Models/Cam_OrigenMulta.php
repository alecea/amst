<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_OrigenMulta extends Model
{

	public $table = "Cam_OrigenMulta";

	public $primaryKey = "Omu_Id";

	public $timestamps = false;

	public $fillable = [
	    "Omu_Tipo"
	];

	public static $rules = [
	    "Omu_Tipo" => "required"
	];

}
