<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Des_DiagnosticoTaller extends Model
{

	public $table = "Des_DiagnosticoTaller";

	public $primaryKey = "Dta_Id";

	public $timestamps = false;

	public $fillable = [
	    "Emp_Id",
		"Dta_FechaDiagnostico",
		"Dta_Diagnostico",
		"Tal_Id"
	];

	public static $rules = [
	    "Emp_Id" => "required",
		"Dta_FechaDiagnostico" => "required",
		"Dta_Diagnostico" => "required",
		"Tal_Id" => "required"
	];

}
