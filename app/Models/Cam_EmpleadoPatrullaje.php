<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_EmpleadoPatrullaje extends Model
{

	public $table = "Cam_EmpleadoPatrullaje";

	public $primaryKey = "Epa_Id";

	public $timestamps = false;

	public $fillable = [
	    "Pat_Id",
		"Emp_Id"
	];

	public static $rules = [
	    "Pat_Id" => "required",
		"Emp_Id" => "required"
	];

}
