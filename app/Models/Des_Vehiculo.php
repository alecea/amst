<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Des_Vehiculo extends Model
{

	public $table = "des_vehiculo";

	public $primaryKey = "Veh_Id";

	public $timestamps = False;

	public $fillable = [
	    "Veh_Marca",
		"Veh_AnnioFabricacion",
		"Veh_NoMatricula",
		"Tve_Id",
		"Veh_Modelo",
		"Veh_Kilometraje",
		"Veh_Color",
		"Veh_Cilindraje",
		"Veh_NoMotor",
		"Veh_NoChasis",
		"Veh_FechaAdquisicion",
		"Veh_Actibo"
	];

	public static $rules = [
	    "Veh_Marca" => "required",
		"Veh_AnnioFabricacion" => "required",
		"Veh_NoMatricula" => "requirede",
		"Tve_Id" => "required",
		"Veh_Annio" => "required",
		"Veh_Modelo" => "required",
		"Veh_Kilometraje" => "required",
		"Veh_Color" => "required",
		"Veh_Cilindraje" => "required",
		"Veh_NoMotor" => "required",
		"Veh_NoChasis" => "required",
		"Veh_FechaAdquisicion" => "required",
		"Veh_Actibo" => "required"
	];

}
