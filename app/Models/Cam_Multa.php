<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Multa extends Model
{

	public $table = "Cam_Multa";

	public $primaryKey = "Mul_Id";

	public $timestamps = false;

	public $fillable = [
	    "Mul_Estado",
		"Omu_Id",
		"Inf_Id",
		"Mul_Fecha",
		"Mul_Hora",
		"Tin_Id",
		"Per_Id"
	];

	public static $rules = [
	    "Mul_Estado" => "required",
		"Omu_Id" => "required",
		"Inf_Id" => "required"
	];

}
