<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_TipoInfraccion extends Model
{

	public $table = "Cam_TipoInfraccion";

	public $primaryKey = "Tin_Id";
    
	public $timestamps = false;

	public $fillable = [
	    "Tin_Articulo",
		"Tin_Descripcion",
		"Tin_Monto"
	];

	public static $rules = [
	    "Tin_Articulo" => "required",
		"Tin_Descripcion" => "required"
	];

}
