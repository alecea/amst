<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Patrullaje extends Model
{

	public $table = "Cam_Patrullaje";

	public $primaryKey = "Pat_Id";

	public $timestamps = false;

	public $fillable = [
	    "Tur_Id",
		"Emp_Id",
		"Pat_Observaciones",
		"Pat_Fecha"
	];

	public static $rules = [
	    "Tur_Id" => "required",
		"Emp_Id" => "required",
		"Pat_Fecha" => "required"
	];

}
