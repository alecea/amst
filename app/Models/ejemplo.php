<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ejemplo extends Model
{

	public $table = "ejemplo";

	public $primaryKey = "id";

	public $timestamps = false;

	public $fillable = [
	    "ruta"
	];

	public static $rules = [
	    "ruta" => "required"
	];

}
