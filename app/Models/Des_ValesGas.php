<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Des_ValesGas extends Model
{
    
	public $table = "des__vales_gas";

	public $primaryKey = "id";
    
	public $timestamps = true;

	public $fillable = [
	    "Emp_IdSolicita",
		"Via_Observacion",
		"Via_CantidadSolicitada",
		"Via_Autorizado",
		"Via_FechaEntrega",
		"Via_CantidadEntregada",
		"Emp_IdAutoriza",
		"Veh_Id"
	];

	public static $rules = [
	    "Emp_IdSolicita" => "required",
		"Via_Observacion" => "required",
		"Via_CantidadSolicitada" => "required",
		"Via_Autorizado" => "required",
		"Via_FechaEntrega" => "required",
		"Via_CantidadEntregada" => "required",
		"Emp_IdAutoriza" => "required",
		"Veh_Id" => "required"
	];

}
