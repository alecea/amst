<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Inspeccion extends Model
{

	public $table = "Cam_Inspeccion";

	public $primaryKey = "Ins_Id";

	public $timestamps = false;

	public $fillable = [
	    "Ins_FechaInspeccion",
		"Ins_HoraInspeccion",
		"Den_Id",
		"Pat_Id",
		"Ins_PersonaAuxiliada",
		"Ins_Motivo",
		"Ins_Auxilio",
		"Ins_ApoyoVehicular",
		"Ins_AccionRealizada",
		"Ins_Evento",
		"Ins_AgentePnc",
		"Ins_ProductoDecomisado",
		"Aop_Id"
	];

	public static $rules = [
	    "Ins_FechaInspeccion" => "required",
		"Ins_HoraInspeccion" => "required",
		"Ins_PersonaAuxiliada" => "required",
		"Aop_Id" => "required"
	];

}
