<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Des_TipoMantenimiento extends Model
{

	public $table = "des_tipoMantenimiento";

	public $primaryKey = "Tma_Id";

	public $timestamps = false;

	public $fillable = [
	    "Tma_Descripcion"
	];

	public static $rules = [
	    "Tma_Descripcion" => "required"
	];

}
