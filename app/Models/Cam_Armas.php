<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Armas extends Model
{

	public $table = "Cam_Armas";

	public $primaryKey = "Arm_Id";

	public $timestamps = false;

	public $fillable = [
	    "Arm_Origen",
		"Arm_Longitud",
		"Arm_LongitudCanion",
		"Arm_Calibre",
		"Arm_Peso",
		"Arm_CapacidadMunicion",
		"Arm_TipoDisparo",
		"Arm_VelocidadInicial",
		"Arm_Mira",
		"Teq_Id",
		"Arm_Estado"
	];

	public static $rules = [
	    "Arm_Origen" => "required",
		"Arm_Longitud" => "required",
		"Teq_Id" => "required",
		"Arm_Estado" => "required"
	];

}
