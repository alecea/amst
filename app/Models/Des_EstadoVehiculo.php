<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Des_EstadoVehiculo extends Model
{

	public $table = "Des_EstadoVehiculo";

	public $primaryKey = "Eve_Id";

	public $timestamps = false;

	public $fillable = [
	    "Eve_Descripcion"
	];

	public static $rules = [
	    "Eve_Descripcion" => "required"
	];

}
