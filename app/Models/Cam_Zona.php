<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cam_Zona extends Model
{

	public $table = "Cam_Zona";

	public $primaryKey = "Zon_Id";

	public $timestamps = false;

	public $fillable = [
	    "Zon_Nombre"
	];

	public static $rules = [

	];

}
